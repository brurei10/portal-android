package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

public class SiteTipoDocumentoCliente {

    private Long codDoc;
    private String descricao;
    private Long codigoSapiens;
    private String pasta;


    public SiteTipoDocumentoCliente() {
    }

    public Long getCodDoc() {
        return codDoc;
    }

    public void setCodDoc(Long codDoc) {
        this.codDoc = codDoc;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoSapiens() {
        return codigoSapiens;
    }

    public void setCodigoSapiens(Long codigoSapiens) {
        this.codigoSapiens = codigoSapiens;
    }

    public String getPasta() {
        return pasta;
    }

    public void setPasta(String pasta) {
        this.pasta = pasta;
    }

    @NonNull
    @Override
    public String toString() {
        return "SiteTipoDocumentoCliente [codDoc=" + codDoc + ", descricao=" + descricao + ", codigoSapiens="
                + codigoSapiens + ", pasta=" + pasta + "]";
    }

}
