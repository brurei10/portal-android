package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.RegistroAtividadeRSC;


public class RegistroAtividadeRSCDao extends BaseDaoImpl<RegistroAtividadeRSC, Integer> {
    public RegistroAtividadeRSCDao(ConnectionSource cs) throws SQLException {
        super(RegistroAtividadeRSC.class);
        setConnectionSource(cs);
        initialize();
    }
}
