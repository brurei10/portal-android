package orsegups.com.br.portaldocliente.activity;


import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.LinkedList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.services.ServiceSaveContato;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.SolicitacoesEnum;
import orsegups.com.br.portaldocliente.util.TelefoneMask;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.util.spinner.Spinner;

/**
 * Created by Orsegups on 03/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class ContatoActivity extends AppCompatActivity {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private Spinner spinner;
    private Context context;
    /* Fab local - 1 reclamacao | 2 solicitacao*/
    private int local;
    private int neoID = 0;
    private EditText telefone;
    private EditText nome;
    private EditText mensagem;
    private LinearLayout tipoLinear;
    private EditText email;
    private String tipoContato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contato);


        context = this;

        local = getIntent().getIntExtra(Constantes.FAB_LOCAL, 0);
        spinner = findViewById(R.id.spinner);
        Button btnEnviar = findViewById(R.id.contato_btn_enviar);
        telefone = findViewById(R.id.contato_telefone);
        nome = findViewById(R.id.contato_nome);
        mensagem = findViewById(R.id.contato_menssagem);
        email = findViewById(R.id.contato_email);

        if (local == 1) {
            MixpanelUtil.track(Utils.getCnpjLogado(this), "ContatoActivity", "RECLAMACAO", this);
        } else {
            MixpanelUtil.track(Utils.getCnpjLogado(this), "ContatoActivity", "SOLICITACAO", this);
        }


        btnEnviar.setOnClickListener(v -> solicitacaoReclamacao());

        configureToolbar();
        init();
        getEmailUser();
    }

    private void configureToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        switch (local) {
            case 1:
                getSupportActionBar().setTitle(getString(R.string.reclamacao));
                spinner.setItems(SolicitacoesEnum.RECLAMACAO);
                break;
            case 2:
                getSupportActionBar().setTitle(getString(R.string.solicita_o));
                spinner.setItems(SolicitacoesEnum.SELECIONE, SolicitacoesEnum.SUGESTAO, SolicitacoesEnum.ORCAMENTO, SolicitacoesEnum.DUVIDA, SolicitacoesEnum.ELOGIO, SolicitacoesEnum.ATUALIZACAO_CADASTRAL, SolicitacoesEnum.SOLICITACAO, SolicitacoesEnum.OUTROS);
                break;
            case 3:
                tipoLinear.setVisibility(View.GONE);
                mensagem.setText(getIntent().getStringExtra("msg"));
                getSupportActionBar().setTitle(getString(R.string.solicita_o));
                spinner.setItems(SolicitacoesEnum.SELECIONE, SolicitacoesEnum.SUGESTAO, SolicitacoesEnum.ORCAMENTO, SolicitacoesEnum.DUVIDA, SolicitacoesEnum.ELOGIO, SolicitacoesEnum.ATUALIZACAO_CADASTRAL, SolicitacoesEnum.SOLICITACAO, SolicitacoesEnum.OUTROS);
                break;
            default:
                getSupportActionBar().setTitle(getString(R.string.contato));
                spinner.setItems(SolicitacoesEnum.SELECIONE, SolicitacoesEnum.SUGESTAO, SolicitacoesEnum.ORCAMENTO, SolicitacoesEnum.DUVIDA, SolicitacoesEnum.ELOGIO, SolicitacoesEnum.ATUALIZACAO_CADASTRAL, SolicitacoesEnum.SOLICITACAO, SolicitacoesEnum.OUTROS);
                break;

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void init() {

        telefone.addTextChangedListener(TelefoneMask.insert(telefone));

        spinner.setOnItemSelectedListener((Spinner.OnItemSelectedListener<SolicitacoesEnum>) (view, position, id, item) -> {

            neoID = item.getNeoId();
            tipoContato = item.getNome();
        });
        spinner.setOnNothingSelectedListener(spinner -> {

        });

    }

    private void solicitacaoReclamacao() {

        if (!Utils.isEmailValid(email.getText().toString().trim())) {
            Alertas.erroInesperado(this, getString(R.string.alerta_email_invalido));
            return;
        }
        if (nome.getText().toString().isEmpty()) {

            Alertas.erroInesperado(this, getString(R.string.alerta_nome_vazio));
            return;
        }

        if (telefone.getText().toString().isEmpty()) {
            Alertas.erroInesperado(this, getString(R.string.alerta_telefone_vazio));
            return;
        }
        if (mensagem.getText().toString().isEmpty()) {
            Alertas.erroInesperado(this, getString(R.string.alerta_mensagem_vazio));
            return;
        }

        if (local == 2 && neoID == 0) {
            Alertas.erroInesperado(this, getString(R.string.alerta_tipo_contato_vazio));
            return;
        }

        enviaSolicitacaoReclamacao();
    }

    public void enviaSolicitacaoReclamacao() {

        final SweetAlertDialog alertDialog = Alertas.enviandoSolicitacaReclamacao(this, local);

        if (local == 1) {
            neoID = 11715151;
        } else if (local == 3) {
            neoID = Constantes.NEOID_GRUPO;
        }

        new ServiceSaveContato(
                context,
                nome.getText().toString(),
                email.getText().toString(),
                mensagem.getText().toString(),
                telefone.getText().toString(),
                neoID + "",
                ContatoActivity.this
        ).execute();

//        String url = Constantes.URL_SOLICITACAO_RECLAMACAO;
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        JsonParser jsonParser = new JsonParser();
//                        JsonElement element = jsonParser.parse(response);
//                        int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
//                        String msg = element.getAsJsonObject().get("msg").getAsString();
//
//                        switch (status){
//                            case 97:
//                                Alertas.loginInvalido2(context);
//                                break;
//                            case 100:
//
//
//                                if (local == 1)
//                                    msg = String.format(getResources().getString(R.string.alerta_enviado_com_sucesso_solicitacao), "Reclamação");
//                                else
//                                    msg = String.format(getResources().getString(R.string.alerta_enviado_com_sucesso_solicitacao), "Solicitação");
//
//
//                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//
//                                        .setTitleText("Sucesso!")
//                                        .setContentText(msg)
//                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                            @Override
//                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                                finish();
//                                            }
//                                        })
//                                        .show();
//
//                                //sucesso
//                                break;
//
//                            default:
//                                msg.replace("\"\"","");
//                                Alertas.erroInesperado(context,msg);
//                                break;
//                        }
//
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                        Alertas.erroComunicacaoServidor(context);
//                    }
//                }) {
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
//                String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
//                params.put("Content-Type", "application/json");
//                params.put("orsid-auth-token", token);
//                params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
//
//                return params;
//            }
//
//            @Override
//            protected Map<String, String> getParams() {
//
//                SharedPreferences sp = getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
//                String cnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");
//
//                Map<String, String>  params = new HashMap<String, String>();
//                params.put("nome", nome.getText().toString());
//                params.put("email", email.getText().toString());
//                params.put("mensagem", mensagem.getText().toString());
//                params.put("telefone", telefone.getText().toString());
//                params.put("tipoContato", neoID + "");
//                params.put("contractCode", Utils.getContractCode(context));
//                params.put("cpfCnpj", cnpj + "");
//                params.put("admin", Utils.isAdmin(context) + "");
//
//                return params;
//            }
//        };
//
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
//                120000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//
//        //Adding the string request to the queue
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);

    }

    public void retornoSolicitacaoReclamacao(ResponseEntity responseEntity) {

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            JsonParser jsonParser = new JsonParser();
            JsonElement element = jsonParser.parse(responseEntity.getBody().toString());
            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
            String msg = element.getAsJsonObject().get("msg").getAsString();

            switch (status) {
                case 97:
                    Alertas.loginInvalido2(context);
                    break;
                case 100:


                    if (local == 1)
                        msg = String.format(getResources().getString(R.string.alerta_enviado_com_sucesso_solicitacao), "Reclamação");
                    else
                        msg = String.format(getResources().getString(R.string.alerta_enviado_com_sucesso_solicitacao), "Solicitação");


                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)

                            .setTitleText("Sucesso!")
                            .setContentText(msg)
                            .setConfirmClickListener(sweetAlertDialog -> finish())
                            .show();

                    //sucesso
                    break;

                default:
                    msg.replace("\"\"", "");
                    Alertas.erroInesperado(context, msg);
                    break;
            }
        } else {

            Alertas.erroComunicacaoServidor(context);

        }

    }

    private void getEmailUser() {
        SharedPreferences sp = getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String retorno = sp.getString(Constantes.SP_EMAIL, "");

        if (retorno.isEmpty()) {
            capturaEmail();
        } else {
            if (retorno.split(";")[0] != null) {
                email.setText(retorno.split(";")[0]);
            }
        }

    }


    private void capturaEmail() {

        String retorno = "";

        int hasWriteContactsPermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasWriteContactsPermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
        }
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }

        }

        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();
        for (Account account : accounts) {

            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            // String[] parts = email.split("@");

            retorno = email;
        }

        if (retorno.split(";")[0] != null) {
            email.setText(retorno.split(";")[0]);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    capturaEmail();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Permissão negada", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}