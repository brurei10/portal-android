package orsegups.com.br.portaldocliente.volley.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTO implements Comparable <AccountDTO>{

    private Long id;
    private String userOwnerName;
    private String productName;
    private String cpfCnpjOwner;
    private String contractCode;
    private String identifier;
    private List<ProfileDTO> profiles;
    private List<FeatureDTO> features;
    private Boolean isAdmin;


    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public AccountDTO() {
        super();
        this.contractCode = "";
    }

    public AccountDTO(Long id) {
        this();
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserOwnerName() {
        return userOwnerName;
    }

    public void setUserOwnerName(String userOwnerName) {
        this.userOwnerName = userOwnerName;
    }

    public String getCpfCnpjOwner() {
        return cpfCnpjOwner;
    }

    public void setCpfCnpjOwner(String cpfCnpjOwner) {
        this.cpfCnpjOwner = cpfCnpjOwner;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public List<ProfileDTO> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileDTO> profiles) {
        this.profiles = profiles;
    }

    public List<FeatureDTO> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeatureDTO> features) {
        this.features = features;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public int compareTo(AccountDTO o) {
        return (this.getCpfCnpjOwner()+this.getIdentifier()).compareTo(o.getCpfCnpjOwner()+o.getIdentifier());
    }

}
