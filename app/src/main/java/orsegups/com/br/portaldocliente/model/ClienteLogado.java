package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class ClienteLogado implements Serializable{

	private String codigo;
	private String cgccpf;
	private String sessionId;
	private String nomcli;
	private String email;

	

	public String getCodigo() {
		return codigo;
	}
	public String getCgccpf() {
		return cgccpf;
	}
	public String getSessionId() {
		return sessionId;
	}
	public String getNomcli() {
		return nomcli;
	}

	public String getEmail() {
		return email;
	}

	@NonNull
	@Override
	public String toString() {
		return "ClienteLogado{" +
				"codigo='" + codigo + '\'' +
				", cgccpf='" + cgccpf + '\'' +
				", nomcli='" + nomcli + '\'' +
				", email='" + email + '\'' +
				'}';
	}
}
