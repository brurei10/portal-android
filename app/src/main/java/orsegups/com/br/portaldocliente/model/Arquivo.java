package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Arquivo {
    @Nullable
    @SerializedName("urlDownload")
    private String mUrlDownload;
    @Nullable
    @SerializedName("nomeArquivo")
    private String mNomeArquivo;
    @SerializedName("tipoArquivo")
    private String mTipoArquivo;
    @SerializedName("compArquivo")
    private String mCompArquivo;
    @SerializedName("tipoArquivoApp")
    private String mTipoArquivoSite;

    @Nullable
    public String getUrlDownload() {
        return mUrlDownload;
    }

    @Nullable
    public String getNomeArquivo() {
        return mNomeArquivo;
    }

    public String getTipoArquivo() {
        return mTipoArquivo;
    }

    public String getCompArquivo() {
        return mCompArquivo;
    }

    @NonNull
    public String getTipoArquivoSite() {
        if (mTipoArquivoSite == null || mTipoArquivoSite.isEmpty()) {
            mTipoArquivoSite = "OUTRO";
        }
        return mTipoArquivoSite;
    }
}
