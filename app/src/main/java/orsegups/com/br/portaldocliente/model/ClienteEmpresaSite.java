package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

public class ClienteEmpresaSite implements Serializable {

    private static final long serialVersionUID = 7131379789266971123L;

    private Long codemp;
    private Long codfil;
    private String cnpj;
    private String nomemp;
    private List<CompetenciaDocumento> competencias;

    public ClienteEmpresaSite() {
        super();
    }

    public ClienteEmpresaSite(Long codemp, Long codfil, String cnpj, String nomemp,
                              List<CompetenciaDocumento> competencias) {
        super();
        this.codemp = codemp;
        this.codfil = codfil;
        this.cnpj = cnpj;
        this.nomemp = nomemp;
        this.competencias = competencias;
    }

    public Long getCodemp() {
        return codemp;
    }

    public void setCodemp(Long codemp) {
        this.codemp = codemp;
    }

    public Long getCodfil() {
        return codfil;
    }

    public void setCodfil(Long codfil) {
        this.codfil = codfil;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNomemp() {
        return nomemp;
    }

    public void setNomemp(String nomemp) {
        this.nomemp = nomemp;
    }

    public List<CompetenciaDocumento> getCompetencias() {
        return competencias;
    }

    public void setCompetencias(List<CompetenciaDocumento> competencias) {
        this.competencias = competencias;
    }

    @NonNull
    @Override
    public String toString() {
        return "EmpresaSiteDTO [codemp=" + codemp + ", codfil=" + codfil + ", cnpj=" + cnpj + ", nomemp=" + nomemp
                + ", competencias=" + competencias + "]";
    }


}
