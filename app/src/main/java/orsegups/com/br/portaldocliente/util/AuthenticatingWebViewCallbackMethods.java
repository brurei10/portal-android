package orsegups.com.br.portaldocliente.util;

import java.util.HashMap;

/**
 * This interface defines the callback functions that AuthenticatingWebView.java will execute in MainActivity.java,
 * since the MainActivity handles displaying results on the screen, and AuthenticatingWebView makes asynchronous http
 * requests.
 */
public interface AuthenticatingWebViewCallbackMethods {
    /**
     * Method is called when it is time to display the progress spinner, for example when loading on the web view.
     */
    void startProgressDialog();

    /**
     * Method is called when it is time to hide the progress spinner
     */
    void stopProgressDialog();

    /**
     * Method is called when authentication is complete
     *
     * @param authorizationReturnParameters The params returned with the token
     */
    void displayResults(HashMap<String, String> authorizationReturnParameters);
}
