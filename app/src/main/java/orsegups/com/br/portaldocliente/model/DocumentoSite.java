package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

public class DocumentoSite implements Serializable {


    private static final long serialVersionUID = 1280177574009410026L;

    private Boolean chkDeacordoRecnf; // indica se o cliente aceitou ou não receber eletronicamente os documentos
    private String intnet; // email principal do cliente para faturamento
    private String cpfCli;
    private List<ClienteEmpresaSite> empresas;

    public DocumentoSite() {
        super();
    }

    public DocumentoSite(Boolean chkDeacordoRecnf, String intnet, String cpfCli, List<ClienteEmpresaSite> empresas) {
        super();
        this.chkDeacordoRecnf = chkDeacordoRecnf;
        this.intnet = intnet;
        this.cpfCli = cpfCli;
        this.empresas = empresas;
    }

    public Boolean getChkDeacordoRecnf() {
        return chkDeacordoRecnf;
    }

    public void setChkDeacordoRecnf(Boolean chkDeacordoRecnf) {
        this.chkDeacordoRecnf = chkDeacordoRecnf;
    }

    public String getIntnet() {
        return intnet;
    }

    public void setIntnet(String intnet) {
        this.intnet = intnet;
    }

    public String getCpfCli() {
        return cpfCli;
    }

    public void setCpfCli(String cpfCli) {
        this.cpfCli = cpfCli;
    }

    public List<ClienteEmpresaSite> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<ClienteEmpresaSite> empresas) {
        this.empresas = empresas;
    }

    @NonNull
    @Override
    public String toString() {
        return "DocumentoSite [chkDeacordoRecnf=" + chkDeacordoRecnf + ", intnet=" + intnet + ", cpfCli=" + cpfCli
                + ", empresas=" + empresas + "]";
    }


}
