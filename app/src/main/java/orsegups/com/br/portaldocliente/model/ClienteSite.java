package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class ClienteSite implements Serializable {


    private static final long serialVersionUID = -40674296066282233L;

    private Long codemp;
    private Long codfil;
    private String nomemp;

    private Long codcli;
    private String nomcli;
    private String apecli;
    private String cgccpf;
    private String intnet;

    public Long getCodemp() {
        return codemp;
    }

    public void setCodemp(Long codemp) {
        this.codemp = codemp;
    }

    public Long getCodfil() {
        return codfil;
    }

    public void setCodfil(Long codfil) {
        this.codfil = codfil;
    }

    public String getNomemp() {
        return nomemp;
    }

    public void setNomemp(String nomemp) {
        this.nomemp = nomemp;
    }

    public Long getCodcli() {
        return codcli;
    }

    public void setCodcli(Long codcli) {
        this.codcli = codcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getApecli() {
        return apecli;
    }

    public void setApecli(String apecli) {
        this.apecli = apecli;
    }

    public String getCgccpf() {
        return cgccpf;
    }

    public void setCgccpf(String cgccpf) {
        this.cgccpf = cgccpf;
    }

    public String getIntnet() {
        return intnet;
    }

    public void setIntnet(String intnet) {
        this.intnet = intnet;
    }

    @NonNull
    @Override
    public String toString() {
        return "ClienteSite [codemp=" + codemp + ", codfil=" + codfil + ", nomemp=" + nomemp + ", codcli=" + codcli
                + ", nomcli=" + nomcli + ", apecli=" + apecli + ", cgccpf=" + cgccpf + ", intnet=" + intnet + "]";
    }


}
