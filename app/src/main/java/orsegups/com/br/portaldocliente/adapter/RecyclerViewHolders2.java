package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.ImagemCameraRat;

public class RecyclerViewHolders2 extends RecyclerView.ViewHolder implements View.OnClickListener {


    public ImageView countryPhoto;
    public Context context;
    List<ImagemCameraRat> itemList;
    ImageView imageView;
    private ImageLoader mImageLoader;

    public RecyclerViewHolders2(View itemView, List<ImagemCameraRat> itemList, ImageView imageView) {
        super(itemView);
        itemView.setOnClickListener(this);

        this.itemList = itemList;
        countryPhoto = (ImageView) itemView.findViewById(R.id.image);
        this.imageView = imageView;
        this.context = itemView.getContext();
        initImageLoader();
    }

    @Override
    public void onClick(View view) {

        setImage(imageView, itemList.get(this.getPosition()).getLnkImg());


    }

    private void initImageLoader() {

        DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.erro_img)
                // .showImageOnLoading(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new FadeInBitmapDisplayer(1000))
                .build();

        ImageLoaderConfiguration conf = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(mDisplayImageOptions)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(conf);

    }

    private void setImage(ImageView image, String url) {

        mImageLoader.displayImage(url,
                image,
                null,
                new ImageLoadingListener() {

                    @Override
                    public void onLoadingCancelled(String uri, View view) {
                        //  Log.i("Script", "onLoadingCancelled()");
                    }

                    @Override
                    public void onLoadingComplete(String uri, View view, Bitmap bmp) {
                        //  Log.i("Script", "onLoadingComplete()");
                    }

                    @Override
                    public void onLoadingFailed(String uri, View view, FailReason fail) {
                        //Log.i("Script", "onLoadingFailed("+fail+")");
                    }

                    @Override
                    public void onLoadingStarted(String uri, View view) {
                        // Log.i("Script", "onLoadingStarted()");
                    }

                }, (uri, view, current, total) -> {

                    //Log.i("Script", "onProgressUpdate("+uri+" : "+total+" : "+current+")");
                });

    }
}