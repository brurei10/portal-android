package orsegups.com.br.portaldocliente.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.table.TableUtils;

import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.List;

import orsegups.com.br.portaldocliente.dao.DatabaseHelper;
import orsegups.com.br.portaldocliente.firebase.MyFirebaseMessagingService;
import orsegups.com.br.portaldocliente.model.ClienteGrupoEmpresa;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.FeatureDTO;

public class PermissionUtils {

    public static void storePermissions(Context context, AccountDTO account) {
        if (account != null) {
            List<FeatureDTO> features = account.getFeatures();

            setPermissionsFalse(context);

            if (features != null) {
                SharedPreferences.Editor editor = Utils.getEditorSharedPreferences(context);
                for (FeatureDTO feature : features) {
                    PermissionEnum permission = PermissionEnum.valueOf(feature.getId());
                    if (permission != null && StringUtils.isNotEmpty(permission.name())) {
                        editor.putBoolean(Constantes.SP_PERMISSION + permission.name(), true);
                    }
                }
                editor.commit();
            }
        }
    }

    private static void setPermissionsFalse(Context context) {

        SharedPreferences.Editor editor = Utils.getEditorSharedPreferences(context);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(1), false);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(7), false);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(9), false);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(10), false);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(11), false);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(12), false);
        editor.putBoolean(Constantes.SP_PERMISSION + PermissionEnum.valueOf(13), false);
        editor.commit();

    }

    public static void invalidateSession(Context context) {
        invalidateSession(context, true);
    }

    public static void invalidateSession(Context context, Boolean showDialog) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String login = sp.getString(Constantes.SP_EMAIL_ORSEGUPS_ID, "");
        sp.edit().clear().commit();
        MyFirebaseMessagingService.setBadge(context, 0);
        try {
            TableUtils.clearTable(new DatabaseHelper(context).getConnectionSource(), ClienteGrupoEmpresa.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (showDialog)
            Alertas.sessaoExpirada(context, login);
    }


}