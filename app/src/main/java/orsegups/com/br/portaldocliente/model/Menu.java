package orsegups.com.br.portaldocliente.model;

import android.graphics.drawable.Drawable;

import androidx.fragment.app.Fragment;

import java.io.Serializable;

/**
 * Created by filipe.neis on 19/04/2017.
 */

public class Menu implements Serializable {

    private int id;
    private String badge;
    private String nome;
    private Drawable icon;
    private Drawable background;
    private Fragment fragment;

    public Menu() {
        super();
    }

    public Menu(int id, String badge, String nome, Drawable icon, Drawable background, Fragment fragment) {
        this.id = id;
        this.badge = badge;
        this.nome = nome;
        this.icon = icon;
        this.background = background;
        this.fragment = fragment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public Drawable getBackground() {
        return background;
    }

    public void setBackground(Drawable background) {
        this.background = background;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Menu menu = (Menu) o;
        return id == menu.id;
    }

}





