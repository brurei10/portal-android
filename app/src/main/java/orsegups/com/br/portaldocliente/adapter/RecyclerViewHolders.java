package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.DetailsImagemCamera;
import orsegups.com.br.portaldocliente.model.ImagemCameraRat;

public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {


    List<ImagemCameraRat> itemList;
    ImageView countryPhoto;
    Context context;

    public RecyclerViewHolders(View itemView, List<ImagemCameraRat> itemList) {
        super(itemView);
        itemView.setOnClickListener(this);

        this.itemList = itemList;
        countryPhoto = (ImageView) itemView.findViewById(R.id.image);
    }

    @Override
    public void onClick(View view) {

        Gson gson = new Gson();
        String lista = gson.toJson(itemList);
        Bitmap bitmap = ((BitmapDrawable) countryPhoto.getDrawable()).getBitmap();

        Utils.setBitmap(bitmap);
        Utils.setListaImg(lista);


        Intent i = new Intent(context, DetailsImagemCamera.class);
        context.startActivity(i);
        //   Toast.makeText(view.getContext(), "Clicou na imagem Position = " + getPosition(), Toast.LENGTH_SHORT).show();

        /*
        *
        *
        Gson gson = new Gson();
        String lista = gson.toJson(itemList);

       // Intent i = new Intent(context, DetailsImagemCamera.class);
        Bitmap bitmap = ((BitmapDrawable)countryPhoto.getDrawable()).getBitmap();
//        i.putExtra("image_bitmap", bitmap);
//        i.putExtra("lista", lista);
//        context.startActivity(i);
     //   Toast.makeText(view.getContext(), "Clicou na imagem Position = " + getPosition(), Toast.LENGTH_SHORT).show();


        boolean wrapInScrollView = true;
        MaterialDialog dialog = new MaterialDialog.Builder(context)
               // .title("Teste")
                .customView(R.layout.details_image, wrapInScrollView)
               // .positiveText("Ok")
                .show();

        ImageView img = (ImageView) dialog.findViewById(R.id.image);
        img.setImageBitmap(bitmap);

//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        RecyclerView rView = (RecyclerView) dialog.findViewById(R.id.recycler_view);






        try{

             GridLayoutManager lLayout;

            lLayout = new GridLayoutManager(dialog.getContext(), 3);

            rView.setHasFixedSize(true);
            rView.setLayoutManager(lLayout);

            RecyclerViewAdapter2 rcAdapter = new RecyclerViewAdapter2(dialog.getContext(), itemList, img);

            rView.setAdapter(rcAdapter);


            List<ImagemCameraRat> finalItemList = itemList;

        }catch (Exception e){}



    }
        *
        * */

    }
}