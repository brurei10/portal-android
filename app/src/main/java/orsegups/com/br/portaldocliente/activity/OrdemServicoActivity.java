package orsegups.com.br.portaldocliente.activity;


import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.adapter.OrdemServicoListAdapter;
import orsegups.com.br.portaldocliente.model.OrdemServicoSite;


/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class OrdemServicoActivity extends AppCompatActivity {


    public RecyclerView recyclerView;
    private OrdemServicoListAdapter mAdapter;
    private List<OrdemServicoSite> ordemServicoList;
    private ActionBar actionBar;
    private TextView emptyView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ordem_servico);
        MixpanelUtil.track(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_ORDEMSERVICO, "SEGURANCA ELETRONICA", this);


        initToolbar();
        initLista();


    }

    public void initLista() {
        emptyView = findViewById(R.id.empty_view);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        //  mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ordemServicoList = new ArrayList<>();
        ordemServicoList = Utils.getOrdemServicos();

        Utils.setOrdemServicos(null);

        if (ordemServicoList == null) {
            ordemServicoList = new ArrayList<>();
        }
        mAdapter = new OrdemServicoListAdapter(this, ordemServicoList);
        recyclerView.setAdapter(mAdapter);


        if (ordemServicoList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    public void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Ordem de Serviço");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);

    }

}
