package orsegups.com.br.portaldocliente.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by filipe.neis on 10/05/2017.
 */
@DatabaseTable
public class ClienteGrupoEmpresa {

    @DatabaseField
    private
    String nomCli;
    @DatabaseField
    private
    Long cgcCpf;
    @DatabaseField
    private
    Long codGru;


    public String getNomCli() {
        return nomCli;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public Long getCgcCpf() {
        return cgcCpf;
    }

    public void setCgcCpf(Long cgcCpf) {
        this.cgcCpf = cgcCpf;
    }

    public Long getCodGru() {
        return codGru;
    }

    public void setCodGru(Long codGru) {
        this.codGru = codGru;
    }
}
