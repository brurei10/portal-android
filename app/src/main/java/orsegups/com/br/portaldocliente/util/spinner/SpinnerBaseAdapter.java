package orsegups.com.br.portaldocliente.util.spinner;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import orsegups.com.br.portaldocliente.R;


/**
 * Created by Orsegups on 07/11/16.<br>
 * Orsegups(link www.Orsegups.com.br)<br>
 *
 * @author José Mário
 * @version Orsegups 1.0
 */
public abstract class SpinnerBaseAdapter <T> extends BaseAdapter {

    private final Context context;
    private int selectedIndex;
    private int textColor;

    public SpinnerBaseAdapter(Context context) {
        this.context = context;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        final TextView textView;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.spinner_list_item, parent, false);
            textView = (TextView) convertView.findViewById(R.id.tv_tinted_spinner);
            textView.setTextColor(textColor);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Configuration config = context.getResources().getConfiguration();
                if (config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                    textView.setTextDirection(View.TEXT_DIRECTION_RTL);
                }
            }
            convertView.setTag(new ViewHolder(textView));
        } else {
            textView = ((ViewHolder) convertView.getTag()).textView;
        }
        textView.setText(getItem(position).toString());
        return convertView;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void notifyItemSelected(int index) {
        selectedIndex = index;
    }

    @Override public long getItemId(int position) {
        return position;
    }



    public SpinnerBaseAdapter<T> setTextColor(int textColor) {
        this.textColor = textColor;
        return this;
    }

    @Override
    public abstract int getCount();

    @Override
    public  abstract T getItem(int i) ;

    public abstract T get(int position);

    public abstract List<T> getItems();


    private static class ViewHolder {

        private TextView textView;

        private ViewHolder(TextView textView) {
            this.textView = textView;
        }

    }

}
