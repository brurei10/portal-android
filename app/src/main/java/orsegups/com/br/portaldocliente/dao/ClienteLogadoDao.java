package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.ClienteLogado;


public class ClienteLogadoDao extends BaseDaoImpl<ClienteLogado, Integer> {
    public ClienteLogadoDao(ConnectionSource cs) throws SQLException {
        super(ClienteLogado.class);
        setConnectionSource(cs);
        initialize();
    }
}
