package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.model.RegistroAtividadeRSC;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class HistoricoSolRecListAdapter extends RecyclerView.Adapter<HistoricoSolRecListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<RegistroAtividadeRSC> filtered_items = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public HistoricoSolRecListAdapter(Context context, List<RegistroAtividadeRSC> items) {

        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public HistoricoSolRecListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_historico_sol_rec, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final RegistroAtividadeRSC atividadeRSC = filtered_items.get(position);


        holder.atividade.setText(atividadeRSC.getNomeAtividade());

        try {
            holder.dtFim.setText(atividadeRSC.getDataInicialString().substring(0, 10));
        } catch (Exception e) {

            holder.dtFim.setText("");
        }

        try {
            holder.dtInicio.setText(atividadeRSC.getDataInicialString().substring(0, 10));
        } catch (Exception e) {

            holder.dtInicio.setText("");
        }

        try {
            holder.resposavel.setText(atividadeRSC.getResponsavel());
        } catch (Exception e) {

            holder.resposavel.setText("");
        }

        try {
            holder.descricao.setText(Utils.getTextHtml(atividadeRSC.getDescricao()));
        } catch (Exception e) {

            holder.descricao.setText("");
        }


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each nfeData item is just a string in this case
        TextView atividade;
        TextView resposavel;

        TextView dtInicio;
        TextView dtFim;
        public TextView descricao;


        public ViewHolder(View v) {
            super(v);

            dtInicio = v.findViewById(R.id.row_sol_rec_inicio);
            atividade = v.findViewById(R.id.row_sol_rec_atividade);
            resposavel = v.findViewById(R.id.row_sol_rec_resposavel);
            dtFim = v.findViewById(R.id.row_sol_rec_fim);
            descricao = v.findViewById(R.id.row_sol_rec_descricao);


        }
    }


}