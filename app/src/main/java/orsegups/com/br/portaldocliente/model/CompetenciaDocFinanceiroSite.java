package orsegups.com.br.portaldocliente.model;

import java.util.List;

public class CompetenciaDocFinanceiroSite {

    private String competencia;
    private String situacaoTitulo;
    private List<DocumentoFinanceiroSite> documentoFinanceiroSiteDTO;

    public List<DocumentoFinanceiroSite> getDocumentoFinanceiroSiteDTO() {
        return documentoFinanceiroSiteDTO;
    }


    public String getCompetencia() {
        return competencia;
    }


    public String getSituacaoTitulo() {
        return situacaoTitulo;
    }

    public void setSituacaoTitulo(String situacaoTitulo) {
        this.situacaoTitulo = situacaoTitulo;
    }
}
