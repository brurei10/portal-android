package orsegups.com.br.portaldocliente.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.firebase.MyFirebaseMessagingService;
import orsegups.com.br.portaldocliente.fragment.DocumentosFragment;
import orsegups.com.br.portaldocliente.fragment.EmpresaNotasFiscaisFragment;
import orsegups.com.br.portaldocliente.fragment.EstatisticaFragment;
import orsegups.com.br.portaldocliente.fragment.IntroChatFragment;
import orsegups.com.br.portaldocliente.fragment.ReclamacaoFragment;
import orsegups.com.br.portaldocliente.fragment.SegurancaEletronicaFragment;
import orsegups.com.br.portaldocliente.fragment.SolicitacoFragment;
import orsegups.com.br.portaldocliente.fragment.TimeLineFragment;
import orsegups.com.br.portaldocliente.model.Boleto;
import orsegups.com.br.portaldocliente.model.ClienteGrupoEmpresa;
import orsegups.com.br.portaldocliente.model.DocumentoFinanceiroSite;
import orsegups.com.br.portaldocliente.model.HistoricoReclamacao;
import orsegups.com.br.portaldocliente.model.Menu;
import orsegups.com.br.portaldocliente.model.OrdemServicoSite;
import orsegups.com.br.portaldocliente.model.RegistroAtividadeRSC;
import orsegups.com.br.portaldocliente.volley.Net.AppController;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.FeatureDTO;
import orsegups.com.br.portaldocliente.volley.dto.ProductDTO;
import orsegups.com.br.portaldocliente.volley.dto.ProfileDTO;
import orsegups.com.br.portaldocliente.volley.dto.UserDTO;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by filipe.neis on 22/02/2017.
 */

public class Utils {

    private static Context mContext = AppController.AppContext;

    private static List<OrdemServicoSite> ordemServicos;
    private static List<RegistroAtividadeRSC> registroAtividadeRSCList;
    private static List<HistoricoReclamacao> historicoReclamacaos;
    private static List<Boleto> boletos;
    private static String listaImg;
    private static Bitmap bitmap;
    @Nullable
    private static List<DocumentoFinanceiroSite> documentoFinanceiroSites;

    public static String getListaImg() {
        return listaImg;
    }

    public static void setListaImg(String listaImg) {
        Utils.listaImg = listaImg;
    }

    public static Bitmap getBitmap() {
        return bitmap;
    }

    public static void setBitmap(Bitmap bitmap) {
        Utils.bitmap = bitmap;
    }

    public static String getVersion() {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getToken(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String token = sp.getString(Constantes.SP_TOKEN_CLI, "");

        return token;
    }


    public static String getDataTimeLine(String data, String horario) {

        String dataFor = "";
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dataDate = formato.parse(data);
            SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_DATA);
            dataFor = formatter.format(dataDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dataFor + ", " + horario;
    }

    public static String completeToLeft(String value, int size) {
        String result = value;
        while (result.length() < size) {
            result = "0" + result;
        }
        return result;
    }

    public static String applyCpfCnpjMask(String str) {
        if (StringUtils.isNotEmpty(str)) {
            if (str.length() <= 11) {
                str = completeToLeft(str, 11);
                return str.substring(0, 3) + "." + str.substring(3, 6) + "." + str.substring(6, 9) + "-" + str.substring(9, 11);
            } else {
                str = completeToLeft(str, 14);
                return (str.substring(0, 2) + "." + str.substring(2, 5) + "." +
                        str.substring(5, 8) + "/" + str.substring(8, 12) + "-" +
                        str.substring(12, 14));
            }
        }
        return str;
    }

    public static String removeCpfCnpjMask(String str) {
        return str.replace(".", "").replace("/", "").replace("-", "");
    }

    public static boolean verificaConexaoInternet() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(@NonNull View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Ler Menos", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "Ler Mais", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }


    public static List<OrdemServicoSite> getOrdemServicos() {
        return ordemServicos;
    }

    public static void setOrdemServicos(List<OrdemServicoSite> ordemServicos) {
        Utils.ordemServicos = ordemServicos;
    }


    public static List<RegistroAtividadeRSC> getRegistroAtividadeRSCList() {
        return registroAtividadeRSCList;
    }

    public static void setRegistroAtividadeRSCList(List<RegistroAtividadeRSC> registroAtividadeRSCList) {
        Utils.registroAtividadeRSCList = registroAtividadeRSCList;
    }

    public static List<HistoricoReclamacao> getHistoricoReclamacaos() {
        return historicoReclamacaos;
    }

    public static void setHistoricoReclamacaos(List<HistoricoReclamacao> historicoReclamacaos) {
        Utils.historicoReclamacaos = historicoReclamacaos;
    }

    public static List<Boleto> getBoletos() {
        return boletos;
    }

    public static void setBoletos(List<Boleto> boletos) {
        Utils.boletos = boletos;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }


    public static int darker(int color, float factor) {
        return Color.argb(Color.alpha(color), Math.max((int) (Color.red(color) * factor), 0),
                Math.max((int) (Color.green(color) * factor), 0),
                Math.max((int) (Color.blue(color) * factor), 0));
    }

    /**
     * Lightens a color by a given factor.
     *
     * @param color  The color to lighten
     * @param factor The factor to lighten the color. 0 will make the color unchanged. 1 will make the
     *               color white.
     * @return lighter version of the specified color.
     */
    public static int lighter(int color, float factor) {
        int red = (int) ((Color.red(color) * (1 - factor) / 255 + factor) * 255);
        int green = (int) ((Color.green(color) * (1 - factor) / 255 + factor) * 255);
        int blue = (int) ((Color.blue(color) * (1 - factor) / 255 + factor) * 255);
        return Color.argb(Color.alpha(color), red, green, blue);
    }

    /**
     * Check if layout direction is RTL
     *
     * @param context the current context
     * @return {@code true} if the layout direction is right-to-left
     */
    public static boolean isRtl(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 &&
                context.getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
    }

    /**
     * Return a drawable object associated with a particular resource ID.
     * <p>
     * <p>Starting in {@link Build.VERSION_CODES#LOLLIPOP}, the returned drawable will be styled for the
     * specified Context's theme.</p>
     *
     * @param id The desired resource identifier, as generated by the aapt tool.
     *           This integer encodes the package, type, and resource entry.
     *           The value 0 is an invalid identifier.
     * @return Drawable An object that can be used to draw this resource.
     */
    public static Drawable getDrawable(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(id);
        }
        return context.getResources().getDrawable(id);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static String convertStringToUTF8(String str) {


        try {
            return new String(str.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;

    }

    public static Spanned getTextHtml(String descricao) {

        Spanned html;
        // descricao = descricao.replace("\n","<br>");
        //descricao = "<font style=\"text-align: justify;\">" + descricao + "</font>";
        //    descricao = "<p align=\\\"justify\\\">" + descricao + "</p>";
        //  descricao =  "Managing Safely <b> End Of Course Theory Test </b> <span style=\"text-align:justify;\"> "+descricao+" </span>";

        //descricao =  descricao.replace("span style=\"text-align:", "font text-align='").replace(";\"","'").replace("</span>", "</font>");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            html = Html.fromHtml(descricao, Html.FROM_HTML_MODE_COMPACT);
        } else {
            html = Html.fromHtml(descricao);
        }
        return html;


    }

    public static void confguraBagde(Context context, Boolean isRat) {


        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        int ratCount = sp.getInt(Constantes.SP_BADGE_RAT, 0);
        int nfCount = sp.getInt(Constantes.SP_BADGE_NOTAFISCAL, 0);

        int total;

        if (isRat) {
            total = nfCount;
            editor.putInt(Constantes.SP_BADGE_RAT, 0);

        } else {

            total = ratCount;
            editor.putInt(Constantes.SP_BADGE_NOTAFISCAL, 0);
        }

        editor.commit();
        MyFirebaseMessagingService.setBadge(context, total);


    }

    public static String firstCharacterUpper(String s) {

//        char first = Character.toUpperCase(s.charAt(0));
//        s = first + s.substring(1).toLowerCase();

        return s.toLowerCase();
    }

    public static AccountDTO getAccountByContractCode(List<AccountDTO> accountDTOList, Long contractCode) {

        for (AccountDTO accountDTO : accountDTOList) {
            if (accountDTO.getContractCode().equals("" + contractCode)) return accountDTO;
        }

        return null;
    }

    public static List<Menu> getMenuFilteredByPermissions(Context context) {
        List<Menu> menus = new ArrayList<>();

        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        Drawable itemBackground = context.getApplicationContext().getResources().getDrawable(R.drawable.relative_borda_arredondada);

        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.ESTATISTICAS.name(), false)) {
            Drawable iconEstatisticas = context.getApplicationContext().getResources().getDrawable(R.drawable.estatisticas);
            menus.add(new Menu(Constantes.ID_MENU_ESTATISTICAS, Constantes.SP_BADGE_ESTATISITICAS, "Estatística", iconEstatisticas, itemBackground, new EstatisticaFragment()));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.SEGURANCA_ELETRONICA.name(), false)) {
            Drawable iconSeguranca = context.getApplicationContext().getResources().getDrawable(R.drawable.seguranca_eletronica);
            menus.add(new Menu(Constantes.ID_MENU_SEGURANCA, Constantes.SP_BADGE_SEGURANCA, "Segurança eletrônica", iconSeguranca, itemBackground, new SegurancaEletronicaFragment()));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.RELATORIO_ATENDIMENTO.name(), false)) {
            Drawable iconRat = context.getApplicationContext().getResources().getDrawable(R.drawable.atendimento_tatico);
            menus.add(new Menu(Constantes.ID_MENU_RAT, Constantes.SP_BADGE_RAT, "Relatório de atendimento", iconRat, itemBackground, new TimeLineFragment(true)));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.NOTAS_FISCAIS.name(), false)) {
            Drawable iconNotaFiscal = context.getApplicationContext().getResources().getDrawable(R.drawable.notas_fiscais);
            menus.add(new Menu(Constantes.ID_MENU_NOTA_FISCAL, Constantes.SP_BADGE_NOTAFISCAL, "Notas Fiscais", iconNotaFiscal, itemBackground, new EmpresaNotasFiscaisFragment()));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.SOLICITACAO.name(), false)) {
            Drawable iconSolicitacao = context.getApplicationContext().getResources().getDrawable(R.drawable.sugestoes);
            menus.add(new Menu(Constantes.ID_MENU_SOLICITACAO, Constantes.SP_BADGE_SOLICITACAO, "Solicitação", iconSolicitacao, itemBackground, new SolicitacoFragment()));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.NOTIFICACOES.name(), false)) {
            Drawable iconNotificacoes = context.getApplicationContext().getResources().getDrawable(R.drawable.timeline);
            menus.add(new Menu(Constantes.ID_MENU_NOTIFICACOES, Constantes.SP_BADGE_NOTIFICACOES, "Notificações", iconNotificacoes, itemBackground, new TimeLineFragment(false)));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.RECLAMACOES.name(), false)) {
            Drawable iconReclamacao = context.getApplicationContext().getResources().getDrawable(R.drawable.reclamacoes);
            menus.add(new Menu(Constantes.ID_MENU_RECLAMACAO, Constantes.SP_BADGE_RECLAMACAO, "Reclamação", iconReclamacao, itemBackground, new ReclamacaoFragment()));
        }
        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.ATENDIMENTO_AUTOMATIZADO.name(), false)) {
            Drawable iconWhatsApp = context.getApplicationContext().getResources().getDrawable(R.drawable.whatsapp);
            iconWhatsApp.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_ATOP);
            Drawable whatsBackground = context.getApplicationContext().getResources().getDrawable(R.drawable.borda_arredondada_whats);
            menus.add(new Menu(Constantes.ID_MENU_WHATSAPP, Constantes.SP_BADGE_RECLAMACAO, "WhatsApp Orsegups", iconWhatsApp, whatsBackground, new IntroChatFragment()));
        }

        if (sp.getBoolean(Constantes.SP_PERMISSION + PermissionEnum.DOCUMENTOS.name(), true)) {
            Drawable iconReclamacao = context.getApplicationContext().getResources().getDrawable(R.drawable.documentos);
            menus.add(new Menu(Constantes.ID_DOCUMMENTOS, Constantes.SP_BADGE_DOCUMENTOS, "Documentos", iconReclamacao, itemBackground, new DocumentosFragment()));
        }
        return menus;
    }

    public static AccountDTO getActiveAccount(Context context) {
        try {
            SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            String cpfCnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");
            UserDTO user = new ObjectMapper().readValue(sp.getString(Constantes.SP_USER_ORSEGUPS_ID, ""), UserDTO.class);
            for (AccountDTO account : user.getProduct().getAccounts()) {
                if (account.getCpfCnpjOwner().equals(cpfCnpj)) {
                    return account;
                }
            }
        } catch (IOException e) {
            return null;
        }
        return null;
    }

    public static boolean isAdmin(Context context) {
        AccountDTO accountDTO = getActiveAccount(context);

        if (accountDTO != null) {
            return accountDTO.getIsAdmin();
        }

        return false;
    }

    public static String getContractCode(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.getString(Constantes.SP_CNPJ_CLI, "");
    }

    public static String getCnpjLogado(Context context) {
        return getOrsegupsIdCnpj(context);
    }

    public static String getNome(Context context) {

        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String cnpjPadrao = sp.getString(Constantes.SP_CNPJ_CLI, "");
        String retorno = "";
        List<ClienteGrupoEmpresa> lista = SincronizaBaseDados.getGrupo(context);
        if (lista != null) {
            for (ClienteGrupoEmpresa clienteGrupoEmpresa : lista) {
                String cnpj = clienteGrupoEmpresa.getCgcCpf().toString();

                if (cnpj.length() <= 11) {
                    cnpj = Utils.completeToLeft(cnpj, 11);
                } else {
                    cnpj = Utils.completeToLeft(cnpj, 14);

                }

                if (cnpjPadrao != null) {
                    if (cnpjPadrao.length() <= 11) {
                        cnpjPadrao = Utils.completeToLeft(cnpj, 11);
                    } else {
                        cnpjPadrao = Utils.completeToLeft(cnpj, 14);

                    }
                }


                if (cnpjPadrao != null && cnpjPadrao.equalsIgnoreCase(cnpj)) {
                    retorno = clienteGrupoEmpresa.getNomCli();
                    break;
                }
            }
        }


        return retorno;
    }

    public static boolean getAskedSendMobileId(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.getBoolean(Constantes.SP_ASKED_SEND_MOBILE_ID, false);
    }

    public static void setAskedSendMobileId(Context context, Boolean value) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putBoolean(Constantes.SP_ASKED_SEND_MOBILE_ID, value);

        editor.commit();
    }

    public static String getEmail(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.getString(Constantes.SP_EMAIL, "");
    }

    public static String getOrsegupsIdEmail(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.getString(Constantes.SP_EMAIL_ORSEGUPS_ID, "");
    }

    public static String getOrsegupsIdName(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.getString(Constantes.SP_NOME_CLI, "");
    }

    @Nullable
    public static String getOrsegupsIdCnpj(@NonNull Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        UserDTO user = null;
        try {
            user = new ObjectMapper().readValue(sp.getString(Constantes.SP_USER_ORSEGUPS_ID, ""), UserDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (user != null) {
            return StringUtils.defaultString(user.getCpfCnpj()).replaceAll("^null$", "");
        } else {
            return null;
        }
    }

    public static String getEndereco(Context ctx, Double lat, Double log) {

        Geocoder geocoder;
        List<Address> yourAddresses = new ArrayList<>();
        geocoder = new Geocoder(ctx, Locale.getDefault());
        try {
            yourAddresses = geocoder.getFromLocation(lat, log, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (yourAddresses.size() > 0) {
            String endereco = yourAddresses.get(0).getAddressLine(0);
            return endereco;

        }

        return "";
    }

    public static SharedPreferences.Editor getEditorSharedPreferences(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.edit();
    }

    public static void addProfileToProduct(ProductDTO productDTO, Long accountId, int featureId) {

        AccountDTO accountDTO = new AccountDTO();

        boolean existsAccount = false;

        // Verify if account exists by ID
        for (AccountDTO account : productDTO.getAccounts()) {
            if (account.getId().equals(accountId)) {
                accountDTO = account;
                existsAccount = true;
                break;
            }
        }

        if (existsAccount) {

            //Verify id feature exists
            for (FeatureDTO feature : accountDTO.getProfiles().get(0).getFeatures()) {
                if (feature.getId().equals(featureId))
                    return;
            }

        } else {

            //Create a new account and add to product
            accountDTO.setId(accountId);
            accountDTO.setContractCode("");

            ProfileDTO profileDTO = new ProfileDTO();
            profileDTO.setFeatures(new ArrayList<>());
            List<ProfileDTO> profileDTOList = new ArrayList<>();
            profileDTOList.add(profileDTO);

            accountDTO.setProfiles(profileDTOList);

            productDTO.getAccounts().add(accountDTO);
        }

        if (featureId == 0) {

            accountDTO.setIsAdmin(true);

        } else {

            FeatureDTO featureDTO = new FeatureDTO();
            featureDTO.setId(featureId);
            accountDTO.getProfiles().get(0).getFeatures().add(featureDTO);

        }

    }

    @Nullable
    public static List<DocumentoFinanceiroSite> getDocumentoFinanceiroSites() {
        return documentoFinanceiroSites;
    }

    public static void setDocumentoFinanceiroSites(@Nullable List<DocumentoFinanceiroSite> documentoFinanceiroSites) {
        Utils.documentoFinanceiroSites = documentoFinanceiroSites;
    }
}


