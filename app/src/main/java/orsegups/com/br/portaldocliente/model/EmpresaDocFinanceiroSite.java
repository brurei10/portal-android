package orsegups.com.br.portaldocliente.model;

import java.util.List;

public class EmpresaDocFinanceiroSite {

    private Long codigoEmpresa;
    private Long codigoFilial;
    private String nomeEmpresa;
    private String cnpj;
    private List<CompetenciaDocFinanceiroSite> competenciaDocFinanceiroSite;

    public List<CompetenciaDocFinanceiroSite> getCompetenciaDocFinanceiroSite() {
        return competenciaDocFinanceiroSite;
    }

    public void setCompetenciaDocFinanceiroSite(List<CompetenciaDocFinanceiroSite> competenciaDocFinanceiroSite) {
        this.competenciaDocFinanceiroSite = competenciaDocFinanceiroSite;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Long getCodigoFilial() {
        return codigoFilial;
    }

    public void setCodigoFilial(Long codigoFilial) {
        this.codigoFilial = codigoFilial;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
