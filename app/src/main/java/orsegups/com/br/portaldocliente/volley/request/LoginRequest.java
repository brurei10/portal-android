package orsegups.com.br.portaldocliente.volley.request;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.volley.common.AbstractRequest;
import orsegups.com.br.portaldocliente.volley.common.BasicRequest;
import orsegups.com.br.portaldocliente.volley.dto.LoginDTO;


public class LoginRequest extends AbstractRequest {

    private final String login;
    private final String password;

    public LoginRequest(Context context, String login, String password) {
        super(context, context.getString(R.string.alerta_login_validando));
        this.login = login;
        this.password = password;
    }

    public void exec() {
        awaitDialog.show();

        String url = Constantes.URL_LOGIN
                + "?grant_type=password"
                + "&username=" + login
                + "?product=" + Constantes.PORTAL_PRODUCT_ID.toString()
                + "&password=" + password;

        BasicRequest request = new BasicRequest<>(Request.Method.POST, url, successResponse(), errorResponse(), LoginDTO.class);
        makeRequest(request);
    }

    private Response.Listener<LoginDTO> successResponse() {
        return new Response.Listener<LoginDTO>() {
            @Override
            public void onResponse(LoginDTO response) {

                SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE).edit();
                editor.putString(Constantes.SP_TOKEN_ORSEGUPS_ID, response.getAccessToken());
                editor.putString(Constantes.SP_REFRESH_TOKEN_ORSEGUPS_ID, response.getRefreshToken());
                editor.putString(Constantes.SP_NOME_CLI, response.getUser().getName());
                editor.commit();

                new UserAccountRequest(context, response, password).exec();
            }
        };
    }

    private Response.ErrorListener errorResponse() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                awaitDialog.dismiss();
                Integer status = error.networkResponse != null ? error.networkResponse.statusCode : -1;
                switch (status) {
                    case -1:
                        Alertas.error(context, context.getString(R.string.alert_error_contact_support));
                        break;
                    case 400:
                    case 401:
                        Alertas.loginInvalido2(context);
                        break;
                    default:
                        Alertas.erroComunicacaoServidor(context);
                        break;
                }
            }
        };
    }

}
