package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.CompetenciaDocumento;

public class CompetenciaDocumentoDao extends BaseDaoImpl<CompetenciaDocumento, Integer> {
    public CompetenciaDocumentoDao(ConnectionSource cs) throws SQLException {
        super(CompetenciaDocumento.class);
        setConnectionSource(cs);
        initialize();
    }
}
