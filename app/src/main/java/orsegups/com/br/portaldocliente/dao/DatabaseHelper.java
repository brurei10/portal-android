package orsegups.com.br.portaldocliente.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.ClienteGrupoEmpresa;


public class DatabaseHelper<E> extends OrmLiteSqliteOpenHelper {
    private static final String databaseName = "portalClientes.db";
    private static final int databaseVersion = 1;

    public DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }


    @Override
    public void onCreate(SQLiteDatabase sd, ConnectionSource cs) {
        try {
            TableUtils.createTable(cs, ClienteGrupoEmpresa.class);
//			TableUtils.createTable(cs, ArquivoSite.class);
//			TableUtils.createTable(cs, ClienteEmpresaSite.class);
//			TableUtils.createTable(cs, ClienteLogado.class);
//			TableUtils.createTable(cs, ClienteSite.class);
//			TableUtils.createTable(cs, CompetenciaDocFinanceiroSite.class);
//			TableUtils.createTable(cs, CompetenciaDocumento.class);
//			TableUtils.createTable(cs, DocumentoFinanceiroSite.class);
//			TableUtils.createTable(cs, DocumentoSite.class);
//			TableUtils.createTable(cs, EmpresaDocFinanceiroSite.class);
//			TableUtils.createTable(cs, Reclamacao.class);
//			TableUtils.createTable(cs, RegistroAtividadeRSC.class);
//			TableUtils.createTable(cs, SiteTipoDocumentoCliente.class);
//			TableUtils.createTable(cs, Solicitacao.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sd, ConnectionSource cs, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(cs, ClienteGrupoEmpresa.class, true);
//			TableUtils.dropTable(cs, ArquivoSite.class, true);
//			TableUtils.dropTable(cs, ClienteEmpresaSite.class, true);
//			TableUtils.dropTable(cs, ClienteLogado.class, true);
//			TableUtils.dropTable(cs, ClienteSite.class, true);
//			TableUtils.dropTable(cs, CompetenciaDocFinanceiroSite.class, true);
//			TableUtils.dropTable(cs, CompetenciaDocumento.class, true);
//			TableUtils.dropTable(cs, DocumentoFinanceiroSite.class, true);
//			TableUtils.dropTable(cs, DocumentoSite.class, true);
//			TableUtils.dropTable(cs, EmpresaDocFinanceiroSite.class, true);
//			TableUtils.dropTable(cs, Reclamacao.class, true);
//			TableUtils.dropTable(cs, RegistroAtividadeRSC.class, true);
//			TableUtils.dropTable(cs, SiteTipoDocumentoCliente.class, true);
//			TableUtils.dropTable(cs, Solicitacao.class, true);


            onCreate(sd, cs);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void close() {
        super.close();
    }
}
