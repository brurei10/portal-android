package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.HistoricoReclamacoescActivity;
import orsegups.com.br.portaldocliente.model.Reclamacao;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class ReclamacaoListAdapter extends RecyclerView.Adapter<ReclamacaoListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<Reclamacao> filtered_items = new ArrayList<>();
    private Context ctx;
    private LayoutInflater mLayoutInflater;


    // Provide a suitable constructor (depends on the kind of dataset)
    public ReclamacaoListAdapter(Context context, List<Reclamacao> items) {

        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mOnItemClickListener = mItemClickListener;
//    }

    @NonNull
    @Override
    public ReclamacaoListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View v = mLayoutInflater.inflate(R.layout.row_reclamacao, parent, false);
        //v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
//

//
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        final Reclamacao reclamacao = filtered_items.get(position);

        holder.descricao.setText(reclamacao.getTextoReclamacao());
        holder.categoria.setText(reclamacao.getOrigem());
        holder.codigoTarefa.setText(reclamacao.getCodigoTarefa());
        String status = "";
        holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.cor_barra_vertical_card_view));
        if (reclamacao.getSituacao().equalsIgnoreCase("finished")) {
            status = "Finalizado";

            try {
                holder.relativeLayoutSatisfacao.setVisibility(View.VISIBLE);
                if (reclamacao.getSatisfacao().equalsIgnoreCase("Satisfeito")) {
                    holder.satisfacao.setText("Satisfeito");
                    holder.satisfacao.setTextColor(ctx.getResources().getColor(R.color.material_green_600));
                    holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_green_600));

                } else if (reclamacao.getSatisfacao().equalsIgnoreCase("Insatisfeito")) {
                    holder.satisfacao.setText("Insatisfeito");
                    holder.satisfacao.setTextColor(ctx.getResources().getColor(R.color.material_red_600));
                    holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_red_600));
                } else if (reclamacao.getSatisfacao().equalsIgnoreCase("Sem Contato")) {
                    holder.satisfacao.setText("Sem Contato");
                    holder.satisfacao.setTextColor(ctx.getResources().getColor(R.color.material_yellow_A700));
                    holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_yellow_A700));
                } else {
                    holder.relativeLayoutSatisfacao.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                holder.relativeLayoutSatisfacao.setVisibility(View.GONE);
            }

        } else {
            holder.relativeLayoutSatisfacao.setVisibility(View.GONE);
            status = "Em execução";
        }

        holder.status.setText(status);
        holder.dtInicio.setText(reclamacao.getDataInicio().substring(0, 10));

        try {
            holder.dtFim.setText(reclamacao.getDataConclusao().substring(0, 10));
        } catch (Exception e) {

            holder.dtFim.setText("");
        }


        if (reclamacao.getVerificacoesDeEficacia() != null) {
            if (!reclamacao.getVerificacoesDeEficacia().isEmpty()) {
                holder.verHistoricoLinear.setEnabled(true);
                holder.verHistoricoLinear.setOnClickListener(v -> {
                    Utils.setHistoricoReclamacaos(reclamacao.getVerificacoesDeEficacia());
                    Intent browserIntent = new Intent(ctx, HistoricoReclamacoescActivity.class);
                    ctx.startActivity(browserIntent);
                });


            } else {

                holder.verHistoricoLinear.setEnabled(false);
            }


        } else {
            holder.verHistoricoLinear.setEnabled(false);
        }


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView status;
        public TextView descricao;
        // each nfeData item is just a string in this case
        TextView codigoTarefa;
        TextView dtInicio;
        TextView dtFim;
        TextView satisfacao;
        RelativeLayout relativeLayoutSatisfacao;
        TextView categoria;
        View barraLateral;
        Button verHistoricoLinear;


        public ViewHolder(View v) {
            super(v);
            barraLateral = v.findViewById(R.id.barra_lateral);
            codigoTarefa = v.findViewById(R.id.row_reclamacao_codigo_tarefa);
            status = v.findViewById(R.id.row_reclamacao_status);
            dtInicio = v.findViewById(R.id.row_reclamacao_dt_inicio);
            dtFim = v.findViewById(R.id.row_reclamacao_dt_fim);
            dtFim = v.findViewById(R.id.row_reclamacao_dt_fim);
            satisfacao = v.findViewById(R.id.row_reclamacao_satisfacao);
            descricao = v.findViewById(R.id.row_reclamacao_descricao);
            categoria = v.findViewById(R.id.row_reclamacao_categoria);
            verHistoricoLinear = v.findViewById(R.id.row_reclamacao_ver_historico);
            relativeLayoutSatisfacao = v.findViewById(R.id.solicitacao_relative_satisfacao);

        }
    }


}