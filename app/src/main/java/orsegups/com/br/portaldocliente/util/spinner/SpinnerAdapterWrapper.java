package orsegups.com.br.portaldocliente.util.spinner;

import android.content.Context;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Orsegups on 08/11/16.<br>
 * Orsegups(link www.Orsegups.com.br)<br>
 *
 * @author José Mário
 * @version Orsegups 1.0
 */
public class SpinnerAdapterWrapper extends  SpinnerBaseAdapter {

    private final ListAdapter listAdapter;

    public SpinnerAdapterWrapper(Context context, ListAdapter toWrap) {
        super(context);
        listAdapter = toWrap;
    }

    @Override public int getCount() {
        return listAdapter.getCount() - 1;
    }

    @Override public Object getItem(int position) {
        if (position >= getSelectedIndex()) {
            return listAdapter.getItem(position + 1);
        } else {
            return listAdapter.getItem(position);
        }
    }

    @Override public Object get(int position) {
        return listAdapter.getItem(position);
    }

    @Override public List<Object> getItems() {
        List<Object> items = new ArrayList<>();
        for (int i = 0; i < getCount(); i++) {
            items.add(getItem(i));
        }
        return items;
    }

}
