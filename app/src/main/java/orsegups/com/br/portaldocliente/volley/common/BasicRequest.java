package orsegups.com.br.portaldocliente.volley.common;

import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

public class BasicRequest<T> extends ObjectRequest<T> {

    public BasicRequest(int method, String url, Response.Listener<T> listener,
                         Response.ErrorListener errorListener, Class<T> type) {
        super(method, url, listener, errorListener, type);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        String credentials = "mobile:bGciOiJSUzI1NiIsInR5cCI6IkpXVCJ";
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        params.put("Authorization", auth);
        return params;
    }

}
