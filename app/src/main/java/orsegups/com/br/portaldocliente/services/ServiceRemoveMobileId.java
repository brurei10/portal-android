package orsegups.com.br.portaldocliente.services;

import android.content.Context;
import android.os.AsyncTask;

import com.google.firebase.iid.FirebaseInstanceId;

import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.ServiceHandler;

public class ServiceRemoveMobileId extends AsyncTask {

    private Context context;

    public ServiceRemoveMobileId(Context context) {
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String url = Constantes.URL_REMOVE_MOBILE_ID +
                "?id=" + Utils.getToken(context);
        ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.POST, context, response -> {
//                Log.d("Debug", response);
        });
        return "";
    }
}
