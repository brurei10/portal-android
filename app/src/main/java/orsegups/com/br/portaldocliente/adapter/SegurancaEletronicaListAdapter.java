package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.OrdemServicoActivity;
import orsegups.com.br.portaldocliente.activity.WebalarmeActivity;
import orsegups.com.br.portaldocliente.model.ContasSigmaSite;

/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */
public class SegurancaEletronicaListAdapter extends RecyclerView.Adapter<SegurancaEletronicaListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<ContasSigmaSite> original_items = new ArrayList<>();
    private List<ContasSigmaSite> filtered_items = new ArrayList<>();
    private Context context;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public SegurancaEletronicaListAdapter(Context context, List<ContasSigmaSite> items) {
        original_items = items;
        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.context = context;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public SegurancaEletronicaListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_seguranca, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final ContasSigmaSite seguranca = filtered_items.get(position);

        holder.conta.setText(seguranca.getCentralParticao() + " " + seguranca.getTipoConta());
        String end = seguranca.getEndereco() + " - " + seguranca.getBairro() + " - " + seguranca.getCidade() + " - " + seguranca.getUf();
        holder.endereco.setText(end);
        if (seguranca.getLinkEventos().contains("&senha=null&")) {
            holder.eventos.setEnabled(false);
        }
        holder.eventos.setOnClickListener(v -> {

            Intent browserIntent = new Intent(context, WebalarmeActivity.class);
            Bundle b = new Bundle();
            b.putString("url", seguranca.getLinkEventos()); //Your id
            browserIntent.putExtras(b);
            context.startActivity(browserIntent);

        });


        if (seguranca.getOrdensDeServico().isEmpty()) {
            holder.ordemDeServico.setEnabled(false);
        } else {

            holder.ordemDeServico.setEnabled(true);

            holder.ordemDeServico.setOnClickListener(v -> {
                Utils.setOrdemServicos(seguranca.getOrdensDeServico());
                Intent browserIntent = new Intent(context, OrdemServicoActivity.class);
                context.startActivity(browserIntent);
            });
        }


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each nfeData item is just a string in this case
        TextView conta;
        TextView endereco;
        Button ordemDeServico;
        Button eventos;


        public ViewHolder(View v) {
            super(v);
            conta = v.findViewById(R.id.row_seguranca_conta);
            endereco = v.findViewById(R.id.row_seguranca_endereco);
            ordemDeServico = v.findViewById(R.id.row_seguranca_ordem_servico);
            eventos = v.findViewById(R.id.row_seguranca_evento);


        }
    }


}