package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.HistoricoSolRecActivity;
import orsegups.com.br.portaldocliente.model.Solicitacao;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class SolicitacaoListAdapter extends RecyclerView.Adapter<SolicitacaoListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<Solicitacao> original_items = new ArrayList<>();
    private List<Solicitacao> filtered_items = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public SolicitacaoListAdapter(Context context, List<Solicitacao> items) {
        original_items = items;
        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public SolicitacaoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_solicitacao, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Solicitacao solicitacao = filtered_items.get(position);

        String status = "";
        holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.cor_barra_vertical_card_view));
        if (solicitacao.getSituacao().equalsIgnoreCase("finished")) {
            holder.verHistoricoLinear.setVisibility(View.GONE);
            status = "Finalizado";
            try {
                holder.relativeLayoutSatisfacao.setVisibility(View.VISIBLE);

                if (solicitacao.getSatisfacao().equalsIgnoreCase("Satisfeito")) {
                    holder.satisfacao.setText("Satisfeito");
                    holder.satisfacao.setTextColor(ctx.getResources().getColor(R.color.material_green_600));
                    holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_green_600));

                } else if (solicitacao.getSatisfacao().equalsIgnoreCase("Insatisfeito")) {
                    holder.satisfacao.setText("Insatisfeito");
                    holder.satisfacao.setTextColor(ctx.getResources().getColor(R.color.material_red_600));
                    holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_red_600));
                } else if (solicitacao.getSatisfacao().equalsIgnoreCase("Sem Contato")) {
                    holder.satisfacao.setText("Sem Contato");
                    holder.satisfacao.setTextColor(ctx.getResources().getColor(R.color.material_yellow_A700));
                    holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_yellow_A700));
                } else {
                    holder.relativeLayoutSatisfacao.setVisibility(View.GONE);

                }


            } catch (Exception e) {
                holder.relativeLayoutSatisfacao.setVisibility(View.GONE);

            }
        } else {
            holder.relativeLayoutSatisfacao.setVisibility(View.GONE);
            // holder.verHistoricoLinear.setVisibility(View.GONE);
            status = "Em execução";

        }

        holder.verHistoricoLinear.setVisibility(View.VISIBLE);
        if (!solicitacao.getRegistroAtividades().isEmpty()) {
            holder.verHistorico.setEnabled(true);
            holder.verHistorico.setOnClickListener(v -> {
                Utils.setRegistroAtividadeRSCList(solicitacao.getRegistroAtividades());
                Intent browserIntent = new Intent(ctx, HistoricoSolRecActivity.class);
                ctx.startActivity(browserIntent);
            });


        } else {

            holder.verHistorico.setEnabled(false);
        }


        holder.contato.setText(solicitacao.getContato());
        holder.status.setText(status);
        holder.dtInicio.setText(solicitacao.getDataInicio().substring(0, 10));
        try {
            holder.dtFim.setText(solicitacao.getDataConclusao().substring(0, 10));
        } catch (Exception e) {

            holder.dtFim.setText("");
        }

        holder.descricao.setText(solicitacao.getDescricao().trim());
        holder.atendente.setText(solicitacao.getAtendente());
        holder.categoria.setText(solicitacao.getCategoria());
        holder.codigoTarefa.setText(solicitacao.getCodigoTarefa());


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView status;
        public TextView descricao;
        // each nfeData item is just a string in this case
        TextView contato;
        TextView codigoTarefa;
        TextView dtInicio;
        TextView dtFim;
        TextView satisfacao;
        RelativeLayout relativeLayoutSatisfacao;
        TextView atendente;
        TextView categoria;
        View barraLateral;
        Button verHistorico;
        LinearLayout verHistoricoLinear;


        public ViewHolder(View v) {
            super(v);
            contato = v.findViewById(R.id.row_solicitacao_contato);
            codigoTarefa = v.findViewById(R.id.row_solicitacao_codigo_tarefa);
            status = v.findViewById(R.id.row_solicitacao_status);
            dtInicio = v.findViewById(R.id.row_solicitacao_dt_inicio);
            dtFim = v.findViewById(R.id.row_solicitacao_dt_fim);
            satisfacao = v.findViewById(R.id.row_solicitacao_satisfacao);
            descricao = v.findViewById(R.id.row_solicitacao_descricao);
            atendente = v.findViewById(R.id.row_solicitacao_atendente);
            categoria = v.findViewById(R.id.row_solicitacao_categoria);
            relativeLayoutSatisfacao = v.findViewById(R.id.solicitacao_relative_satisfacao);
            verHistorico = v.findViewById(R.id.row_solicitacao_ver_historico);
            verHistoricoLinear = v.findViewById(R.id.row_solicitacao_ver_historico_linear);
            barraLateral = v.findViewById(R.id.barra_lateral);

        }
    }


}