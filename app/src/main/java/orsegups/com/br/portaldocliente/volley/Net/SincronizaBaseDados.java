package orsegups.com.br.portaldocliente.volley.Net;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.table.TableUtils;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.leolin.shortcutbadger.ShortcutBadger;
import orsegups.com.br.portaldocliente.MainActivity;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.activity.LoginActivity;
import orsegups.com.br.portaldocliente.dao.ClienteGrupoEmpresaDao;
import orsegups.com.br.portaldocliente.dao.DatabaseHelper;
import orsegups.com.br.portaldocliente.firebase.MyFirebaseMessagingService;
import orsegups.com.br.portaldocliente.model.BadgeContador;
import orsegups.com.br.portaldocliente.model.BadgeNota;
import orsegups.com.br.portaldocliente.model.BadgeRat;
import orsegups.com.br.portaldocliente.model.ClienteGrupoEmpresa;
import orsegups.com.br.portaldocliente.model.ClienteLogado;
import orsegups.com.br.portaldocliente.model.Rat;
import orsegups.com.br.portaldocliente.services.ServiceRemoveMobileId;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.PermissionUtils;
import orsegups.com.br.portaldocliente.util.Utils;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by filipe.neis on 18/01/2017.
 */

public class SincronizaBaseDados {

    private static Thread thread;

    public static void changePassword(final Context context, final String currentPassword, final String newPassword, final String email) {
        changePassword(context, currentPassword, newPassword, email, false);
    }

    @SuppressLint("ApplySharedPref")
    public static void changePassword(final Context context, final String currentPassword, final String newPassword, final String email, final Boolean firstLogin) {
//        final SweetAlertDialog dialog = Alertas.wait(context, context.getString(R.string.alert_changing_password));

        String url = Constantes.URL_CHANGE_PASSWORD;

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("newPassword", newPassword);
            jsonObject.put("currentPassword", currentPassword);
        } catch (JSONException e) {
            Log.e("Erro", e.getMessage());
        }
        ServiceHandler.chamadaServicoObject(url, ServiceHandler.ServiceHandlerEnum.PUT, context, jsonObject, object -> {
            SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(Constantes.SP_SENHA_CLI, newPassword);
            editor.commit();
            if (firstLogin) {
                Intent intent = new Intent();
                intent.setClass(AppController.AppContext, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                AppController.AppContext.startActivity(intent);
            } else {
                Alertas.changedPassword(context);
            }
        });


    }

    public static boolean avaliacaoRat(final Context context, final Rat rat) {

//        final SweetAlertDialog dialog = Alertas.comunicacaoServidor(context, context.getString(R.string.alerta_altera_senha));

        final SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);


        String url = Constantes.URL_AVALIACAO_RAT
                + "id=" + rat.getId()
                + "&nota=" + rat.getAvaliacaoNota()
                + "&comentario=" + rat.getAvaliacaoComentario()
                + "&autorizacao=" + rat.getAvaliacaoAutorizacao();
        ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, context, response -> {

            JsonParser jsonParser = new JsonParser();
            JsonElement element = jsonParser.parse(response);
            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
            String msg = element.getAsJsonObject().get("msg").getAsString();
//                        dialog.dismiss();
            switch (status) {
                case 100:
//                                Alertas.senhaAlteradaComsucesso(context);
                    //sucesso
                    break;

                case 96:
                    Alertas.sessaoExpirada(context, sp.getString(Constantes.SP_CNPJ_CLI_MASK, ""));
                    break;
                default:

                    Alertas.erroInesperado(context, msg.replace("\"\"", ""));
                    break;
            }
        });


        return true;

    }

    public static void removeToken(final Context context, final Activity activity) {
        final SweetAlertDialog dialog = Alertas.msgSair(context);
        dialog.setConfirmClickListener(sweetAlertDialog -> {
            sweetAlertDialog.dismissWithAnimation();
            saindoApp(context, activity);
        });
    }

    public static void saindoApp(final Context context, final Activity activity) {
        final SweetAlertDialog dialog = Alertas.wait(context, context.getString(R.string.alerta_logout));

        Utils.setAskedSendMobileId(context, false);
        new ServiceRemoveMobileId(context).execute();
        final SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String url = Constantes.URL_LOGOUT + sp.getString(Constantes.SP_TOKEN_CLI, "");
        Log.i("URL_LOGOUT", url);

        dialog.dismiss();
        PermissionUtils.invalidateSession(context, false);

        Intent intentLogin = new Intent(context, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentLogin);
        activity.finish();

    }

    @SuppressLint("ApplySharedPref")
    public static void badgeContador(final Context context, final int badgeLocal) {

        final SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);

        String contractCode = sp.getString(Constantes.SP_CNPJ_CLI, "");
        String cpfCnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");

        String url = Constantes.URL_BADGE_NOVO;
        ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, context, response -> {
            if (response.isEmpty())
                return;
            JsonParser jsonParser = new JsonParser();
            JsonElement element = jsonParser.parse(response);
            if (element == null)
                return;
            Gson gson = new Gson();
            JsonObject jsonObject = element.getAsJsonObject();
            if (jsonObject != null) {
                JsonElement jsonElement = jsonObject.get("ret");

                //Lendo as badges rats
                JsonArray jsonElement1 = jsonElement.getAsJsonObject().get("badgeRats").getAsJsonArray();
                List<BadgeRat> lista = new ArrayList<>();
                for (JsonElement element1 : jsonElement1) {

                    BadgeRat badgeRat = gson.fromJson(element1, BadgeRat.class);
                    lista.add(badgeRat);
                }
                //Lendo badge notas
                JsonArray jsonElement2 = jsonElement.getAsJsonObject().get("badgeNotas").getAsJsonArray();
                List<BadgeNota> lista2 = new ArrayList<>();
                for (JsonElement element1 : jsonElement2) {

                    BadgeNota badgeRat = gson.fromJson(element1, BadgeNota.class);
                    lista2.add(badgeRat);
                }

                BadgeContador badge = gson.fromJson(jsonElement, BadgeContador.class);
                badge.setListaRats(lista);
                badge.setListxaNotas(lista2);
                SharedPreferences.Editor edit = sp.edit();

                //Adicionando badge ao icone da app
                ShortcutBadger.applyCount(context, badge.getTotal());

                switch (badgeLocal) {
                    case Constantes.BADGE_LOCAL_RAT:
                        edit.putInt(Constantes.SP_BADGE_NOTAFISCAL, badge.getNotasfiscaisCount(cpfCnpj));
                        break;
                    case Constantes.BADGE_LOCAL_NF:
                        edit.putInt(Constantes.SP_BADGE_RAT, badge.getRatCount(contractCode));
                        break;
                    default:
                        edit.putInt(Constantes.SP_BADGE_NOTAFISCAL, badge.getNotasfiscaisCount(cpfCnpj));
                        edit.putInt(Constantes.SP_BADGE_RAT, badge.getRatCount(contractCode));
                        break;
                }

                edit.commit();
            }
        });

    }

    public static void novaEmpresaGrupo(final Context context, String novoCnpj) {

        final SweetAlertDialog dialog = Alertas.wait(context, "");

        final SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);

        String cnpjCli = sp.getString(Constantes.SP_CNPJ_CLI, "Verificando CNPJ informado.");

        String url = Constantes.URL_ADICIONAR_GRUPO
                + "cgcCpf=" + cnpjCli
                + "&novoCnpj=" + novoCnpj;
        ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, context, response -> {
            JsonParser jsonParser = new JsonParser();
            JsonElement element = jsonParser.parse(response);
            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
            final String msg = element.getAsJsonObject().get("msg").getAsString();
            dialog.dismiss();
            switch (status) {
                case 97:
                    Alertas.loginInvalido2(context);
                    break;
                case 100:

                    Gson gson = new Gson();

                    ClienteGrupoEmpresa empresa = gson.fromJson(element.getAsJsonObject().get("ret"), ClienteGrupoEmpresa.class);
                    salvaGrupo(context, empresa);

                    final SweetAlertDialog alert = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
//                                alert.setCustomImage(R.drawable.robo_sorrindo);
                    alert.setTitleText("Sucesso");
                    alert.setContentText(msg.replace("\"\"", ""));
                    alert.setConfirmClickListener(sweetAlertDialog -> {
                        alert.dismiss();
                        Intent i = new Intent(context, MainActivity.class);
                        context.startActivity(i);
                    });
                    alert.show();


                    //sucesso
                    break;
                case 103:
                    // abreRSC(context, msg);

                    final SweetAlertDialog alert2 = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

                    alert2.setTitleText("");
                    alert2.setContentText(msg.replace("\"\"", ""));
                    alert2.setConfirmClickListener(sweetAlertDialog -> {
                        alert2.dismiss();
                        Intent i = new Intent(context, MainActivity.class);
                        context.startActivity(i);
                    });
                    alert2.show();
                    break;


                default:

                    if (status == 96) {
                        Alertas.sessaoExpirada(context, sp.getString(Constantes.SP_CNPJ_CLI_MASK, ""));
                    } else {
                        Alertas.erroInesperado(context, msg.replace("\"\"", ""));
                    }
                    break;
            }
        });

        dialog.dismiss();
        Alertas.erroComunicacaoServidor(context);


    }


    private static void clearUserSession(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(Constantes.SP_SESSION_ID, "");
        editor.putString(Constantes.SP_COD_CLI, "");
        editor.putString(Constantes.SP_CNPJ_CLI, "");
        editor.putString(Constantes.SP_EMAIL, "");
        editor.putString(Constantes.SP_SENHA_CLI, "");
        editor.putString(Constantes.SP_MENU_RESTRICOES, "");
        editor.putString(Constantes.SP_GRUPO_CLIENTE, "");

        MyFirebaseMessagingService.setBadge(context, 0);
        DatabaseHelper dh = new DatabaseHelper(context);
        try {
            TableUtils.clearTable(dh.getConnectionSource(), ClienteGrupoEmpresa.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ApplySharedPref")
    public static void saveUserSession(JsonObject element, String password, Context context) {
        clearUserSession(context);

        Gson gson = new Gson();
        ClienteLogado clienteLogado = gson.fromJson(element.get("ret"), ClienteLogado.class);

        for (JsonElement group : element.get("ret").getAsJsonObject().get("cliMesGru").getAsJsonArray()) {
            salvaGrupo(context, gson.fromJson(group, ClienteGrupoEmpresa.class));
        }

        SharedPreferences sp = AppController.AppContext.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString(Constantes.SP_SESSION_ID, clienteLogado.getSessionId());
        editor.putString(Constantes.SP_COD_CLI, clienteLogado.getCodigo());
        editor.putString(Constantes.SP_CNPJ_CLI, clienteLogado.getCgccpf());
        editor.putString(Constantes.SP_EMAIL, clienteLogado.getEmail());
        if (StringUtils.isNotEmpty(password))
            editor.putString(Constantes.SP_SENHA_CLI, password);
        editor.putString(Constantes.SP_MENU_RESTRICOES, element.get("ret").getAsJsonObject().get("menuRestricoesDTO").toString());
        editor.putString(Constantes.SP_GRUPO_CLIENTE, element.get("ret").getAsJsonObject().get("cliMesGru").toString());
        editor.commit();

        Intent intent = new Intent();
        intent.setClass(AppController.AppContext, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        AppController.AppContext.startActivity(intent);
    }


    private static void salvaGrupo(Context context, ClienteGrupoEmpresa empresa) {
        DatabaseHelper dh = new DatabaseHelper(context);

        try {
            ClienteGrupoEmpresaDao dao = new ClienteGrupoEmpresaDao(dh.getConnectionSource());
            dao.create(empresa);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public static List<ClienteGrupoEmpresa> getGrupo(Context context) {
        DatabaseHelper dh = new DatabaseHelper(context);

        try {
            ClienteGrupoEmpresaDao dao = new ClienteGrupoEmpresaDao(dh.getConnectionSource());
            return dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void updateBadge(final Context context, final int badgeLocal) {
//        startThread(new Runnable() {
//            @Override
//            public void run() {
        SincronizaBaseDados.badgeContador(context, badgeLocal);
//            }
//        });
    }

    public static void updateContasPanico(final Context context) {
        startThread(() -> SincronizaBaseDados.loadContasTipoAlarme(context));
    }

    @SuppressLint("ApplySharedPref")
    private static void loadContasTipoAlarme(final Context context) {

        try {

            final SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            String codcli = sp.getString(Constantes.SP_CNPJ_CLI, "");

            String url = Constantes.URL_LISTA_SEGURANCA_ELETRONICA
                    + "contractCode=" + codcli + "&cpfCnpj=" + Utils.getCnpjLogado(context);
            ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, context, response -> {
                JsonParser jsonParser = new JsonParser();
                JsonElement element = jsonParser.parse(response);
                int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
                if (status == 100) {
                    SharedPreferences sp1 = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp1.edit();
                    editor.putString(Constantes.SP_SEGURANCA_ELETRONICA, response);
                    editor.commit();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void startThread(Runnable run) {
        if (thread != null)
            thread.interrupt();
        thread = new Thread(run);
        thread.start();
    }


}
