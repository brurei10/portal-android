package orsegups.com.br.portaldocliente.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.volley.request.LoginRequest;


/**
 * Created by Orsegups on 22/02/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class LoginActivity extends AppCompatActivity {


    // UI references.
    private EditText loginEditText;
    private EditText passwordEditText;
    private Button btnLogin;
    private TextView forgotPassword;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        context = this;

        passwordEditText = findViewById(R.id.txt_password);
        loginEditText = findViewById(R.id.txt_login);
        btnLogin = findViewById(R.id.email_sign_in_button);
        forgotPassword = findViewById(R.id.login_forgot_password);
        forgotPassword.setPaintFlags(forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        initInstances();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    private void initInstances() {
        initBtnLogin();
        configureEditText();
        configureForgotPassword();
    }

    private void initBtnLogin() {

        btnLogin.setEnabled(false);
        btnLogin.setOnClickListener(v -> new LoginRequest(context, loginEditText.getText().toString(), passwordEditText.getText().toString()).exec());
    }

    private void configureEditText() {
        String login = getIntent().getStringExtra(Constantes.SESSAO_INPIRADA);
        if (login != null) {
            loginEditText.setText(login);
        }

        loginEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnLogin.setEnabled(configureBtnLogin(s.toString(), passwordEditText.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnLogin.setEnabled(configureBtnLogin(loginEditText.getText().toString(), s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void configureForgotPassword() {
        forgotPassword.setOnClickListener(v -> {
            Intent i = new Intent(LoginActivity.this, EsqueciSenhaActivity.class);
            i.putExtra("cnpj", loginEditText.getText().toString());

            startActivity(i);
        });
    }

    private boolean configureBtnLogin(String login, String password) {
        return !login.isEmpty() && !password.isEmpty();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

