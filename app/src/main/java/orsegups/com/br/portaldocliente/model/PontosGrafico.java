package orsegups.com.br.portaldocliente.model;

/**
 * Created by filipeamaralneis on 16/05/17.
 */

public class PontosGrafico {

    private String data;
    private long tempo;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getTempo() {
        return tempo;
    }

    public void setTempo(long tempo) {
        this.tempo = tempo;
    }
}
