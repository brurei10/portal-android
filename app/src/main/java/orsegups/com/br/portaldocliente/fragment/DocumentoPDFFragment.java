package orsegups.com.br.portaldocliente.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.adapter.DocumentoPDFAdapter;
import orsegups.com.br.portaldocliente.model.Arquivo;

public class DocumentoPDFFragment extends Fragment {
    private View emptyView;
    private List<Arquivo> mLista;
    private RecyclerView mRecyclerView;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_pdf_documento, container, false);
        emptyView = view.findViewById(R.id.empty_view);
        mRecyclerView = view.findViewById(R.id.recyclerView);

        configurarLista();
        return view;

    }

    private void configurarLista() {

        DocumentoPDFAdapter documentoPDFAdapter = new DocumentoPDFAdapter();
        documentoPDFAdapter.setLista(mLista);
        mRecyclerView.setAdapter(documentoPDFAdapter);
        LinearLayoutManager linear = new LinearLayoutManager(view.getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                linear.getOrientation());
        dividerItemDecoration.setOrientation(DividerItemDecoration.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divisao_linha));
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setLayoutManager(linear);
    }

    public void setLista(List<Arquivo> lista) {
        mLista = lista;
    }

}
