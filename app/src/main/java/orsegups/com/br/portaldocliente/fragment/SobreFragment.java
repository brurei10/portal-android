package orsegups.com.br.portaldocliente.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import orsegups.com.br.portaldocliente.R;


/**
 * Created by Orsegups on 15/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */
public class SobreFragment extends Fragment {


    private Button btnSobre;
    View view;


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sobre, container, false);

        btnSobre = (Button) view.findViewById(R.id.btn_sobre);

        btnSobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWebPage();
            }
        });




        return view;
    }

    public void openWebPage() {
        Uri webpage = Uri.parse("https://www.orsegups.com.br/");

        Intent intent = new Intent();
        intent.setData(webpage);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        view.getContext().startActivity(intent);
    }


}


