package orsegups.com.br.portaldocliente.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.Rat;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

public class Avaliacao {

    //BODY
    public TextView     textViewAvaliacao;
    public RatingBar    ratingBarAvaliacao;
    public LinearLayout ratAvaliacao;

    //AVALIADA
    public LinearLayout linearLayoutAvaliada;
    public RatingBar    ratingBarAvaliada;

    //MODAL
    TextView    textViewModalAvaliacao;
    RatingBar   ratingBarModalAvaliacao;
    EditText    editTextModalComentario;
    CheckBox    checkBoxModalAutorizacao;
    Button      buttonModalEnviar;

    //MODEL
    AlertDialog.Builder builder;
    AlertDialog dialog;

    public void init(final Context context, View view, final Rat rat) {

        ratAvaliacao            = (LinearLayout)    view.findViewById(R.id.row_timeline_rat_avaliacao_nova);
        linearLayoutAvaliada    = (LinearLayout)    view.findViewById(R.id.row_timeline_rat_avaliacao_avaliada);

        textViewAvaliacao       = (TextView)        view.findViewById(R.id.row_timeline_rat_text_rating);
        ratingBarAvaliacao      = (RatingBar)       view.findViewById(R.id.row_timeline_rat_rating_bar);

        ratingBarAvaliada       = (RatingBar)       view.findViewById(R.id.row_timeline_rat_rating_bar_avaliada);

        ratingBarAvaliacao.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                rat.setAvaliacaoNota((long) rating);
                rat.setAvaliacaoComentario("");
                rat.setAvaliacaoAutorizacao(false);

                if (SincronizaBaseDados.avaliacaoRat(context, rat)) {

                    textViewAvaliacao.setText(getRatingText(rating));
                    ratingBarAvaliacao.setOnRatingBarChangeListener(null);
                    ratingBar.setIsIndicator(true);

                    builder = new AlertDialog.Builder(context);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        dialog = builder.setTitle("Avaliação")
                                .setView(initModal(context, rating, rat))
                                .setOnDismissListener(new AlertDialog.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        setRatAvaliada((long) ratingBarAvaliacao.getRating());
                                    }
                                })
                                .create();
                    }
                    dialog.show();
                }
            }
        });

    }

    public View initModal(final Context context, Float rating, final Rat rat) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View modalAvaliacao = inflater.inflate(R.layout.modal_avaliacao,null);

        textViewModalAvaliacao = (TextView) modalAvaliacao.findViewById(R.id.modal_avaliacao_text);
        ratingBarModalAvaliacao = (RatingBar) modalAvaliacao.findViewById(R.id.modal_avaliacao_rating_bar);
        editTextModalComentario = (EditText) modalAvaliacao.findViewById(R.id.modal_avaliacao_comentario);
        checkBoxModalAutorizacao = (CheckBox) modalAvaliacao.findViewById(R.id.modal_avaliacao_checkbox_autorizacao);
        buttonModalEnviar = (Button) modalAvaliacao.findViewById(R.id.modal_avaliacao_button);

        ratingBarModalAvaliacao.setRating(rating);
        textViewModalAvaliacao.setText(getRatingText(rating));

        ratingBarModalAvaliacao.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating < 1f) {
                    ratingBar.setRating(1f);
                } else {
                    textViewModalAvaliacao.setText(getRatingText(rating));
                    textViewAvaliacao.setText(getRatingText(rating));

                    ratingBarAvaliacao.setRating(rating);
                }
            }
        });

        buttonModalEnviar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                rat.setAvaliacaoNota((long) ratingBarModalAvaliacao.getRating());
                rat.setAvaliacaoComentario(editTextModalComentario.getText().toString());
                rat.setAvaliacaoAutorizacao(checkBoxModalAutorizacao.isChecked());

                if (SincronizaBaseDados.avaliacaoRat(context, rat)) {
                    dialog.dismiss();
                }
            }
        });

        return modalAvaliacao;

    }

    public void setRatAvaliada(Long rating) {

        if (rating != null) {
            ratAvaliacao.setVisibility(View.GONE);
            linearLayoutAvaliada.setVisibility(View.VISIBLE);
            ratingBarAvaliada.setRating(rating);
        } else {
            ratAvaliacao.setVisibility(View.VISIBLE);
            linearLayoutAvaliada.setVisibility(View.GONE);
        }


    }

    public String getRatingText(float rating) {

        String textoAvaliacao = "Péssimo";
        switch ((int) rating) {
            case 2:
                textoAvaliacao = "Ruim";
                break;
            case 3:
                textoAvaliacao = "Regular";
                break;
            case 4:
                textoAvaliacao = "Bom";
                break;
            case 5:
                textoAvaliacao = "Excelente";
                break;
        }

        return "\"" + textoAvaliacao + "\"";

    }

}
