package orsegups.com.br.portaldocliente.model;

import java.math.BigDecimal;

/**
 * Created by filipe.neis on 19/04/2017.
 */

public class Boleto {

    private BigDecimal valTit;
    private String numTit;
    private Long codEmp;
    private Long codFil;
    private String linDig;
    private String lnkDow;
    private String datHorEnv ;
    private String datVen ;
    private String formaPagamento;
    private boolean situacao ;
    private String mLinkboleto;


    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public void setValTit(BigDecimal valTit) {
        this.valTit = valTit;
    }

    public void setNumTit(String numTit) {
        this.numTit = numTit;
    }

    public void setCodEmp(Long codEmp) {
        this.codEmp = codEmp;
    }

    public void setCodFil(Long codFil) {
        this.codFil = codFil;
    }

    public void setLinDig(String linDig) {
        this.linDig = linDig;
    }

    public void setLnkDow(String lnkDow) {
        this.lnkDow = lnkDow;
    }

    public void setDatHorEnv(String datHorEnv) {
        this.datHorEnv = datHorEnv;
    }

    public void setDatVen(String datVen) {
        this.datVen = datVen;
    }

    public BigDecimal getValTit() {
        return valTit;
    }

    public String getNumTit() {
        return numTit;
    }

    public String getLinDig() {
        return linDig;
    }

    public String getLnkDow() {
        return lnkDow;
    }

    public String getDatHorEnv() {
        return datHorEnv;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public String getDatVen() {
        return datVen;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public void setLinkboleto(String linkboleto) {
        mLinkboleto = linkboleto;
    }

    public String getLinkboleto() {
        return mLinkboleto;
    }
}
