package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class ArquivoSite implements Serializable {

    private static final long serialVersionUID = 1L;

    private String caminho;
    private String tipo;
    private Boolean mostrarSite;


    public ArquivoSite() {
        super();
    }

    public ArquivoSite(String caminho, String tipo, Boolean mostrarSite) {
        super();
        this.caminho = caminho;
        this.tipo = tipo;
        this.mostrarSite = mostrarSite;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Boolean getMostrarSite() {
        return mostrarSite;
    }

    public void setMostrarSite(Boolean mostrarSite) {
        this.mostrarSite = mostrarSite;
    }

    @NonNull
    @Override
    public String toString() {
        return "ArquivoSite [nome=" + caminho + ", tipo=" + tipo + ", mostrarSite=" + mostrarSite + "]";
    }
}
