package orsegups.com.br.portaldocliente.activity;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.adapter.RecyclerViewAdapter;
import orsegups.com.br.portaldocliente.model.Rat;

public class RatDetalhesActivity extends AppCompatActivity {


    private View parent_view;
    private CardView cardAgente;
    private ImageView fotoLocalAtendimento;
    private TextView local;
    private TextView data;
    private TextView evento;
    private TextView resultado;
    private TextView observacao;
    private RelativeLayout relativeLayoutObservacao;
    private LinearLayout viewData;

    private ImageLoader mImageLoader;


    private GridLayoutManager lLayout;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rat_details);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initImageLoader();
        inicializaObjetos();

        Rat rat = (Rat) getIntent().getSerializableExtra(Constantes.OBJ_RAT);


        MixpanelUtil.trackMessage(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_RAT, "RAT", this, rat.getEvento());


        String titulo = getIntent().getStringExtra("titulo");
        titulo = titulo.toLowerCase();
        titulo = titulo.substring(0, 1).toUpperCase() + titulo.substring(1);
        getSupportActionBar().setTitle(titulo);

        local.setText(rat.getLocal());
        evento.setText(rat.getEvento());
        resultado.setText(rat.getResultadoDoAtendimento());

        confData(rat);
        confImagemAtendente(rat);
        confImagemLocal(rat);
        confLerMasi(rat);
        confImagensCamera(rat);

    }

    private void inicializaObjetos() {

        parent_view = findViewById(android.R.id.content);
        cardAgente = findViewById(R.id.rat_card_agente);
        fotoLocalAtendimento = findViewById(R.id.rat_detalhe_foto);
        local = findViewById(R.id.rat_detalhe_local);
        data = findViewById(R.id.rat_detalhe_data);
        evento = findViewById(R.id.rat_detalhe_evento_valor);
        resultado = findViewById(R.id.rat_detalhe_resultado_valor);
        observacao = findViewById(R.id.rat_detalhe_observacao_valor);
        relativeLayoutObservacao = findViewById(R.id.relative_observaoca);
        viewData = findViewById(R.id.rat_view_data);
    }

    private void confData(Rat rat) {

        try {
            if (rat.getDataAtendimento() == null && rat.getDataAtendimento().isEmpty()) {
                viewData.setVisibility(View.GONE);
            } else {
                data.setText(Utils.getDataTimeLine(rat.getDataAtendimento(), rat.getHoraAtendimento()));
            }
        } catch (Exception e) {
            viewData.setVisibility(View.GONE);
        }
    }

    private void confImagemLocal(Rat rat) {

        try {

            if (rat.getFotoLocalLnk() != null && !rat.getFotoLocalLnk().trim().isEmpty() && !rat.getFotoLocalLnk().equalsIgnoreCase("null")) {
                setImage(fotoLocalAtendimento, rat.getFotoLocalLnk());
            } else {
                fotoLocalAtendimento.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            fotoLocalAtendimento.setVisibility(View.GONE);
            MixpanelUtil.trackMessage(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_RAT, "RAT-ERRO", this, rat.getEvento());
        }
    }

    private void confLerMasi(Rat rat) {

        try {

            if (rat.getObservacao() != null && !rat.getObservacao().trim().isEmpty() && !rat.getObservacao().equalsIgnoreCase("null")) {
                observacao.setText(Utils.getTextHtml(rat.getObservacao()));
                relativeLayoutObservacao.setVisibility(View.VISIBLE);
            } else {
                relativeLayoutObservacao.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            relativeLayoutObservacao.setVisibility(View.GONE);
            MixpanelUtil.trackMessage(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_RAT, "RAT-ERRO-ƒ", this, rat.getEvento());
        }

//        `
    }

    private void confImagensCamera(Rat rat) {

        if (rat.getLnkImg() != null && rat.getLnkImg().size() > 0) {

            lLayout = new GridLayoutManager(RatDetalhesActivity.this, 3);
            RecyclerView rView = findViewById(R.id.recycler_view);
            rView.setHasFixedSize(true);
            rView.setLayoutManager(lLayout);

            RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(this, rat.getLnkImg());
            rView.setAdapter(rcAdapter);


        }

    }

    private void confImagemAtendente(Rat rat) {

        try {
            if (rat.getFotoAitLnk() != null && !rat.getFotoAitLnk().trim().isEmpty() && !rat.getFotoAitLnk().equalsIgnoreCase("null")) {
                cardAgente.setVisibility(View.VISIBLE);

                ImageView fotoAtendente = findViewById(R.id.rat_detalhe_atendente_foto);//
                setImage(fotoAtendente, rat.getFotoAitLnk());

                TextView nomeAtendente = findViewById(R.id.rat_detalhe_atendente_nome);
                nomeAtendente.setText(rat.getAtendidoPor());
            }
        } catch (Exception e) {
            cardAgente.setVisibility(View.GONE);

            MixpanelUtil.trackMessage(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_RAT, "RAT-ERRO- IMAGEM ATENDENTE", this, rat.getEvento());
        }
    }

    private void setImage(ImageView image, String url) {
        mImageLoader.displayImage(url,
                image,
                null,
                new ImageLoadingListener() {

                    @Override
                    public void onLoadingCancelled(String uri, View view) {
                        //  Log.i("Script", "onLoadingCancelled()");
                    }

                    @Override
                    public void onLoadingComplete(String uri, View view, Bitmap bmp) {
                        //  Log.i("Script", "onLoadingComplete()");
                    }

                    @Override
                    public void onLoadingFailed(String uri, View view, FailReason fail) {
                        //Log.i("Script", "onLoadingFailed("+fail+")");
                    }

                    @Override
                    public void onLoadingStarted(String uri, View view) {
                        // Log.i("Script", "onLoadingStarted()");
                    }

                }, (uri, view, current, total) -> {

                    //Log.i("Script", "onProgressUpdate("+uri+" : "+total+" : "+current+")");
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            Snackbar.make(parent_view, item.getTitle() + " clicked", Snackbar.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        //    getMenuInflater().inflate(R.menu.menu_activity_recipe_details, menu);
        return true;
    }


    private void initImageLoader() {

        DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.erro_img)
                // .showImageOnLoading(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new FadeInBitmapDisplayer(1000))
                .build();

        ImageLoaderConfiguration conf = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(mDisplayImageOptions)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(conf);

    }

}
