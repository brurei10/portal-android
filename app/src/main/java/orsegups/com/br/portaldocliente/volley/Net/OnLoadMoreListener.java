package orsegups.com.br.portaldocliente.volley.Net;

/**
 * Created by filipeamaralneis on 02/08/17.
 */

public interface OnLoadMoreListener {

    void onLoadMore();
}
