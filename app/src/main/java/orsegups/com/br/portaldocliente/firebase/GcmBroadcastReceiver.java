package orsegups.com.br.portaldocliente.firebase;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.legacy.content.WakefulBroadcastReceiver;

import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

import static android.content.Context.MODE_PRIVATE;
import static orsegups.com.br.portaldocliente.firebase.MyFirebaseMessagingService.setBadge;

/**
 * Created by filipe.neis on 11/05/2017.
 */

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.


        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String cnpj = sp.getString(Constantes.SP_CNPJ_CLI, "");


        if (cnpj.isEmpty()) {
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancelAll();
        } else {
            try {
                SincronizaBaseDados.updateBadge(context, Constantes.BADGE_LOCAL_DEFAULT);
                int badge = Integer.parseInt(intent.getExtras().get("count").toString());
                setBadge(context, badge);

            } catch (Exception e) {

                setBadge(context, 1);
            }
        }

//        Log.i("Push", "ssss");
//        try {
//
//
//            // Start the service, keeping the device awake while it is launching.
//
//            SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
//            String cnpj = sp.getString(Constantes.SP_CNPJ_CLI, "");
//
//
//            if (!cnpj.isEmpty()) {
//                String cnpjPush = (String) intent.getExtras().get("cnpj");
////                ComponentName comp = new ComponentName(context.getPackageName(),
////                        MyFirebaseMessagingService.class.getName());
//                if (cnpj.equalsIgnoreCase(cnpjPush)) {
//
//                    Intent intent2 = new Intent(context, MainActivity.class);
//                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    try {
//                        String local = (String) intent.getExtras().get("tipoDocumento");
//                        intent2.putExtra(Constantes.SP_LOCAL_PUSH_NUMBER, local);
//                        Log.i("Local Push ---->", local);
//                    } catch (Exception e) {
//                        intent2.putExtra(Constantes.SP_LOCAL_PUSH_NUMBER, "0");
//                    }
//
//
//                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0/*Request code*/, intent2, PendingIntent.FLAG_ONE_SHOT);
//                    //Set sound of notification
//                    Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//                    Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
//                    String titulo = (String) intent.getExtras().get("gcm.notification.title");
//                    String body = (String) intent.getExtras().get("gcm.notification.body");
//
//                    NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(context)
//                            .setLargeIcon(icon)
//                            .setSmallIcon(R.drawable.icone_push)
//                            .setContentTitle(titulo)
//                            .setContentText(body)
//                            .setAutoCancel(true)
//                            .setSound(notificationSound)
//                            .setContentIntent(pendingIntent);
//
//
//                    int i = (int) (new Date().getTime() / 1000);
//                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//                    notificationManager.notify(i /*ID of notification*/, notifiBuilder.build());
//
//
//                    try {
//                        int badge = Integer.parseInt(intent.getExtras().get("count").toString());
//                        setBadge(context, badge);
//                    } catch (Exception e) {
//
//                        setBadge(context, 1);
//                    }
//
//
//                }
//            }
//
//
////            startWakefulService(context, (intent.setComponent(comp)));
////            setResultCode(Activity.RESULT_OK);
//
//        } catch (Exception e) {
//            Log.i("Push", "erro");
//        }
    }


}