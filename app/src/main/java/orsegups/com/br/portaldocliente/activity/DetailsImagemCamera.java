package orsegups.com.br.portaldocliente.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.adapter.RecyclerViewAdapter2;
import orsegups.com.br.portaldocliente.model.ImagemCameraRat;
import orsegups.com.br.portaldocliente.util.Utils;

public class DetailsImagemCamera extends AppCompatActivity {

    private GridLayoutManager mLLayout;
    private ImageLoader mImageLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_image);
        initImageLoader();

        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Imagens");

        Bitmap bitmap = Utils.getBitmap();
        ImageView imageView = findViewById(R.id.image);
        RecyclerView rView = findViewById(R.id.recycler_view);


        imageView.setImageBitmap(bitmap);


        try {
            String jsonLista = Utils.getListaImg();
            Gson gson = new Gson();
            List<ImagemCameraRat> itemList = new ArrayList<>();
            TypeToken<List<ImagemCameraRat>> token = new TypeToken<List<ImagemCameraRat>>() {
            };
            itemList = gson.fromJson(jsonLista, token.getType());
            mLLayout = new GridLayoutManager(DetailsImagemCamera.this, 3);

            rView.setHasFixedSize(true);
            rView.setLayoutManager(mLLayout);

            RecyclerViewAdapter2 rcAdapter = new RecyclerViewAdapter2(this, itemList, imageView);

            rView.setAdapter(rcAdapter);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initImageLoader() {

        DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.erro_img)
                // .showImageOnLoading(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new FadeInBitmapDisplayer(1000))
                .build();

        ImageLoaderConfiguration conf = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(mDisplayImageOptions)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(conf);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
