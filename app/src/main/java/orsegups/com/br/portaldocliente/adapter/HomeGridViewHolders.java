package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.model.Menu;

import static android.content.Context.MODE_PRIVATE;

public class HomeGridViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView icon;
    private TextView badge;
    private TextView title;
    private RelativeLayout itemLayout;

    private Context context;
    private Fragment fragment;
    private Menu menu;

    public HomeGridViewHolders(Context context, View itemView, Fragment fragment) {
        super(itemView);
        itemView.setOnClickListener(this);

        icon = itemView.findViewById(R.id.home_icone);
        badge = itemView.findViewById(R.id.home_badge);
        title = itemView.findViewById(R.id.home_titulo);
        itemLayout = itemView.findViewById(R.id.home_relative2);

        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public void onClick(View view) {
        if (menu.getFragment() != null) {
            openFragment(menu.getFragment(), menu.getNome());
        }
    }

    public void setMenu(Menu menu) {
        this.menu = menu;

        title.setText(menu.getNome());
        icon.setTag(menu.getId());
        icon.setImageDrawable(menu.getIcon());
        itemLayout.setBackground(menu.getBackground());

        updateBadge();
    }

    private void updateBadge() {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        int valor = sp.getInt(menu.getBadge(), 0);

        if (valor == 0) {
            badge.setVisibility(View.GONE);
        } else {
            badge.setVisibility(View.VISIBLE);
            badge.setText(String.valueOf(valor));
        }
    }

    private void openFragment(Fragment fragment, String title) {
        ((AppCompatActivity) context).getSupportActionBar().setTitle(title);

        this.fragment.getFragmentManager().beginTransaction()
                .replace(R.id.framelayout, fragment)
                .addToBackStack(null)
                .commit();
    }
}