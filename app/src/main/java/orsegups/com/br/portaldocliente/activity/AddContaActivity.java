package orsegups.com.br.portaldocliente.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.CpfCnpjMaks;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;


/**
 * Created by Orsegups on 03/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class AddContaActivity extends AppCompatActivity {


    private EditText mCnpjDesejado;

//    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_conta);
//        context = this;

        mCnpjDesejado = findViewById(R.id.add_conta_cnpj);

        MixpanelUtil.track(Utils.getCnpjLogado(this), Constantes.MIXPANEL_ADD_CONTA, "ADD CONTA", this);


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        mCnpjDesejado.addTextChangedListener(CpfCnpjMaks.insert(mCnpjDesejado));
    }


    public void salvarAlteracoes(View v) {

        String novoCnpj = mCnpjDesejado.getText().toString();
        novoCnpj = novoCnpj.replace(".", "");
        novoCnpj = novoCnpj.replace("/", "");
        novoCnpj = novoCnpj.replace("-", "");

        if (!novoCnpj.isEmpty() && novoCnpj.length() >= 13) {
            SincronizaBaseDados.novaEmpresaGrupo(this, novoCnpj);
        } else {
            Toast.makeText(this, "Informe um CNPJ valido", Toast.LENGTH_LONG).show();
        }


    }

}



