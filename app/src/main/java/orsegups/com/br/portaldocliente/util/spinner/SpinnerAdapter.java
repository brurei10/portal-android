package orsegups.com.br.portaldocliente.util.spinner;

import android.content.Context;

import java.util.List;
/**
 * Created by Orsegups on 08/11/16.<br>
 * Orsegups(link www.Orsegups.com.br)<br>
 *
 * @author José Mário
 * @version Orsegups 1.0
 */
public class SpinnerAdapter <T> extends SpinnerBaseAdapter {

    private final List<T> items;

    public SpinnerAdapter(Context context, List<T> items) {
        super(context);
        this.items = items;
    }

    @Override public int getCount() {
        return items.size() - 1;
    }

    @Override public T getItem(int position) {
        if (position >= getSelectedIndex()) {
            return items.get(position + 1);
        } else {
            return items.get(position);
        }
    }

    @Override public T get(int position) {
        return items.get(position);
    }

    @Override public List<T> getItems() {
        return items;
    }

}
