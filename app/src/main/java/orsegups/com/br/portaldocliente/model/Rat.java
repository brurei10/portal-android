package orsegups.com.br.portaldocliente.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by filipe.neis on 20/04/2017.
 */

public class Rat implements Serializable {

    private Long id;
    private String informativo;
    private String resultado;
    private String resultadoDoAtendimento;
    private String atendidoPor;
    private String local;
    private String dataAtendimento;
    private String horaAtendimento;
    private String evento;
    private String observacao;
    private String fotoAitBase64;
    private String fotoAitLnk;
    private String fotoLocalLnk;
    private String fotoLocalBase64;
    private long cgcCpf;
    private long tipRat;
    private Long neoId;
    private Long avaliacaoNota;
    private String avaliacaoComentario;
    private boolean avaliacaoAutorizacao;
    private String avaliacaoDatahora;
    private List<ImagemCameraRat> lnkImg;


    public Rat() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInformativo() {
        return informativo;
    }

    public String getResultado() {
        return resultado;
    }

    public String getResultadoDoAtendimento() {
        return resultadoDoAtendimento;
    }

    public String getAtendidoPor() {
        return atendidoPor;
    }

    public String getLocal() {
        return local;
    }

    public String getDataAtendimento() {
        return dataAtendimento;
    }

    public String getHoraAtendimento() {
        return horaAtendimento;
    }

    public String getEvento() {
        return evento;
    }

    public String getObservacao() {
        return observacao;
    }

    public long getCgcCpf() {
        return cgcCpf;
    }

    public long getTipRat() {
        return tipRat;
    }

    public String getFotoAitLnk() {
        return fotoAitLnk;
    }

    public String getFotoLocalLnk() {
        return fotoLocalLnk;
    }

    public Long getNeoId() {
        return neoId;
    }

    public void setNeoId(Long neoId) {
        this.neoId = neoId;
    }

    public Long getAvaliacaoNota() {
        return avaliacaoNota;
    }

    public void setAvaliacaoNota(Long avaliacaoNota) {
        this.avaliacaoNota = avaliacaoNota;
    }

    public String getAvaliacaoComentario() {
        return avaliacaoComentario;
    }

    public void setAvaliacaoComentario(String avaliacaoComentario) {
        this.avaliacaoComentario = avaliacaoComentario;
    }

    public boolean getAvaliacaoAutorizacao() {
        return avaliacaoAutorizacao;
    }

    public void setAvaliacaoAutorizacao(boolean avaliacaoAutorizacao) {
        this.avaliacaoAutorizacao = avaliacaoAutorizacao;
    }

    public String getAvaliacaoDatahora() {
        return avaliacaoDatahora;
    }

    public List<ImagemCameraRat> getLnkImg() {
        return lnkImg;
    }
}


