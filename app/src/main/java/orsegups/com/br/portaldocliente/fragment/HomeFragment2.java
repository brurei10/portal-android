package orsegups.com.br.portaldocliente.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.adapter.HomeGridViewAdapter;
import orsegups.com.br.portaldocliente.model.Menu;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

public class HomeFragment2 extends Fragment {

    View view;
    private GridLayoutManager lLayout;
    private Timer timer;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home_grid, container, false);
        RecyclerView rView = view.findViewById(R.id.recyclerView);
        lLayout = new GridLayoutManager(view.getContext(), 3);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        List<Menu> menuList = Utils.getMenuFilteredByPermissions(view.getContext());
        HomeGridViewAdapter rcAdapter = new HomeGridViewAdapter(view.getContext(), menuList, this);
        rView.setAdapter(rcAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        SincronizaBaseDados.badgeContador(view.getContext(), 0);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((RecyclerView) view.findViewById(R.id.recyclerView)).getAdapter().notifyDataSetChanged();
                    }
                });
            }
        }, 0, 1000);

    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        timer.purge();
    }

}


