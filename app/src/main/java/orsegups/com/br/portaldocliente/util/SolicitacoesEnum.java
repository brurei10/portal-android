package orsegups.com.br.portaldocliente.util;

/**
 * Created by Orsegups on 06/03/2017.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author filipe.neis
 */
public enum SolicitacoesEnum {
    SELECIONE("Selecione",0),
    SUGESTAO("Sugestão",11715124),
    ORCAMENTO("Orçamento",11715150),
    RECLAMACAO("Reclamação",11715151),
    ELOGIO("Elogio",1028885044),
    DUVIDA("Dúvida",12429911),
    ATUALIZACAO_CADASTRAL("Atualização Cadastral",12430375),
    SOLICITACAO("Solicitação",127514862),
    OUTROS("Outros",209568153);


    private String nome;
    private int neoId;

    SolicitacoesEnum(String nome, int neoId){
        this.nome = nome;
        this.neoId = neoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNeoId() {
        return neoId;
    }

    public void setNeoId(int neoId) {
        this.neoId = neoId;
    }

    @Override
    public String toString(){
        return  nome;
    }
}
