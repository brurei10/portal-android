package orsegups.com.br.portaldocliente.model;

import java.util.List;

public class Reclamacao {
    private Long neoid;
    private String codigoTarefa;
    private String processo;
    private String solicitante;
    private String dataInicio;
    private String dataConclusao;
    private String situacao;
    private String link;
    private String textoReclamacao;
    private String origem;
    private String atendente;
    private String contato;
    private String satisfacao;
    private List<HistoricoReclamacao> verificacoesDeEficacia;

    public Long getNeoid() {
        return neoid;
    }

    public void setNeoid(Long neoid) {
        this.neoid = neoid;
    }

    public String getCodigoTarefa() {
        return codigoTarefa;
    }

    public void setCodigoTarefa(String codigoTarefa) {
        this.codigoTarefa = codigoTarefa;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(String dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTextoReclamacao() {
        return textoReclamacao;
    }

    public void setTextoReclamacao(String textoReclamacao) {
        this.textoReclamacao = textoReclamacao;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getAtendente() {
        return atendente;
    }

    public void setAtendente(String atendente) {
        this.atendente = atendente;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getSatisfacao() {
        return satisfacao;
    }

    public void setSatisfacao(String satisfacao) {
        this.satisfacao = satisfacao;
    }

    public List<HistoricoReclamacao> getVerificacoesDeEficacia() {
        return verificacoesDeEficacia;
    }

    public void setVerificacoesDeEficacia(List<HistoricoReclamacao> verificacoesDeEficacia) {
        this.verificacoesDeEficacia = verificacoesDeEficacia;
    }
}
