package orsegups.com.br.portaldocliente.volley.request;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.fragment.InviteUserFragment;
import orsegups.com.br.portaldocliente.volley.common.AbstractRequest;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.UserAccountDTO;

import static android.content.Context.MODE_PRIVATE;

public class UserAccountByCnpjRequest extends AbstractRequest {

    private String cpfCnpj;
    private Object parent;

    public UserAccountByCnpjRequest(Context context, String cpfCnpj, Object parent) {
        super(context);
        this.cpfCnpj = cpfCnpj;
        this.parent = parent;
    }

    public void exec() {
        awaitDialog.show();

        String url = Constantes.URL_USER_ACCOUNTS_BY_CNPJ + "?cpfCnpj=" + cpfCnpj + "&product=" + Constantes.PORTAL_PRODUCT_ID;

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        JsonParser parser = new JsonParser();
                        JsonElement mJson =  parser.parse(response);
                        Gson gson = new Gson();
                        UserAccountDTO userAccountDTO = gson.fromJson(mJson, UserAccountDTO.class);

                        List<AccountDTO> accounts = userAccountDTO.getData();

                        awaitDialog.dismiss();

                        if (parent instanceof InviteUserFragment) {
                            ((InviteUserFragment) parent).configureFeatures(accounts);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("d", "Erro");
                        awaitDialog.dismiss();

                        Alertas.invalidPassword(context);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + token);
                params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

}
