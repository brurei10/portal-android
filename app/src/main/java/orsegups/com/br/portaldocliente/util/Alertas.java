package orsegups.com.br.portaldocliente.util;

import android.content.Context;
import android.content.Intent;
import android.widget.Button;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.MainActivity;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.activity.LoginActivity;

/**
 * Created by Orsegups on 22/02/2017.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author filipe.neis
 */
public class Alertas {

    public static void success(final Context context, String msg, SweetAlertDialog.OnSweetClickListener confirm) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Sucesso..")
                .setContentText(msg)
                .setConfirmClickListener(confirm);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void error(Context context, String msg) {
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Ops..")
                .setContentText(msg)
                .show();
    }

    public static void unexpectedError(Context context) {
        error(context, context.getString(R.string.alert_unexpected_error));
    }


    public static SweetAlertDialog wait(Context context, String msg) {
        return wait(context, msg, true);
    }

    public static SweetAlertDialog wait(Context context, String msg, boolean show) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        dialog.setTitleText("Aguarde...");
        dialog.setContentText(msg);
        dialog.getProgressHelper().setBarColor(context.getResources().getColor(R.color.colorPrimary));
        dialog.setCancelable(false);
        if (show) dialog.show();
        return dialog;
    }

    public static void erroComunicacaoServidor(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Erro")
                .setContentText(context.getString(R.string.alerta_erro_servidor))
                .show();
    }


    public static void erroInesperado(Context context, String msg) {
        error(context, msg);
    }

    public static SweetAlertDialog genericConfirmDialog(Context context, String title, String message) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

        sweetAlertDialog
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("Sim")
                .setCancelText("Não");

        sweetAlertDialog.show();

        sweetAlertDialog.findViewById(R.id.confirm_button)
                .setBackgroundColor(context.getResources().getColor(R.color.blue_btn_bg_color));

        sweetAlertDialog.findViewById(R.id.cancel_button)
                .setBackgroundColor(context.getResources().getColor(R.color.gray_btn_bg_color));

        return sweetAlertDialog;

    }

    public static SweetAlertDialog msgSair(Context context) {

        SweetAlertDialog d = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

        d.setTitleText("");
        d.setContentText("Deseja realmente sair ?");
        d.setConfirmText("Sim");
        d.setCancelText("Não");
        d.show();

        Button btn = (Button) d.findViewById(R.id.confirm_button);
        Button btnC = (Button) d.findViewById(R.id.cancel_button);
        btn.setBackgroundColor(context.getResources().getColor(R.color.blue_btn_bg_color));
        btnC.setBackgroundColor(context.getResources().getColor(R.color.gray_btn_bg_color));

        return d;
    }

    public static void changePasswordFailed(final Context context, final Intent intent) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Ops..")
                .setContentText(context.getString(R.string.alert_change_password_failed))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        context.startActivity(intent);
                    }
                })
                .show();

    }

    public static void changedPassword(final Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Sucesso..")
                .setContentText(context.getString(R.string.alert_change_password_success))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(context, MainActivity.class);
                        context.startActivity(intent);
                    }
                })
                .show();
    }

    public static void invalidPassword(Context context) {
        error(context, context.getString(R.string.alert_invalid_password));
    }

    public static void invalidAccounts(Context context) {
        error(context, context.getString(R.string.alert_error_account));
    }

    public static void resetPassword(final Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Sucesso..")
                .setContentText(context.getString(R.string.alert_reset_password_success))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                })
                .show();
    }

    public static void loginInvalido(final Context context, final Intent intent) {

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Ops..")
                .setContentText(context.getString(R.string.alerta_login_invalido))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        context.startActivity(intent);
                    }
                })
                .show();

    }

    public static void loginInvalido2(Context context) {
        error(context, context.getString(R.string.alerta_login_invalido));
    }

    public static void sessaoExpirada(final Context context, final String cnpjMask) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
            .setTitleText("Ops..")
            .setContentText(context.getString(R.string.alerta_sessao_expirada))
            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.putExtra(Constantes.SESSAO_INPIRADA, cnpjMask);
                    context.startActivity(intent);
                }
            });
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void codigoCopiado(final Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Sucesso..")
                .setContentText(context.getString(R.string.alerta_codigo_copiado_sucesso))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public static SweetAlertDialog enviandoSolicitacaReclamacao(Context context, int local) {
        String msg = "";

        if (local == 1)
            msg = "Enviando sua Reclamação...";
        else
            msg = "Enviando sua Solicitação...";

        SweetAlertDialog caixaMenssagem = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        caixaMenssagem.setTitleText("Aguarde");
        caixaMenssagem.setContentText(msg);
        caixaMenssagem.getProgressHelper().setBarColor(context.getResources().getColor(R.color.colorPrimary));
        caixaMenssagem.show();

        return caixaMenssagem;
    }

    public static SweetAlertDialog msgAcionarConta(Context context, String msg) {

        SweetAlertDialog d = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);

        d.setTitleText("Acionar Pânico");
        d.setContentText("Deseja realmente acionar pânico para conta: " + msg.toUpperCase());
        d.setConfirmText("Sim");
        d.setCancelText("Não");
        d.show();

        Button btn = (Button) d.findViewById(R.id.confirm_button);
        Button btnC = (Button) d.findViewById(R.id.cancel_button);
        btn.setBackgroundColor(context.getResources().getColor(R.color.blue_btn_bg_color));
        btnC.setBackgroundColor(context.getResources().getColor(R.color.gray_btn_bg_color));


        return d;
    }

}
