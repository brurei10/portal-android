package orsegups.com.br.portaldocliente.model;

import java.io.Serializable;

/**
 * Created by filipeamaralneis on 24/05/17.
 */

public class ImagemCameraRat implements Serializable {

    private int id;
    private String lnkImg;


    public int getId() {
        return id;
    }

    public String getLnkImg() {
        return lnkImg;
    }
}
