package orsegups.com.br.portaldocliente.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.FeatureDTO;
import orsegups.com.br.portaldocliente.volley.dto.ProductDTO;
import orsegups.com.br.portaldocliente.volley.dto.UserDTO;
import orsegups.com.br.portaldocliente.volley.request.InviteUserRequest;

public class InviteUserFragment extends Fragment {

    // UI references.
    private EditText email;
    private Context context;
    private LinearLayout linearLayoutFeatures;
    private String cpfCnpj = "";

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_user, container, false);

        context = view.getContext();

        email = view.findViewById(R.id.email);
        linearLayoutFeatures = view.findViewById(R.id.linearLayoutFeatures);

        Button btnInvite = view.findViewById(R.id.btn_invite);
        btnInvite.setOnClickListener(v -> inviteUser());

        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);

        try {

            UserDTO user = new ObjectMapper().readValue(sp.getString(Constantes.SP_USER_ORSEGUPS_ID, ""), UserDTO.class);
            List<AccountDTO> accounts = new ArrayList<>();

            for (AccountDTO accountDTO : user.getProduct().getAccounts()) {
                if (accountDTO.getCpfCnpjOwner().equals(sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "")) && accountDTO.getIsAdmin()) {
                    accounts.add(accountDTO);
                }
            }

            configureFeatures(accounts);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void configureFeatures(final List<AccountDTO> accounts) {

        final SweetAlertDialog dialog = Alertas.wait(context, "");

        final Resources res = this.getResources();

        new Handler().postDelayed(() -> {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 16, 0, 16);

            List<Integer> featuresId = Arrays.asList(1, 7, 9, 10, 11, 12, 13);
            boolean iniciarAberto = accounts.size() == 1;
            for (AccountDTO accountDTO : accounts) {

                LinearLayout linearLayoutAccount = new LinearLayout(context);
                linearLayoutAccount.setOrientation(LinearLayout.VERTICAL);
                LinearLayout linearLayoutNome = new LinearLayout(context);
                linearLayoutNome.setOrientation(LinearLayout.HORIZONTAL);

                TextView textView = new TextView(context);
                textView.setText(accountDTO.getIdentifier());
                if ((accountDTO.getProductName() != null) && (!accountDTO.getProductName().isEmpty())) {
                    textView.setText(accountDTO.getIdentifier() + " - " + accountDTO.getProductName());
                }


                textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                textView.setTextColor(Color.WHITE);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                linearLayoutNome.addView(textView);
//                linearLayoutNome.addView(imageView);
                linearLayoutAccount.addView(linearLayoutNome);
                //Colocado o touch de abirr e fechar no texto para não ter problema ao tentar adicionar ou retiar permissão
                textView.setOnTouchListener((view, motionEvent) -> {
                    LinearLayout permissions = (LinearLayout) linearLayoutAccount.getChildAt(1);
                    Drawable seta = null;
                    if (permissions.getVisibility() != View.VISIBLE) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            seta = getContext().getDrawable(R.drawable.ic_arrow_drop_down);
                            seta.setTint(Color.WHITE);
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, seta, null);
                        }
                        permissions.setVisibility(View.VISIBLE);
                    } else {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            seta = getContext().getDrawable(R.drawable.ic_arrow_right);
                            seta.setTint(Color.WHITE);
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, seta, null);
                        }
                        permissions.setVisibility(View.GONE);
                    }
                    return false;
                });

                LinearLayout linearLayoutPermissions = new LinearLayout(context);
                linearLayoutPermissions.setShowDividers(LinearLayout.SHOW_DIVIDER_BEGINNING | LinearLayout.SHOW_DIVIDER_MIDDLE | LinearLayout.SHOW_DIVIDER_END);
                linearLayoutPermissions.setDividerDrawable(res.getDrawable(R.drawable.divider));
                linearLayoutPermissions.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                linearLayoutPermissions.setOrientation(LinearLayout.VERTICAL);
                LayoutTransition layoutTransition = new LayoutTransition();
                ObjectAnimator appearingAnim = ObjectAnimator.ofFloat(null, "rotationY", 0f, 90f, 180f, 270f, 359.9999f);
                appearingAnim.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator anim) {
                        View view = (View) ((ObjectAnimator) anim).getTarget();
                        view.setRotationY(0f);
                    }
                });
                layoutTransition.setAnimator(LayoutTransition.APPEARING, appearingAnim);
                layoutTransition.setDuration(LayoutTransition.APPEARING, 2000L);
                layoutTransition.setStartDelay(LayoutTransition.APPEARING, 0);
                linearLayoutPermissions.setLayoutTransition(layoutTransition);

                Switch switchAdmin = new Switch(new ContextThemeWrapper(context, R.style.PortalSwitch));
                switchAdmin.setLayoutParams(layoutParams);
                switchAdmin.setText("ADMINISTRADOR");
                switchAdmin.setTextColor(Color.WHITE);
                switchAdmin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                switchAdmin.setChecked(false);
                switchAdmin.setTag(accountDTO.getId() + "|0");

                linearLayoutPermissions.addView(switchAdmin);

                for (FeatureDTO featureDTO : accountDTO.getFeatures()) {

                    Switch sw = new Switch(new ContextThemeWrapper(context, R.style.PortalSwitch));
                    sw.setLayoutParams(layoutParams);
                    sw.setText(featureDTO.getName());
                    sw.setTextColor(Color.WHITE);
                    sw.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                    sw.setChecked(false);
                    sw.setTag(accountDTO.getId() + "|" + featureDTO.getId());
                    if (!featuresId.contains(featureDTO.getId()))
                        sw.setVisibility(View.GONE);

                    linearLayoutPermissions.addView(sw);

                }

                linearLayoutAccount.addView(linearLayoutPermissions);

                linearLayoutFeatures.addView(linearLayoutAccount);
                LinearLayout permissions = (LinearLayout) linearLayoutAccount.getChildAt(1);
                Drawable seta = null;
                if (iniciarAberto) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        seta = getContext().getDrawable(R.drawable.ic_arrow_drop_down);
                        seta.setTint(Color.WHITE);
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, seta, null);
                    }

                    permissions.setVisibility(View.VISIBLE);

                } else {

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        seta = getContext().getDrawable(R.drawable.ic_arrow_right);
                        seta.setTint(Color.WHITE);
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, seta, null);
                    }
                    permissions.setVisibility(View.GONE);

                }

            }

            dialog.dismissWithAnimation();
        }, 800);

    }

    private ProductDTO getProductWithSelectedFeatures() {

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(Long.valueOf(Constantes.PORTAL_PRODUCT_ID));
        productDTO.setAccounts(new ArrayList<>());

        final int childCount = linearLayoutFeatures.getChildCount();
        for (int i = 0; i < childCount; i++) {

            View view = linearLayoutFeatures.getChildAt(i);

            if (view instanceof LinearLayout) {

                LinearLayout linearLayout = (LinearLayout) view;

                final int childSwitchCount = linearLayout.getChildCount();
                for (int j = 0; j < childSwitchCount; j++) {

                    View viewSwitch = linearLayout.getChildAt(j);

                    if (viewSwitch instanceof Switch) {

                        Switch sw = (Switch) viewSwitch;

                        if (sw.isChecked()) {

                            String[] splitTag = sw.getTag().toString().split("\\|");

                            Utils.addProfileToProduct(productDTO, Long.parseLong(splitTag[0]), Integer.parseInt(splitTag[1]));
                        }

                    }
                }
            }
        }

        return productDTO;
    }

    private void inviteUser() {
        if (validateSubmit()) {
            new InviteUserRequest(context, cpfCnpj, getProductWithSelectedFeatures(), email.getText().toString()).exec();
        }
    }

    private boolean validateSubmit() {
        String emailText = email.getText().toString();

        if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            Alertas.error(context, context.getString(R.string.alert_invalid_email));
            return false;
        }

        return true;
    }

}