package orsegups.com.br.portaldocliente.volley.request;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import orsegups.com.br.portaldocliente.MainActivity;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.JsonConverterUtil;
import orsegups.com.br.portaldocliente.util.PermissionUtils;
import orsegups.com.br.portaldocliente.activity.FirstLoginActivity;
import orsegups.com.br.portaldocliente.volley.Net.AppController;
import orsegups.com.br.portaldocliente.volley.common.AbstractRequest;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.LoginDTO;
import orsegups.com.br.portaldocliente.volley.dto.ProductDTO;
import orsegups.com.br.portaldocliente.volley.dto.UserAccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.UserDTO;

import static android.content.Context.MODE_PRIVATE;

class UserAccountRequest extends AbstractRequest {

    private Context context;
    private LoginDTO loginDTO;
    private String password;

    UserAccountRequest(Context context, LoginDTO login, String password) {
        super(context);
        this.context = context;
        this.loginDTO = login;
        this.password = password;
    }

    void exec() {
        awaitDialog.show();

        String url = Constantes.URL_USER_ACCOUNTS;

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        JsonParser parser = new JsonParser();
                        JsonElement mJson = parser.parse(response);
                        Gson gson = new Gson();
                        UserAccountDTO userAccountDTO = gson.fromJson(mJson, UserAccountDTO.class);

                        List<AccountDTO> accounts = userAccountDTO.getData();

                        Collections.sort(accounts);

                        awaitDialog.dismiss();


                        UserDTO user = loginDTO.getUser();
                        ProductDTO productDTO = new ProductDTO();
                        productDTO.setAccounts(accounts);
                        user.setProduct(productDTO);
                        AccountDTO account = getUserAccountOrFirst(user.getProduct().getAccounts(), user.getCpfCnpj());
                        PermissionUtils.storePermissions(context, account);

                        SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE).edit();
                        editor.putString(Constantes.SP_USER_ORSEGUPS_ID, JsonConverterUtil.toString(user));
                        editor.putString(Constantes.SP_EMAIL_ORSEGUPS_ID, user.getEmail());
                        editor.putString(Constantes.SP_CNPJ_CLI, account.getContractCode());
                        String cpfCnpj = account != null ? account.getCpfCnpjOwner() : user.getCpfCnpj();
                        editor.putString(Constantes.SP_LOGIN_ORSEGUPS_ID, cpfCnpj);
                        editor.commit();

                        if (user.getFirstAccess()) {
                            awaitDialog.dismiss();
                            Intent intent = new Intent();
                            intent.setClass(context, FirstLoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("currentPassword", password);
                            context.startActivity(intent);
                        } else {

                            Intent intent = new Intent();
                            intent.setClass(AppController.AppContext, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            AppController.AppContext.startActivity(intent);

                            //REMOVIDO
                            //new GetUserRequest(context, cpfCnpj, password, awaitDialog).exec();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("d", "Erro");
                        awaitDialog.dismiss();

                        Alertas.invalidAccounts(context);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + token);
                params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                return params;
            }

        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    private AccountDTO getUserAccountOrFirst(List<AccountDTO> accounts, String userCpfCnpj) {
        if (accounts.size() > 0) {
            if (StringUtils.isNotEmpty(userCpfCnpj)) {
                for (AccountDTO account : accounts)
                    if (userCpfCnpj.equals(account.getCpfCnpjOwner()))
                        return account;
            }
            return accounts.get(0);
        }
        return null;
    }


}
