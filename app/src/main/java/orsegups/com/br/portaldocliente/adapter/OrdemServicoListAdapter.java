package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.OrdemServicoSite;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class OrdemServicoListAdapter extends RecyclerView.Adapter<OrdemServicoListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<OrdemServicoSite> original_items = new ArrayList<>();
    private List<OrdemServicoSite> filtered_items = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public OrdemServicoListAdapter(Context context, List<OrdemServicoSite> items) {
        original_items = items;
        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public OrdemServicoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ordem_servico, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        final OrdemServicoSite ordemServico = filtered_items.get(position);

        String status = "";
        if (ordemServico.getFechado().equals("1")) {
            status = "Finalizado";
        } else {
            status = "Em execução";
        }

        holder.situacao.setText(status);
        holder.dtInicio.setText(ordemServico.getAbertura().substring(0, 10));

        try {
            holder.dtFim.setText(ordemServico.getFechamento().substring(0, 10));
        } catch (Exception e) {

            holder.dtFim.setText("");
        }

        holder.descricao.setText(ordemServico.getDefeito());
        holder.codigoTarefa.setText(ordemServico.getIdOrdem().toString());


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView descricao;
        // each nfeData item is just a string in this case
        TextView codigoTarefa;
        TextView situacao;
        TextView dtInicio;
        TextView dtFim;


        public ViewHolder(View v) {
            super(v);

            dtInicio = v.findViewById(R.id.row_ordem_servico_dt_inicio);
            codigoTarefa = v.findViewById(R.id.row_ordem_servico_codigo_tarefa);
            situacao = v.findViewById(R.id.row_ordem_servico_situacao);
            dtFim = v.findViewById(R.id.row_ordem_servico_dt_fim);
            descricao = v.findViewById(R.id.row_ordem_servico_descricao);


        }
    }


}