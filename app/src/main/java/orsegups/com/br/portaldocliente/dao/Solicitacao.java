package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;


public class Solicitacao extends BaseDaoImpl<Solicitacao, Integer> {
    public Solicitacao(ConnectionSource cs) throws SQLException {
        super(Solicitacao.class);
        setConnectionSource(cs);
        initialize();
    }
}
