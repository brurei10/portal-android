package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.Menu;

public class HomeGridViewAdapter extends RecyclerView.Adapter<HomeGridViewHolders> {

    private List<Menu> itemList;
    private Context context;
    private Fragment fragment;


    public HomeGridViewAdapter(Context context, List<Menu> itemList, Fragment fragment) {
        this.itemList = itemList;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public HomeGridViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_home, null);
        return new HomeGridViewHolders(context, layoutView, fragment);
    }

    @Override
    public void onBindViewHolder(HomeGridViewHolders holder, int position) {
        holder.setMenu(itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


}

