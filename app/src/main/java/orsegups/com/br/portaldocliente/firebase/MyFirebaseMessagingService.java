package orsegups.com.br.portaldocliente.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import orsegups.com.br.portaldocliente.MainActivity;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

/**
 * Created by NgocTri on 8/9/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            Log.e("classname", "null");
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
//        Log.d(TAG, "New Token: " + s);
        SharedPreferences sp = getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constantes.SP_TOKEN_CLI, s);
        editor.commit();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//        Log.d(TAG, "FROM:" + remoteMessage.getFrom());


        if (remoteMessage.getNotification() != null) {

            SharedPreferences sp = getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            String cnpj = sp.getString(Constantes.SP_CNPJ_CLI, "");
            String cnpjPush = remoteMessage.getData().get("cnpj");

            if (cnpj != null && !cnpj.isEmpty()) {
                if (cnpj.equalsIgnoreCase(cnpjPush)) {
                    SincronizaBaseDados.updateBadge(getApplicationContext(), Constantes.BADGE_LOCAL_DEFAULT);
                    sendNotification(remoteMessage);
                    try {
                        int badge = Integer.parseInt(remoteMessage.getData().get("count"));
                        setBadge(getApplicationContext(), badge);
                    } catch (Exception e) {

                        setBadge(getApplicationContext(), 1);
                    }
                }
            }
        }
    }

    @Override
    public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        return super.registerReceiver(receiver, filter);
    }

    /**
     * Dispay the notification
     *
     * @param remoteMessage
     */


    private void sendNotification(RemoteMessage remoteMessage) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            String local = remoteMessage.getData().get("tipoDocumento");
            intent.putExtra(Constantes.SP_LOCAL_PUSH_NUMBER, local);
            Log.i("Local Push ---->", local);
        } catch (Exception e) {

            if (remoteMessage.getData().get("type") != null) {
                if (remoteMessage.getData().get("type").equals("message")) {
                    intent.putExtra(Constantes.SP_LOCAL_PUSH_NUMBER, "5");
                } else {
                    intent.putExtra(Constantes.SP_LOCAL_PUSH_NUMBER, "0");
                }
            } else {
                intent.putExtra(Constantes.SP_LOCAL_PUSH_NUMBER, "0");
            }


        }


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.icone_push);
        String titulo = remoteMessage.getNotification().getTitle();
        String body = null;
        try {
            body = URLDecoder.decode(remoteMessage.getNotification().getBody(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            body = "";
        }

        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this);
        notifiBuilder.setSmallIcon(R.drawable.icone_push);
        notifiBuilder.setLargeIcon(icon);
        // notifiBuilder.addAction(R.drawable.ic_launcher, "Ver agora", pendingIntent);
        notifiBuilder.setContentTitle(titulo);
        notifiBuilder.setContentText(body);
        notifiBuilder.setAutoCancel(true);
        notifiBuilder.setSound(notificationSound);
        notifiBuilder.setContentIntent(pendingIntent);

//        notifiBuilder.setStyle(new Notification.InboxStyle()
//        .a;


        remoteMessage.getMessageId();
        int i = (int) (new Date().getTime() / 1000);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(i /*ID of notification*/, notifiBuilder.build());


    }
}