package orsegups.com.br.portaldocliente.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import orsegups.com.br.portaldocliente.R;

public class ViewPDFActivity extends Activity {

    private static final int REQUEST = 112;
    public static final String NOME_ARQUIVO = "NomeArquivo";
    public static final String PDF = "PDF";
    private String mUrlPdf;
    private String mNomeArquivo;


    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pdf);


        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions((Activity) this, PERMISSIONS, REQUEST);
            }
        }


        if (getIntent().getExtras().containsKey(NOME_ARQUIVO)) {
            mNomeArquivo = getIntent().getExtras().getString(NOME_ARQUIVO);
        }
        if (getIntent().getExtras().containsKey(PDF)) {
            setUrlPdf((String) getIntent().getExtras().get(PDF));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    abrirPdf();
                } else {
                    Toast.makeText(this, "The app was not allowed to read your store.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void abrirPdf() {

        new Thread(() -> {
            InputStream input;
            OutputStream output;
            HttpURLConnection connection;
            try {
                URL url = new URL(mUrlPdf);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.d("Debug", "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage());
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                String root = Environment.getExternalStorageDirectory().toString();
                if (mNomeArquivo == null || mNomeArquivo.isEmpty()) {
                    mNomeArquivo = "arquivo.pdf";
                }
                String a = root + File.separator + mNomeArquivo;
                File arquivo = new File(a);
                if (!arquivo.exists()) {
                    new File(root).mkdirs();
                    arquivo.createNewFile();
                }
                output = new FileOutputStream(arquivo);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
//                            if (isCancelled()) {
//                                input.close();
//                                return null;
//                            }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
//                                publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                }
                try {
                    output.close();
                    input.close();
                } catch (IOException ignored) {
                }

                connection.disconnect();

                runOnUiThread(() -> {

                    Intent pdfViewIntent = new Intent(Intent.ACTION_VIEW);
                    pdfViewIntent.setDataAndType(Uri.fromFile(arquivo), "application/pdf");
                    pdfViewIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    Intent intent = Intent.createChooser(pdfViewIntent, "Open File");
                    try {
                        startActivity(intent);
                        finish();
                    } catch (ActivityNotFoundException e) {
                        // Instruct the user to install a PDF reader here, or something
                    }
//
//                            pdfView.fromFile(arquivo).pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
//                                    .enableSwipe(true)
//                                    .swipeHorizontal(false)
//                                    .enableDoubletap(true)
//                                    .defaultPage(0)
//                                    .onDraw(onDrawListener)
//                                    .onLoad(onLoadCompleteListener)
//                                    .onPageChange(onPageChangeListener)
//                                    .onPageScroll(onPageScrollListener)
//                                    .onError(onErrorListener)
//                                    .enableAnnotationRendering(false)
//                                    .password(null)
//                                    .scrollHandle(null)
//                                    .load();;

                });

            } catch (Exception e) {
                e.printStackTrace();
            }


        }).start();
    }

    public String getUrlPdf() {
        return mUrlPdf;
    }

    public void setUrlPdf(String urlPdf) {
        mUrlPdf = urlPdf;
        abrirPdf();
    }
}
