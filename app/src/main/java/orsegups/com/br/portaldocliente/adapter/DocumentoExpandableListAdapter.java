package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.fragment.DocumentoPDFFragment;
import orsegups.com.br.portaldocliente.model.Documento;
import orsegups.com.br.portaldocliente.model.Empresa;

/**
 * Created by Orsegups on 23/02/2017.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author filipe.neis
 */
public class DocumentoExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    @Nullable
    private Documento empresaDocFinanceiroSites;
    private Fragment mFragment;

    public DocumentoExpandableListAdapter(Context context, @Nullable Documento empresaDocFinanceiroSites, Fragment fragment) {
        this._context = context;
        this.mFragment = fragment;
        this.empresaDocFinanceiroSites = empresaDocFinanceiroSites;
    }

    @Override
    public int getGroupCount() {
        // Get header size
        if (this.empresaDocFinanceiroSites != null) {

            return this.empresaDocFinanceiroSites.getEmpresas() != null ? this.empresaDocFinanceiroSites.getEmpresas().size() : 0;

        } else {
            return 0;
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return children count
        if (this.empresaDocFinanceiroSites != null) {

            return this.empresaDocFinanceiroSites.getEmpresas() != null ? this.empresaDocFinanceiroSites.getEmpresas().get(groupPosition).getArquivoList().size() : 0;

        } else {
            return 0;
        }
    }

    @Override
    public Empresa getGroup(int groupPosition) {
        // Get header position
        if (this.empresaDocFinanceiroSites != null) {
            return this.empresaDocFinanceiroSites.getEmpresas() != null ? this.empresaDocFinanceiroSites.getEmpresas().get(groupPosition) : null;
        } else {
            return null;
        }
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        List<Date> lista = getLista(groupPosition);
        SimpleDateFormat df = new SimpleDateFormat("MM/yyyy");
        return df.format(lista.get(childPosition));
    }

    private List<Date> getLista(int groupPosition) {
        List<Date> lista = null;
        if (this.empresaDocFinanceiroSites != null) {
            if (this.empresaDocFinanceiroSites.getEmpresas() != null) {
                lista = new ArrayList<>(this.empresaDocFinanceiroSites.getEmpresas().get(groupPosition).getArquivoList().keySet());
            }
            Collections.sort(lista, (o1, o2) -> o2.compareTo(o1));
        }


        return lista;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // Getting header title
        Empresa headerTitle = getGroup(groupPosition);

        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.header_empresas_notas, parent, false);
        }

        TextView header_text = convertView.findViewById(R.id.header);
        header_text.setText(headerTitle.getNomemp());

        // If group is expanded then change the text into bold and change the
        // icon
        if (isExpanded) {
            header_text.setTypeface(null, Typeface.BOLD);
            // header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, '\ue5c7', 0);//up
        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon

            header_text.setTypeface(null, Typeface.NORMAL);
            //header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, '\ue5c7', 0); //dow
        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Getting child text
        final String childText = getChild(groupPosition, childPosition);
        // Inflating child layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_empresas_notas, parent, false);
        }

        TextView child_text = convertView.findViewById(R.id.child);
        convertView.findViewById(R.id.child_relative).setVisibility(View.GONE);

        LinearLayout lyt = convertView.findViewById(R.id.lyt_clid_empresas_notas);
        lyt.setOnClickListener(v -> {
            DocumentoPDFFragment documentoPDFFragment = new DocumentoPDFFragment();
            Date chave = getLista(groupPosition).get(childPosition);
            if (this.empresaDocFinanceiroSites != null) {
                if (this.empresaDocFinanceiroSites.getEmpresas() != null) {
                    documentoPDFFragment.setLista(this.empresaDocFinanceiroSites.getEmpresas().get(groupPosition).getArquivoList().get(chave));
                }
            }
            this.mFragment.getFragmentManager().beginTransaction()
                    .replace(R.id.framelayout, documentoPDFFragment)
                    .addToBackStack(null)
                    .commit();
        });

        child_text.setText(childText);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

}
