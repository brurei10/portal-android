package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.model.HistoricoReclamacao;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class HistoricoReclamacaoListAdapter extends RecyclerView.Adapter<HistoricoReclamacaoListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<HistoricoReclamacao> filtered_items = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public HistoricoReclamacaoListAdapter(Context context, List<HistoricoReclamacao> items) {

        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public HistoricoReclamacaoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_historico_reclamcacao, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        final HistoricoReclamacao atividadeRSC = filtered_items.get(position);


        try {
            holder.dtInicio.setText(atividadeRSC.getData());
        } catch (Exception e) {

            holder.resposavel.setText("");
        }

        try {
            holder.resposavel.setText(atividadeRSC.getResponsavel());
        } catch (Exception e) {

            holder.resposavel.setText("");
        }

        try {
            holder.descricao.setText(Utils.getTextHtml(atividadeRSC.getDescricao()));
        } catch (Exception e) {

            holder.descricao.setText("");
        }


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView descricao;
        // each nfeData item is just a string in this case
        TextView resposavel;
        TextView dtInicio;


        public ViewHolder(View v) {
            super(v);

            dtInicio = v.findViewById(R.id.row_sol_rec_inicio);
            resposavel = v.findViewById(R.id.row_sol_rec_resposavel);
            descricao = v.findViewById(R.id.row_sol_rec_descricao);


        }
    }


}