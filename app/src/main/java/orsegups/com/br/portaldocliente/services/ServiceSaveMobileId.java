package orsegups.com.br.portaldocliente.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import orsegups.com.br.portaldocliente.MainActivity;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;

import static android.content.Context.MODE_PRIVATE;

public class ServiceSaveMobileId extends AsyncTask {

    private Context context;
    private String mTokenDevice;

    public ServiceSaveMobileId(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            InstanceIdResult instanceIdResult = task.getResult();
            if (instanceIdResult != null) {
                mTokenDevice = instanceIdResult.getToken();
            }

            SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE).edit();
            editor.putString(Constantes.SP_TOKEN_CLI, mTokenDevice);
            editor.commit();
        });
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String url = Constantes.URL_SAVE_MOBILE_ID +
                "?cpfCnpj=" + Utils.getOrsegupsIdCnpj(context) +
                "&id=" + mTokenDevice +
                "&type=" + "Android:" + Build.MODEL + "Version:" + Build.VERSION.RELEASE;

        HttpHeaders requestHeaders = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();

        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");

        requestHeaders.set("Content-Type", "application/json");
        requestHeaders.set("orsid-auth-token", token);
        requestHeaders.set("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");


        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(requestHeaders);

        try {
            ResponseEntity responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

            return responseEntity;
        } catch (HttpClientErrorException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
