package orsegups.com.br.portaldocliente.volley.Net;

import androidx.annotation.NonNull;

import org.json.JSONObject;

public interface OnTaskCompletedObject {
    void OnTaskCompletedJSON(@NonNull JSONObject object);
}
