package orsegups.com.br.portaldocliente.model;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;

import static android.content.Context.MODE_PRIVATE;


public class Chat {

    public Chat() {
    }

    public static String getNome(@NonNull Context context) {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        return sp.getString(Constantes.ID_CHAT_NOME_USUARIO, "");
    }

    public static void saveNome(@NonNull String nome, @NonNull Context context) {
        SharedPreferences.Editor editor = Utils.getEditorSharedPreferences(context);
        editor.putString(Constantes.ID_CHAT_NOME_USUARIO, nome.replace("\"", ""));
        editor.commit();
    }

}

