package orsegups.com.br.portaldocliente.adapter;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.view.IconicsImageView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.activity.ViewPDFActivity;
import orsegups.com.br.portaldocliente.model.Boleto;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Utils;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class BoletosListAdapter extends RecyclerView.Adapter<BoletosListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<Boleto> objetos = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public BoletosListAdapter(Context context, List<Boleto> items) {
        objetos = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;

    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public BoletosListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_boleto, parent, false);
        v.setBackgroundResource(mBackground);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.cor_barra_vertical_card_view));


        final Boleto boleto = objetos.get(position);


        holder.boletoNumTitulo.setText(boleto.getNumTit());
        holder.boletoVencimento.setText(boleto.getDatVen());
        holder.boleotData.setText(boleto.getDatHorEnv());
        NumberFormat format = NumberFormat.getCurrencyInstance();
        holder.boletoValor.setText(format.format(boleto.getValTit()));
        holder.boletoBtnCopiarCodigo.setOnClickListener(v -> {

            ClipboardManager _clipboard = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
            _clipboard.setText(boleto.getLinDig());
            Alertas.codigoCopiado(ctx);
        });
        if (boleto.getLinkboleto() != null && !boleto.getLinkboleto().isEmpty()) {
            holder.mBotaoPdfBoleto.setEnabled(true);
            holder.mBotaoPdfBoleto.setVisibility(View.VISIBLE);
            holder.mBotaoPdfBoleto.setOnClickListener(view -> {
                Intent browserIntent = new Intent(view.getContext(), ViewPDFActivity.class);
                browserIntent.putExtra(ViewPDFActivity.PDF, Utils.getBoletos().get(0).getLinkboleto());
                ctx.startActivity(browserIntent);
            });
        } else {
            holder.mBotaoPdfBoleto.setEnabled(false);
            holder.mBotaoPdfBoleto.setVisibility(View.GONE);
        }


        if (!boleto.isSituacao()) {
            holder.boletoSituacao.getIcon().icon(GoogleMaterial.Icon.gmd_warning);
//                holder.boletoSituacao.setIcon(GoogleMaterial.Icon.gmd_warning);
            int cor = holder.itemView.getResources().getColor(R.color.bootstrap_brand_warning);
            holder.boletoSituacao.getIcon().color(cor);
            holder.boletoSituacaoDescricao.setText("Pendente");
            holder.boletoSituacaoDescricao.setTextColor(cor);
            holder.barraLateral.setBackgroundColor(cor);
        } else {
            holder.boletoSituacao.getIcon().icon(GoogleMaterial.Icon.gmd_check_circle);
//                holder.boletoSituacao.setIcon(GoogleMaterial.Icon.gmd_check_circle);
            int cor = holder.itemView.getResources().getColor(R.color.material_green_600);
            holder.barraLateral.setBackgroundColor(cor);
            holder.boletoSituacao.getIcon().color(cor);
            holder.boletoSituacaoDescricao.setText("Pago");
            holder.boletoSituacaoDescricao.setTextColor(cor);
            holder.boletoLinearBotoes.setVisibility(View.GONE);
        }

        if (boleto.getLinDig() != null && !boleto.getLinDig().isEmpty()) {
            holder.tituloTextView.setText("Boleto digital");
            holder.boletoBtnCopiarCodigo.setVisibility(View.VISIBLE);
        } else {
            holder.tituloTextView.setText(boleto.getFormaPagamento());
            holder.boletoBtnCopiarCodigo.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return objetos.size();
    }

    public List<Boleto> getItens() {
        return objetos;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View barraLateral;

        //BOLETO
        TextView boletoTitulo;
        TextView boletoSituacaoDescricao;
        TextView boleotData;
        TextView boletoNumTitulo;
        TextView boletoValor;
        TextView boletoVencimento;
        TextView tituloTextView;
        ImageView boletoBtnCompartilhar;
        BootstrapButton boletoBtnCopiarCodigo;
        BootstrapButton mBotaoPdfBoleto;
        IconicsImageView boletoSituacao;
        LinearLayout boletoLinearBotoes;

        public ViewHolder(View v) {
            super(v);

            barraLateral = v.findViewById(R.id.barra_lateral);


            //Boleto
            boletoTitulo = v.findViewById(R.id.row_timeline_boleto_titulo);
            boleotData = v.findViewById(R.id.row_timeline_boleto_data);
            boletoNumTitulo = v.findViewById(R.id.row_timeline_boleto_num_titulo);
            boletoValor = v.findViewById(R.id.row_timeline_boleto_valor);
            boletoVencimento = v.findViewById(R.id.row_timeline_boleto_vencimento);
            boletoBtnCompartilhar = v.findViewById(R.id.row_timeline_boleto_compartilhar);
            boletoBtnCopiarCodigo = v.findViewById(R.id.row_timeline_boleto_copiar_codigo);
            boletoSituacaoDescricao = v.findViewById(R.id.row_timeline_boleto_situacao_descricao);
            tituloTextView = v.findViewById(R.id.row_boleto_titulo_text_view);
            boletoSituacao = v.findViewById(R.id.row_timeline_boleto_situacao);
            boletoLinearBotoes = v.findViewById(R.id.row_timeline_boleto_botoes);
            mBotaoPdfBoleto = v.findViewById(R.id.row_timeline_boleto_visualizar);


        }
    }


}