package orsegups.com.br.portaldocliente.volley.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.Listener;
import com.android.volley.Response.ErrorListener;

import java.util.HashMap;
import java.util.Map;

import orsegups.com.br.portaldocliente.util.Constantes;

public class TokenRequest extends ObjectRequest {

    private final Context context;

    public TokenRequest(Context context, int method, String url, Listener listener, ErrorListener errorListener, Class type) {
        super(method, url, listener, errorListener, type);
        this.context = context;
    }

    public TokenRequest(Context context, int method, String url, String requestBody, Listener listener, ErrorListener errorListener, Class type) {
        super(method, url, requestBody, listener, errorListener, type);
        this.context = context;
    }

    public TokenRequest(Context context, int method, String url, Listener listener, ErrorListener errorListener) {
        this(context, method, url, listener, errorListener, String.class);
    }

    public TokenRequest(Context context, int method, String url, String requestBody, Listener listener, ErrorListener errorListener) {
        this(context, method, url, requestBody, listener, errorListener, String.class);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
        params.put("Authorization", "Bearer " + token);
        params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
        return params;
    }

}