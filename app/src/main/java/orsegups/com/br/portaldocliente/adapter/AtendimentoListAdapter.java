package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.model.Deslocamento;

/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */
//TODO Pode deletar??
public class AtendimentoListAdapter extends RecyclerView.Adapter<AtendimentoListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<Deslocamento> descamentos = new ArrayList<>();
    private Context ctx;
    // private OnItemClickListener mOnItemClickListener;


//    public interface OnItemClickListener {
//        void onItemClick(View view, int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mOnItemClickListener = mItemClickListener;
//    }


    public AtendimentoListAdapter(Context context, List<Deslocamento> items) {
        descamentos = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    @NonNull
    @Override
    public AtendimentoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_atendimento_rat, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Deslocamento deslocamento = descamentos.get(position);

        holder.status.setText(deslocamento.getNomeStatus());
        holder.atendente.setText(deslocamento.getNomeViatura());
        String evento = deslocamento.getCodigoEvento() + " - " + deslocamento.getNomeEvento();
        holder.evento.setText(evento);
        holder.local.setText(Utils.getEndereco(ctx, deslocamento.getLatitude(), deslocamento.getLongitude()));

        holder.iconePanico.setImageDrawable(new IconicsDrawable(ctx, GoogleMaterial.Icon.gmd_warning).sizeDp(24).color(ContextCompat.getColor(ctx, R.color.material_grey_600)));
        holder.iconePin.setImageDrawable(new IconicsDrawable(ctx, GoogleMaterial.Icon.gmd_place).sizeDp(24).color(ContextCompat.getColor(ctx, R.color.material_grey_600)));
        holder.iconeEvento.setImageDrawable(new IconicsDrawable(ctx, GoogleMaterial.Icon.gmd_event_available).sizeDp(24).color(ContextCompat.getColor(ctx, R.color.material_grey_600)));


    }

    @Override
    public int getItemCount() {
        return descamentos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each nfeData item is just a string in this case
        TextView evento, local, status, atendente;
        ImageView iconePanico, iconeEvento, iconePin;
        Button detalhes;

        public ViewHolder(View v) {
            super(v);
            evento = v.findViewById(R.id.row_atendimento_evento);
            local = v.findViewById(R.id.row_atendimento_local);
            status = v.findViewById(R.id.row_atendimento_status);
            atendente = v.findViewById(R.id.row_atendimento_atendente);
            detalhes = v.findViewById(R.id.row_atendimento_detalhe);

            iconeEvento = v.findViewById(R.id.row_atendimento_icone_evento);
            iconePanico = v.findViewById(R.id.row_atendimento_icone);
            iconePin = v.findViewById(R.id.row_atendimento_icone_pin);


        }
    }


}