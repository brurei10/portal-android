package orsegups.com.br.portaldocliente.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mikepenz.iconics.view.IconicsImageView;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.adapter.PageViewAdapter;
import orsegups.com.br.portaldocliente.model.ContaSigma;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Orsegups on 15/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */
public class EstatisticaFragment extends Fragment {


    View view;
    private IconicsImageView btnAnterior, btnProximo, btnAnteriorIcon, btnProximoIcon;
    private List<ContaSigma> contaSigmas = new ArrayList<>();
    private List<String> titulos = new ArrayList<>();
    private TextView emptyView;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_estatistica, container, false);


        emptyView = view.findViewById(R.id.empty_view);

        setHasOptionsMenu(true);
        MixpanelUtil.track(Utils.getCnpjLogado(view.getContext()), "EstatisticaFragment", "GRAFICO", view.getContext());
        loadGraficos();


        return view;
    }


    private void loadGraficos() {

        final SweetAlertDialog dialog = Alertas.wait(view.getContext(), view.getContext().getString(R.string.alerta_carregando));
        try {

            final SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            String contractCode = sp.getString(Constantes.SP_CNPJ_CLI, "");
            String cgcCpf = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");


            String url = Constantes.URL_ESTATISTIACAS
                    + "contractCode=" + contractCode
                    + "&cgcCpf=" + cgcCpf;

            RequestQueue queue = Volley.newRequestQueue(view.getContext());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                Gson gson = new Gson();
                                JsonParser jsonParser = new JsonParser();
                                JsonElement element = jsonParser.parse(response);

                                int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
                                String msg = element.getAsJsonObject().get("msg").getAsString();

                                if (status == 100) {
                                    SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(Constantes.SP_ESTATISTICA, response);
                                    editor.commit();
                                    int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

                                    for (int i = 0; i < total; i++) {

                                        ContaSigma reclamacao = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), ContaSigma.class);
                                        contaSigmas.add(reclamacao);

                                    }
                                    initGrafico();

                                    dialog.dismiss();


                                } else {
                                    dialog.dismiss();
                                    Alertas.erroInesperado(view.getContext(), msg.replace("\"\"", ""));

                                }

//                                Log.d("d", response.toString());

                            } catch (Exception e) {
                                dialog.dismiss();

                                // tituloVazio.setVisibility(View.VISIBLE);
                                //Alertas.erroInesperado(view.getContext(), "Você não tem nenhuma informação para mostrar no momento.");
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("d", "Erro");
                    dialog.dismiss();
                    Alertas.erroComunicacaoServidor(view.getContext());
                    // configuraSemConexao();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                    String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer " + token);
                    params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    120000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);


        } catch (Exception e) {

            // mPtrFrame.refreshComplete();
            Alertas.erroComunicacaoServidor(view.getContext());
            //  configuraSemConexao();
        }
    }

    private void initGrafico() {


        // pageIndicatorView.setVisibility(View.VISIBLE);


        if (contaSigmas.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);

        } else {

            final PageViewAdapter adapter = new PageViewAdapter(contaSigmas, titulos, view.getContext());
            adapter.setData(createPageList());

            final ViewPager pager = view.findViewById(R.id.viewpager);
            pager.setAdapter(adapter);

            PageIndicatorView pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
            pageIndicatorView.setViewPager(pager);

            //  tituloEstastitica.setText(titulos.get( pager.getCurrentItem()));
            // tituloEstastitica.setText(titulos.get( pager.getCurrentItem()));

            emptyView.setVisibility(View.GONE);
            pageIndicatorView.setVisibility(View.VISIBLE);
            pager.setVisibility(View.VISIBLE);

        }


    }


    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();


        LayoutInflater factory = LayoutInflater.from(view.getContext());

        for (ContaSigma cs : contaSigmas) {
            String titulo = cs.getCentralParticao() + cs.getFantasia();
            titulos.add(titulo);

            pageList.add(factory.inflate(R.layout.row_estatistica, null));
        }
        return pageList;
    }


}


