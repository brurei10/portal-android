package orsegups.com.br.portaldocliente.volley.common;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ObjectRequest<T> extends Request<T> {

    protected static final String PROTOCOL_CHARSET = "utf-8";

    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    /** Lock to guard mListener as it is cleared on cancel() and read on delivery. */
    private final Object mLock = new Object();
    // @GuardedBy("mLock")
    private Listener<T> mListener;
    private final String mRequestBody;
    private Class<T> type;

    public ObjectRequest(int method, String url, String requestBody, Listener<T> listener, ErrorListener errorListener, Class<T> type) {
        super(method, url, errorListener);
        this.mListener = listener;
        this.mRequestBody = requestBody;
        this.type = type;
    }

    public ObjectRequest(int method, String url, Listener<T> listener, ErrorListener errorListener, Class<T> type) {
        this(method, url, null, listener, errorListener, type);
    }

    @Override
    public void cancel() {
        super.cancel();
        synchronized (mLock) {
            mListener = null;
        }
    }

    @Override
    protected void deliverResponse(T response) {
        Listener<T> listener;
        synchronized (mLock) {
            listener = mListener;
        }
        if (listener != null) {
            listener.onResponse(response);
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }

        if (type == String.class)
            return Response.success((T) parsed, HttpHeaderParser.parseCacheHeaders(response));

        try {
            return Response.success(new ObjectMapper().readValue(parsed, type), HttpHeaderParser.parseCacheHeaders(response));
        } catch (IOException e ) {
            return Response.error(new VolleyError());
        }
    }

    @Override
    public String getBodyContentType() {
        return mRequestBody == null ? super.getBodyContentType() : PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

}