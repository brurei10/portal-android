package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.ClienteSite;

public class ClienteSiteDao extends BaseDaoImpl<ClienteSite, Integer> {
    public ClienteSiteDao(ConnectionSource cs) throws SQLException {
        super(ClienteSite.class);
        setConnectionSource(cs);
        initialize();
    }
}
