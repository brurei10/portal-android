package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import orsegups.com.br.portaldocliente.R;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class EstatisticaListAdapter extends RecyclerView.Adapter<EstatisticaListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    //PDFView pdfView;
    private String TAG = "PDF-FILIPE";
    private List<Object> original_items = new ArrayList<>();
    private List<Object> filtered_items = new ArrayList<>();
    private ArrayList<Date> datas;
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public EstatisticaListAdapter(Context context, List<Object> items) {
        original_items = items;
        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    private LineData generateDataLine(int cnt, int range, View view) {


        ArrayList<Entry> e1 = new ArrayList<Entry>();

        for (int i = 0; i < cnt; i++) {
            e1.add(new Entry(i, (int) (Math.random() * 65) + 40));

        }

        Drawable drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.bg_gradient);

        LineDataSet d1 = new LineDataSet(e1, "Ultimos Atendimentos Taticos");
        d1.setLineWidth(2.5f);
        d1.setCircleRadius(4.5f);
        d1.setColor(Color.parseColor("#2196F3"));
        d1.setCircleColor(d1.getColor());
        d1.setDrawValues(false);
        d1.setDrawFilled(true);
        d1.setFillDrawable(drawable);


        ArrayList<Entry> e3 = new ArrayList<Entry>();

        for (int i = 0; i < cnt; i++) {
            e3.add(new Entry(i, e1.get(i).getY() - 30));
        }


        LineDataSet d3 = new LineDataSet(e3, "Ultimos Atendimentos de Desvio de Habito");
        d3.setLineWidth(2.5f);
        d3.setValueTextColor(Color.WHITE);
        d3.setCircleRadius(4.5f);
        d3.setColor(Color.parseColor("#689F38"));
        d3.setCircleColor(d3.getColor());
        d3.setDrawValues(false);
        d3.setDrawFilled(true);

        Drawable drawable3 = ContextCompat.getDrawable(view.getContext(), R.drawable.bg_gradient3);
        d3.setFillDrawable(drawable3);

        ArrayList<Entry> e2 = new ArrayList<Entry>();
        for (int i = 0; i < cnt; i++) {
            if (i % 2 == 0) {

                e2.add(new Entry(i, e1.get(i).getY() + 5));
            } else {

                e2.add(new Entry(i, e1.get(i).getY() - 10));
            }
        }

        LineDataSet d2 = new LineDataSet(e2, "Ultimos Atendimentos de Desvio de Habito");
        d2.setLineWidth(2.5f);
        d2.setValueTextColor(Color.WHITE);
        d2.setCircleRadius(4.5f);
        d2.setColor(Color.parseColor("#678acc"));
        d2.setCircleColor(d2.getColor());
        d2.setDrawValues(false);
        d2.setDrawFilled(true);
        Drawable drawable2 = ContextCompat.getDrawable(view.getContext(), R.drawable.bg_gradient2);
        d2.setFillDrawable(drawable2);


        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
        sets.add(d1);
        sets.add(d2);
        sets.add(d3);
        LineData cd = new LineData(sets);

        cd.setValueTextColor(Color.WHITE);


        return cd;
    }

    private void detalhesEstatisticas(View v) {
        MaterialDialog dialog = new MaterialDialog.Builder(v.getContext())
                .title("Atendimento Taticos")
                .customView(R.layout.dashboard_dialog, false)
                .positiveText("Ok")
                .negativeText("")
                .build();

        dialog.show();
    }

    @NonNull
    @Override
    public EstatisticaListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_estatistica, parent, false);
        v.setBackgroundResource(mBackground);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each nfeData item is just a string in this case
        public LineChart lineChart;


        public ViewHolder(final View v) {
            super(v);
            lineChart = v.findViewById(R.id.chart1);

            datas = new ArrayList<>();

            for (int i = 0; i < 10; i++) {
                Date dt = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, i);
                dt = c.getTime();
                datas.add(dt);

            }

            // apply styling
            // lineChart.setValueTypeface(mTf);
            lineChart.getDescription().setEnabled(false);
            lineChart.setDrawGridBackground(false);


            XAxis xAxis = lineChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTextColor(Color.WHITE);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(true);
            xAxis.setGranularity(3f);
            xAxis.setValueFormatter(new IAxisValueFormatter() {

                private SimpleDateFormat mFormat = new SimpleDateFormat("dd/MM/yy");

                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    return mFormat.format(datas.get(Math.round(value)));
                }
            });

            YAxis leftAxis = lineChart.getAxisLeft();
            leftAxis.setTextColor(Color.WHITE);
            leftAxis.setLabelCount(5, false);
            leftAxis.setAxisMinimum(0f);
            leftAxis.setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {

                    String valor = String.valueOf(value);
                    return valor.replaceAll("\\.?0*$", "") + " min";
                }
            });

            YAxis rightAxis = lineChart.getAxisRight();
            rightAxis.setTextColor(Color.TRANSPARENT);

            // set nfeData
            lineChart.setData(generateDataLine(8, 5, v));

            Legend l = lineChart.getLegend();
            l.setEnabled(false);

            lineChart.animateX(750);
            lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    String local = "Valor X" + String.valueOf(e.getX()) + " Valor Y" + String.valueOf(e.getY());
                    Toast.makeText(v.getContext(), local, Toast.LENGTH_SHORT).show();
                    //  detalhesEstatisticas(v);
                }

                @Override
                public void onNothingSelected() {

                }
            });


        }
    }

}