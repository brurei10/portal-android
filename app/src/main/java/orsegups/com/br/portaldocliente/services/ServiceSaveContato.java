package orsegups.com.br.portaldocliente.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import orsegups.com.br.portaldocliente.activity.ContatoActivity;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;

import static android.content.Context.MODE_PRIVATE;
import static java.nio.charset.StandardCharsets.UTF_8;

public class ServiceSaveContato extends AsyncTask {

    private Context context;
    private String nome;
    private String email;
    private String mensagem;
    private String telefone;
    private String tipoContato;
    private ContatoActivity parent;

    public ServiceSaveContato(Context context, String nome, String email, String mensagem, String telefone, String tipoContato, ContatoActivity parent) {
        this.context = context;
        this.nome = nome;
        this.email = email;
        this.mensagem = mensagem;
        this.telefone = telefone;
        this.tipoContato = tipoContato;
        this.parent = parent;
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String url = Constantes.URL_SOLICITACAO_RECLAMACAO;

        HttpHeaders requestHeaders = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //Aceita acentuaçâo pelo Rest
            restTemplate.getMessageConverters()
                    .add(0, new StringHttpMessageConverter(UTF_8));
        }

        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");

        requestHeaders.set("Content-Type", "application/json");
        requestHeaders.set("orsid-auth-token", token);
        requestHeaders.set("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");

        String cnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");

//        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
//        body.add("nome", nome);
//        body.add("email", email);
//        body.add("mensagem", mensagem);
//        body.add("telefone", telefone);
//        body.add("tipoContato", tipoContato);
//        body.add("contractCode", Utils.getContractCode(context));
//        body.add("cpfCnpj", cnpj);
//        body.add("admin", Utils.isAdmin(context) + "");
        Map<String, String> body = new HashMap<>();
        body.put("nome", nome);
        body.put("email", email);
        body.put("mensagem", mensagem);
        body.put("telefone", telefone);
        body.put("tipoContato", tipoContato);
        body.put("contractCode", Utils.getContractCode(context));
        body.put("cpfCnpj", cnpj);
        body.put("admin", Utils.isAdmin(context) + "");

        Gson gson = new Gson();

        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(gson.toJson(body), requestHeaders);

        try {
            ResponseEntity responseEntity = restTemplate.postForEntity(url, entity, String.class);
            return responseEntity;
        } catch (HttpClientErrorException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        parent.retornoSolicitacaoReclamacao((ResponseEntity) o);
    }
}
