package orsegups.com.br.portaldocliente.model;

import java.util.ArrayList;
import java.util.List;

public class Solicitacao {
    private Long neoid;
    private String codigoTarefa;
    private String processo;
    private String solicitante;
    private String dataInicio;
    private String dataConclusao;
    private String categoria;
    private String situacao;
    private String link;
    private String descricao;
    private String atendente;
    private String contato;
    private String satisfacao;

    private List<RegistroAtividadeRSC> registroAtividades = new ArrayList<RegistroAtividadeRSC>();

    public Long getNeoid() {
        return neoid;
    }

    public void setNeoid(Long neoid) {
        this.neoid = neoid;
    }

    public String getCodigoTarefa() {
        return codigoTarefa;
    }

    public void setCodigoTarefa(String codigoTarefa) {
        this.codigoTarefa = codigoTarefa;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(String dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAtendente() {
        return atendente;
    }

    public void setAtendente(String atendente) {
        this.atendente = atendente;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public List<RegistroAtividadeRSC> getRegistroAtividades() {
        return registroAtividades;
    }

    public void setRegistroAtividades(List<RegistroAtividadeRSC> registroAtividades) {
        this.registroAtividades = registroAtividades;
    }

    public String getSatisfacao() {
        return satisfacao;
    }

    public void setSatisfacao(String satisfacao) {
        this.satisfacao = satisfacao;
    }
}
