package orsegups.com.br.portaldocliente.activity;


import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.adapter.NotaListAdapter;
import orsegups.com.br.portaldocliente.model.DocumentoFinanceiroSite;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.OnTaskCompleted;


/**
 * Created by Orsegups on 03/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */


public class NotaFiscalActivity extends AppCompatActivity implements OnTaskCompleted {

    public RecyclerView recyclerView;
    protected Handler handler;
    private NotaListAdapter mAdapter;
    private List<DocumentoFinanceiroSite> docFincList;
    private ActionBar actionBar;
    private String FILIAL;
    private String EMPRESA;
    private String COMPETENCIA;
    private TextView emptyView;
    private String url;
    private int NUMPAG = 0;
    private Boolean isFinalLista = false;
    private ProgressBar progressBar;
    private RelativeLayout relativeProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_nota_fical);

        initToolbar();


        initCompontes();
        getWebServiceData();


    }

    private void initCompontes() {

        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressbar);
        relativeProgress = findViewById(R.id.relative_progress);
        emptyView = findViewById(R.id.empty_view);

        FILIAL = getIntent().getStringExtra(Constantes.NOTA_FILIAL);
        COMPETENCIA = getIntent().getStringExtra(Constantes.NOTA_COMPETENCIA);
        EMPRESA = getIntent().getStringExtra(Constantes.NOTA_EMPRESA);

        recyclerView.setVisibility(View.GONE);

        docFincList = new ArrayList<>();
        handler = new Handler();


        MixpanelUtil.track(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_NOTA, "NOTA", this);


    }

    public void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Notas Fiscais");
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(null);
            getWindow().setEnterTransition(null);
        }


    }

    private void configureList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new NotaListAdapter(this, docFincList, recyclerView);
        recyclerView.setAdapter(mAdapter);

        relativeProgress.setVisibility(View.GONE);

        if (docFincList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);

        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

        }

        mAdapter.setOnLoadMoreListener(() -> {

            if (!isFinalLista) {
                docFincList.add(null);
                mAdapter.notifyItemInserted(docFincList.size() - 1);

                final Runnable r = () -> {
                    docFincList.remove(docFincList.size() - 1);
                    mAdapter.notifyItemRemoved(docFincList.size());

                    ++NUMPAG;
                    getWebServiceData();
                };
//
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            docFincList.remove(docFincList.size() - 1);
//                            mAdapter.notifyItemRemoved(docFincList.size());
//
//                            ++NUMPAG;
//                            getWebServiceData();
//
//                        }
//                    }, 2000); //time 2 seconds
                handler.post(r);

            }

        });


    }

    public void getWebServiceData() {

        final SharedPreferences sp = getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String cnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");
        String sessao = sp.getString(Constantes.SP_SESSION_ID, "");

        String url = Constantes.URL_NOTAS_POR_COMPETENCIA
                + "sessionId=" + sessao
                + "&isMobile=true"
                + "&cgcCpf=" + cnpj
                + "&filial=" + FILIAL
                + "&empresa=" + EMPRESA
                + "&numPag=" + NUMPAG
                + "&cpt=" + COMPETENCIA;
//        url = "http://portal-mobile-prod.sa-east-1.elasticbeanstalk.com/portalcliente/v1/competenciaNotas?cgcCpf=" + cnpj;
//        ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, this, this);
        docFincList = Utils.getDocumentoFinanceiroSites();
        configureList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);

    }


    @Override
    public void onTaskCompleted(@NonNull String response) {
        try {
            Gson gson = new Gson();

            JsonElement element = JsonParser.parseString(response);
            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());

            if (status == 100) {

                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

                if (total == 0) {

                    isFinalLista = true;
                }
//            DocumentoFinanceiroSite financeiroSite = Utils.getDocumentoFinanceiroSites();
                if (NUMPAG > 0) {

                    for (int i = 0; i < total; i++) {

                        DocumentoFinanceiroSite documentoFinanceiroSite = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), DocumentoFinanceiroSite.class);
//                    if (financeiroSite != null) {
//                        documentoFinanceiroSite.setLinkBoleto(financeiroSite.getLinkBoleto());
//                    }
                        docFincList.add(documentoFinanceiroSite);
                        mAdapter.notifyItemInserted(docFincList.size());

                    }
                    mAdapter.setLoaded();
                }


                if (NUMPAG == 0) {
                    // Se na primeira pagina vim menos de 10, ou seja nao tem paginacao
                    if (total <= 8) {

                        isFinalLista = true;
                    }

                    for (int i = 0; i < total; i++) {

                        DocumentoFinanceiroSite documentoFinanceiroSite = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), DocumentoFinanceiroSite.class);
//                    if (financeiroSite != null) {
//                        documentoFinanceiroSite.setLinkBoleto(financeiroSite.getLinkBoleto());
//                    }
                        docFincList.add(documentoFinanceiroSite);


                    }
                    configureList();
                }

                mAdapter.setLoaded();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}