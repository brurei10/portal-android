package orsegups.com.br.portaldocliente.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.ContatoActivity;
import orsegups.com.br.portaldocliente.adapter.ReclamacaoListAdapter;
import orsegups.com.br.portaldocliente.model.Reclamacao;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by filipeamaralneis on 01/03/17.
 */
public class ReclamacaoFragment extends Fragment {

    public RecyclerView recyclerView;
    //private ReclamacaoListAdapter mAdapter;
    private TextView emptyView;
    private PtrClassicFrameLayout mPtrFrame;
    private FloatingActionButton fab;


    View view;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main, container, false);
        emptyView = (TextView) view.findViewById(R.id.empty_view);
        setHasOptionsMenu(true);
        configuraRefresh();
        novaReclamacao();

        return view;
    }

    private void configuraRefresh() {
        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                boolean isConecao = Utils.verificaConexaoInternet();

                if (isConecao) {
                    loadDocumentoFinanceiro();
                } else {
                    configuraSemConexao();
                }
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }
        });

        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setPullToRefresh(false);
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh(true);
            }
        }, 100);
    }

    private void loadDocumentoFinanceiro() {


        //  final SweetAlertDialog dialog = Alertas.comunicacaoServidor(view.getContext(), view.getContext().getString(R.string.alerta_carregando));
        try {

            final SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            String cnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");
            String sessao = sp.getString(Constantes.SP_SESSION_ID, "");
            final List<Reclamacao> reclamacoes = new ArrayList<>();

            String url = Constantes.URL_LISTA_RECLAMACOES
                    + "cgcCpf=" + cnpj
                    + "&contractCode=" + Utils.getContractCode(view.getContext())
                    + "&admin=" + Utils.isAdmin(view.getContext());

            RequestQueue queue = Volley.newRequestQueue(view.getContext());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            SincronizaBaseDados.updateBadge(view.getContext(), Constantes.BADGE_LOCAL_DEFAULT);


                            Gson gson = new Gson();
                            JsonParser jsonParser = new JsonParser();
                            JsonElement element = jsonParser.parse(response);
                            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
                            String msg = element.getAsJsonObject().get("msg").getAsString();

                            if (status == 100) {
                                SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(Constantes.SP_RECLAMACOES, response);
                                editor.commit();
                                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

                                for (int i = 0; i < total; i++) {

                                    Reclamacao reclamacao = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), Reclamacao.class);
                                    reclamacoes.add(reclamacao);

                                }
                                configureList(reclamacoes);


                            } else {
                                mPtrFrame.refreshComplete();
                                if (status == 96) {
                                    Alertas.sessaoExpirada(view.getContext(), sp.getString(Constantes.SP_CNPJ_CLI_MASK, ""));
                                } else {
                                    Alertas.erroInesperado(view.getContext(), msg.replace("\"\"", ""));
                                }
                            }

                            Log.d("d", response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("d", "Erro");
                    mPtrFrame.refreshComplete();
                    Alertas.erroComunicacaoServidor(view.getContext());
                    configuraSemConexao();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                    String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer " + token);
                    params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                    return params;
                }
            };

//            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
//                    0,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);


        } catch (Exception e) {

            mPtrFrame.refreshComplete();
            Alertas.erroComunicacaoServidor(view.getContext());
            configuraSemConexao();
        }
    }

    private void configuraSemConexao() {

        SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String response = sp.getString(Constantes.SP_RECLAMACOES, "");

        List<Reclamacao> reclamacoes = new ArrayList<>();

        if (!response.isEmpty()) {
            try {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement element = jsonParser.parse(response);
                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();
                for (int i = 0; i < total; i++) {

                    Reclamacao reclamacao = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), Reclamacao.class);
                    reclamacoes.add(reclamacao);

                }
            } catch (Exception e) {
            }
        }
        configureList(reclamacoes);
    }


    private void configureList(List<Reclamacao> reclamacoes) {
        mPtrFrame.refreshComplete();

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
       // recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);

        //  mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        ReclamacaoListAdapter mAdapter = new ReclamacaoListAdapter(getActivity(), reclamacoes);
        recyclerView.setAdapter(mAdapter);

        if (reclamacoes.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

    }

    private void novaReclamacao(){
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setColorFilter(ContextCompat.getColor(view.getContext(), R.color.amarelo_orsegups));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                * Fab local - 1 reclamacao | 2 solicitacao
                * */
                Intent i = new Intent(view.getContext(), ContatoActivity.class);
                i.putExtra(Constantes.FAB_LOCAL,1);
                startActivity(i);
            }
        });

    }


}


