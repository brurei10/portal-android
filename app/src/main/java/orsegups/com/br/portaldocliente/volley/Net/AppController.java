package orsegups.com.br.portaldocliente.volley.Net;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.beardedhen.androidbootstrap.TypefaceProvider;


public class AppController extends Application {

    public static Context AppContext = null;
    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppContext = getApplicationContext();
        TypefaceProvider.registerDefaultIconSets();


    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }




}