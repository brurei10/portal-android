package orsegups.com.br.portaldocliente.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.adapter.EmpresaNotasExpandableListAdapter;
import orsegups.com.br.portaldocliente.model.CompetenciaDocFinanceiroSite;
import orsegups.com.br.portaldocliente.model.EmpresaDocFinanceiroSite;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.OnTaskCompleted;
import orsegups.com.br.portaldocliente.volley.Net.ServiceHandler;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class EmpresaNotasFiscaisFragment extends Fragment implements OnTaskCompleted {

    public RecyclerView recyclerView;
    private View view;
    private ExpandableListView expandableListView;
    private EmpresaNotasExpandableListAdapter mAdapter;
    private TextView emptyView;
    private PtrClassicFrameLayout mPtrFrame;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_empresas, container, false);
        emptyView = view.findViewById(R.id.empty_view);

        SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(Constantes.SP_LOCAL_PUSH_NUMBER, 0);
        editor.commit();

        configuraRefresh();
        return view;
    }

    private void configuraRefresh() {
        mPtrFrame = view.findViewById(R.id.store_house_ptr_frame);
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                boolean isConecao = Utils.verificaConexaoInternet();

                if (isConecao) {
                    loadDocumentoFinanceiro();
                } else {
                    configuraSemConexao();
                }
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }
        });

        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setPullToRefresh(false);
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(() -> mPtrFrame.autoRefresh(true), 100);
    }

    private void loadDocumentoFinanceiro() {


        try {

            // final SweetAlertDialog dialog = Alertas.comunicacaoServidor(view.getContext(),view.getContext().getString(R.string.alerta_carregando));
            final SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
            String cnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");

            final List<EmpresaDocFinanceiroSite> empresaDocFinanceiroSites = new ArrayList<>();

            String url = Constantes.URL_COMPETENCIAS_NOTAS + cnpj;

            ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, getContext(), this);

        } catch (Exception e) {
            mPtrFrame.refreshComplete();
            Alertas.erroComunicacaoServidor(view.getContext());
            configuraSemConexao();
        }
    }

    private void configuraSemConexao() {

        SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String response = sp.getString(Constantes.SP_NOTAS_FISCAIS, "");
        List<EmpresaDocFinanceiroSite> empresaDocFinanceiroSites = new ArrayList<>();
        if (response != null && !response.isEmpty()) {

            try {
                Gson gson = new Gson();

                JsonElement element = JsonParser.parseString(response);
                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

                for (int i = 0; i < total; i++) {

                    EmpresaDocFinanceiroSite docFinanceiroSite = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), EmpresaDocFinanceiroSite.class);
                    empresaDocFinanceiroSites.add(docFinanceiroSite);

                }

            } catch (Exception ignored) {
            }
        }
        configureList(empresaDocFinanceiroSites);
    }


    private void configureList(List<EmpresaDocFinanceiroSite> empresaDocFinanceiroSites) {
        mPtrFrame.refreshComplete();

        expandableListView = view.findViewById(R.id.simple_expandable_listview);

        expandableListView.setGroupIndicator(null);

        final int[] lastExpandedPosition = {-1};

        // fecha a pergunta aberta quando abre uma nova pergunta
        expandableListView.setOnGroupExpandListener(groupPosition -> {
            // closeKeyboard();
            if (lastExpandedPosition[0] != -1
                    && groupPosition != lastExpandedPosition[0]) {
                expandableListView.collapseGroup(lastExpandedPosition[0]);

            }
            lastExpandedPosition[0] = groupPosition;
        });

        ArrayList<String> header = new ArrayList<>();
        List<String> child1 = new ArrayList<>();
        LinkedHashMap<String, List<String>> hashMap = new LinkedHashMap<>();

        int count = 0;
        for (EmpresaDocFinanceiroSite emp : empresaDocFinanceiroSites) {

            header.add(emp.getNomeEmpresa());
            for (CompetenciaDocFinanceiroSite comp : emp.getCompetenciaDocFinanceiroSite()) {

                child1.add(comp.getCompetencia());

            }
            hashMap.put(header.get(count), child1);
            child1 = new ArrayList<>();
            count++;
        }
        mAdapter = new EmpresaNotasExpandableListAdapter(view.getContext(), header, hashMap, empresaDocFinanceiroSites);
        // Setting adpater over expandablelistview


        expandableListView.setAdapter(mAdapter);

        if (empresaDocFinanceiroSites.size() >= 1) {

            // expandableListView.collapseGroup(1);
            expandableListView.expandGroup(0);
        }

        if (empresaDocFinanceiroSites.isEmpty()) {
            expandableListView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            expandableListView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }


    }


    @Override
    public void onTaskCompleted(@NonNull String response) {
        Gson gson = new Gson();

        JsonElement element = JsonParser.parseString(response);
        int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
        String msg = element.getAsJsonObject().get("msg").getAsString();
        SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        if (status == 100) {
            SincronizaBaseDados.updateBadge(view.getContext(), Constantes.BADGE_LOCAL_NF);

            SharedPreferences.Editor editor = sp.edit();
            editor.putString(Constantes.SP_NOTAS_FISCAIS, response);
            editor.putInt(Constantes.SP_BADGE_NOTAFISCAL, 0);
            Utils.confguraBagde(view.getContext(), false);

            editor.commit();
            int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

            List<EmpresaDocFinanceiroSite> empresaDocFinanceiroSites = new ArrayList<>();

            for (int i = 0; i < total; i++) {

                EmpresaDocFinanceiroSite empresaDocFinanceiroSite = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), EmpresaDocFinanceiroSite.class);

                empresaDocFinanceiroSites.add(empresaDocFinanceiroSite);

            }

            //  dialog.dismiss();

            configureList(empresaDocFinanceiroSites);


        } else {
            mPtrFrame.refreshComplete();
            if (status == 96) {

                Alertas.sessaoExpirada(view.getContext(), sp.getString(Constantes.SP_CNPJ_CLI_MASK, ""));
            } else {
                Alertas.erroInesperado(view.getContext(), msg.replace("\"\"", ""));
            }
        }
    }
}


