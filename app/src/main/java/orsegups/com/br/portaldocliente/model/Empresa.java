package orsegups.com.br.portaldocliente.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Empresa {

    @SerializedName("cpdeemp")
    private int mCodemp;
    @SerializedName("codfil")
    private int mCodFil;
    @SerializedName("cnpj")
    private String mCnpj;
    @SerializedName("nomemp")
    private String mNomemp;
    @SerializedName("competencias")
    private String mCompetencias;
    @SerializedName("arquivos")
    private HashMap<Date, List<Arquivo>> mArquivoList;


    public int getCodemp() {
        return mCodemp;
    }

    public int getCodFil() {
        return mCodFil;
    }

    public String getCnpj() {
        return mCnpj;
    }

    public String getNomemp() {
        return mNomemp;
    }

    public String getCompetencias() {
        return mCompetencias;
    }

    public HashMap<Date, List<Arquivo>> getArquivoList() {
        return mArquivoList;
    }
}
