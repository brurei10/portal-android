package orsegups.com.br.portaldocliente.util;

public enum PermissionEnum {
    ESTATISTICAS(9),
    SEGURANCA_ELETRONICA(10),
    RELATORIO_ATENDIMENTO(11),
    NOTAS_FISCAIS(1),
    SOLICITACAO(7),
    NOTIFICACOES(13),
    RECLAMACOES(12),
    ATENDIMENTO_AUTOMATIZADO(15),
    DOCUMENTOS(16);

    private final Integer code;

    PermissionEnum(Integer code) {
        this.code = code;
    }

    public static PermissionEnum valueOf(Integer code) {
        for (PermissionEnum permission : values()) {
            if (permission.code.equals(code))
                return permission;
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

}
