package orsegups.com.br.portaldocliente.volley.common;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.util.Alertas;

public abstract class AbstractRequest {

    protected final Context context;
    protected final SweetAlertDialog awaitDialog;

    public AbstractRequest(Context context, String awaitMsg) {
        this.context = context;
        this.awaitDialog = Alertas.wait(context, awaitMsg, false);
    }

    public AbstractRequest(Context context) {
        this(context, "");
    }

    public AbstractRequest(Context context, SweetAlertDialog awaitDialog) {
        this.context = context;
        this.awaitDialog = awaitDialog;
    }

    protected void makeRequest(Request request) {
        RequestQueue queue = Volley.newRequestQueue(context);
        request.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

}
