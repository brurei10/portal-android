package orsegups.com.br.portaldocliente.util;

/**
 * Created by filipe.neis on 21/02/2017.
 */

public class Constantes {


    /*URL SERVIDOR*/

    //    public static final String DEFAULT_PORTAL_URL            = "http://DSOO07:8080/portalcliente/v1/"; //DANILLO

//        public static final String DEFAULT_PORTAL_URL            = " http://DSOO395:8080/portalcliente/v1/";
    private static final String DEFAULT_PORTAL_URL = "http://portal-mobile-prod.sa-east-1.elasticbeanstalk.com/portalcliente/v1/"; //AMAZON

    //    public static final String DEFAULT_HIMEKER_CHAT          = "https://painel.ellyweb.com.br/public/orsegups_homolog/mobile/";
    private static final String DEFAULT_HIMEKER_API_V2 = "https://live.elly.orsegups.com.br/api/v2/";
    public static final String DEFAULT_HIMEKER_CHAT = "https://elly.orsegups.com.br/chat/mobile/v1.1/";

    //    public static final String DEFAULT_ORSEGUPS_ID_URL            = "http://orsegups-id-homol.us-east-1.elasticbeanstalk.com/"; //HOMOLOGAÇÃO
//    public static final String DEFAULT_ORSEGUPS_ID_URL       = "http://DSOO07:8080/"; //Danillo
    private static final String DEFAULT_ORSEGUPS_ID_URL = "https://id2.orsegups.com.br/"; //AMAZON

    public static final String SHARED_PREF_NAME = "orsegups_app";
    public static final String SP_REFRESH_TOKEN_ORSEGUPS_ID = "refresh_token_orsegups_id";
    public static final String SP_TOKEN_ORSEGUPS_ID = "token_orsegups_id";
    public static final String SP_USER_ORSEGUPS_ID = "user_orsegups_id";
    public static final String SP_USER_DOCUMENT = "user_document";
    public static final String SP_EMAIL_ORSEGUPS_ID = "email_orsegups_id";
    public static final String SP_LOGIN_ORSEGUPS_ID = "login_orsegups_id";
    public static final String SP_SESSION_ID = "session_cli";
    public static final String SP_NOTAS_FISCAIS = "notas_fiscais_cli";
    public static final String SP_SOLICITACOES = "cli_solicitacoes";
    public static final String SP_RECLAMACOES = "cli_reclamacoes";
    public static final String SP_TIMELINE = "cli_timeline";
    public static final String SP_RAT = "cli_rat";
    public static final String SP_SEGURANCA_ELETRONICA = "cli_seguranca";
    public static final String SP_ESTATISTICA = "cli_sestatistica";
    public static final String SP_COD_CLI = "cod_cli";
    public static final String SP_LOCAL_PUSH_NUMBER = "local_push_number";
    public static final String SP_FOTO_BASE64 = "sp_foto_cli";
    public static final String SP_NOME_CLI = "nome_cli";
    public static final String SP_MIXPANEL = "mix_panel";
    public static final String SP_SENHA_CLI = "senha_cli";
    public static final String SP_CNPJ_CLI_MASK = "cnpj_mask_cli";
    public static final String SP_CNPJ_CLI = "cnpj_cli";
    public static final String SP_DOCUMENTO_CLI = "DOCUMENTOcli";
    public static final String SP_TOKEN_CLI = "token_device_cli";
    public static final String SP_EMAIL = "email_cli";
    public static final String SP_MENU_RESTRICOES = "menu_restricao";
    public static final String SP_GRUPO_CLIENTE = "group_cli";
    public static final String SP_PERMISSION = "PERMISSION_";
    public static final String SP_USER_ACCOUNTS = "user_accounts";
    public static final String SP_ASKED_SEND_MOBILE_ID = "asked_send_mobile_id";

    public static final String SESSAO_INPIRADA = "sessao_inspirada_cli";
    public static final String DOC_FINANCEIRO = "doc_financiero";
    public static final String FAB_LOCAL = "fabl_local";
    public static final String OBJ_RAT = "OBJ_RAT";
    public static final String FORMATO_DATA = "dd MMMM yyyy";
    public static final String TOKEN_MIXPANEL_HOMOLOGACAO = "98aed49ce61a8b255867e28c197b0a65";
    public static final String TOKEN_MIXPANEL = "b51ac794523cc3b40544d77700d4693a";
    public static final String MIXPANEL_DETALHE_RAT = " Detalhes Rat";
    public static final String MIXPANEL_DETALHE_NOTA = " NOTA FISCAL";
    public static final String MIXPANEL_DETALHE_ORDEMSERVICO = " ORDEM SERVICO";
    public static final String MIXPANEL_DETALHE_HISTORICO = " HISTORICO SOL REC";
    public static final String MIXPANEL_DETALHE_PERFIL = " TROCA SENHA";
    public static final String MIXPANEL_ADD_CONTA = " ADD CONTA";
    public static final String NOTA_FILIAL = " NOTA_FILIAL";
    public static final String NOTA_EMPRESA = " NOTA_EMPRESA";
    public static final String NOTA_COMPETENCIA = " NOTA_COMPETENCIA";

    public static final Integer PORTAL_PRODUCT_ID = 2;
    public static final int MENU_CONFIGURACAO = 101010;
    public static final int MENU_ADD_NOVO = 101010101;
    public static final int MENU_USER_INVITE = 5000;
    public static final int MENU_SUBSIDIARY = 5001;
    public static final int NEOID_GRUPO = 773898952;

    /* neo id rats*/
    public static final int NEOID_VIGZUL = 750713060;
    public static final int NEOID_ORSEGUPS = 750714483;
    public static final int NEOID_AUTO_CARGO = 750715037;
    public static final int NEOID_SEYPROL = 758437614;
    public static final int NEOID_VIASEG = 111111111;

    /*BADGES*/
    public static final int BADGE_LOCAL_DEFAULT = 0;
    public static final int BADGE_LOCAL_RAT = 1;
    public static final int BADGE_LOCAL_NF = 2;

    public static final String SP_BADGE_ESTATISITICAS = "badge_estatistica_cli";
    public static final String SP_BADGE_SEGURANCA = "badge_seguranca_cli";
    public static final String SP_BADGE_RAT = "badge_rat_cli";
    public static final String SP_BADGE_NOTAFISCAL = "badge_notafiscal_cli";
    public static final String SP_BADGE_SOLICITACAO = "badge_rat_solicitacao";
    public static final String SP_BADGE_RECLAMACAO = "badge_rat_reclamacao";
    public static final String SP_BADGE_NOTIFICACOES = "badge_rat_notificacoes";
    public static final String SP_BADGE_PANICO = "badge_rat_panico";
    public static final String SP_BADGE_DOCUMENTOS= "badge_rat_documentos";

    public static final String TOKEN_MATEUS = "cG9ydGFsbW9iaWxlMjAxODA5MDM=";
    public static final String TOKEN_HIMAKER = "9FOfmIlBZI2EK5ach5VMhK4mwJkhvONJpwIustTaTsp";


    public static final String URL_LOGIN = DEFAULT_ORSEGUPS_ID_URL + "oauth/token";
    public static final String URL_CHANGE_PASSWORD = DEFAULT_ORSEGUPS_ID_URL + "user/changepassword";
    public static final String URL_FORGOT_PASSWORD = DEFAULT_ORSEGUPS_ID_URL + "user/resetpassword";
    public static final String URL_INVITE_USER = DEFAULT_ORSEGUPS_ID_URL + "user/inviteuser";
    public static final String URL_USER_ACCOUNTS_BY_CNPJ = DEFAULT_ORSEGUPS_ID_URL + "account/by/cpfCnpj/product";
    public static final String URL_USER_ACCOUNTS = DEFAULT_ORSEGUPS_ID_URL + "account/all?product=" + PORTAL_PRODUCT_ID;

    public static final String URL_LISTA_RECLAMACOES = DEFAULT_PORTAL_URL + "listaReclamacoes?";
    public static final String URL_TIMELINE_MOBILE = DEFAULT_PORTAL_URL + "timelinemobile?";
    public static final String URL_LISTA_SEGURANCA_ELETRONICA = DEFAULT_PORTAL_URL + "minhasegurancaeletronica?";
    public static final String URL_LISTA_SOLICITACOES = DEFAULT_PORTAL_URL + "listaSolicitacoes?";
    public static final String URL_ALTERAR_SENHA = DEFAULT_PORTAL_URL + "alterarSenha?";
    public static final String URL_BADGE_NOVO = DEFAULT_PORTAL_URL + "new_badges";
    public static final String URL_LOGOUT = DEFAULT_PORTAL_URL + "logoutmobile/";
    public static final String URL_DOCUMENTO = DEFAULT_PORTAL_URL + "documentos?cgcCpf=";
    public static final String URL_CHAT_USAR = DEFAULT_PORTAL_URL + "configuracao";
    public static final String URL_SOLICITACAO_RECLAMACAO = DEFAULT_PORTAL_URL + "salvarcontato";
    public static final String URL_ADICIONAR_GRUPO = DEFAULT_PORTAL_URL + "adicionarclientegrupo?";
    public static final String URL_ESTATISTIACAS = DEFAULT_PORTAL_URL + "estatisticas?";
    public static final String URL_RAT = DEFAULT_PORTAL_URL + "rats?";
    public static final String URL_AVALIACAO_RAT = DEFAULT_PORTAL_URL + "avaliacaoRat?";
    public static final String URL_COMPETENCIAS_NOTAS = DEFAULT_PORTAL_URL + "competenciaNotas?cgcCpf=";
    public static final String URL_NOTAS_POR_COMPETENCIA = DEFAULT_PORTAL_URL + "notasPorCompetencia?";
    public static final String URL_SAVE_MOBILE_ID = DEFAULT_PORTAL_URL + "registraDevice";
    public static final String URL_REMOVE_MOBILE_ID = DEFAULT_PORTAL_URL + "deleteDevice";

    public static final String URL_ACIONA_EVENTO_PANICO = "https://apps.orsegups.com.br:8080/sigma-service/default/newEvent";

    public static final String URL_CHECK_STATUS = DEFAULT_HIMEKER_API_V2 + "conversation/status";
    public static final String URL_CREATE_CHAT = DEFAULT_HIMEKER_API_V2 + "conversation/create";


    public static final int ID_MENU_ESTATISTICAS = 1;
    public static final int ID_MENU_SEGURANCA = 2;
    public static final int ID_MENU_RAT = 3;
    public static final int ID_MENU_NOTA_FISCAL = 4;
    public static final int ID_MENU_SOLICITACAO = 5;
    public static final int ID_MENU_NOTIFICACOES = 6;
    public static final int ID_MENU_RECLAMACAO = 7;
    public static final int ID_MENU_PANICO = 8;
    public static final int ID_MENU_WHATSAPP = 9;
    public static final int ID_DOCUMMENTOS = 10;
    public static final String ID_CHAT_HIMAKER = "ID_CHAT_HIMAKER";
    public static final String ID_CHAT_NOME_USUARIO = "ID_CHAT_NOME_USUARIO";

}
