package orsegups.com.br.portaldocliente.model;

import org.springframework.http.HttpMethod;

public class RequestURL {

    private String url;
    private HttpMethod method;

    public RequestURL() {

    }

    public RequestURL(String url, HttpMethod method) {
        this.url = url;
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }
}
