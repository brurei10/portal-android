package orsegups.com.br.portaldocliente.activity;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Patterns;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.volley.request.ForgotPasswordRequest;

/**
 * Created by Orsegups on 03/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class EsqueciSenhaActivity extends AppCompatActivity {


    private EditText email;
    private EditText emailConfirm;
    private Button btnSend;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_esqueci_senha);
        context = this;

        email = findViewById(R.id.email);
        emailConfirm = findViewById(R.id.email_confirm);
        btnSend = findViewById(R.id.send_button);

        initBtnSend();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void initBtnSend() {
        btnSend.setOnClickListener(v -> {
            if (validateSubmit())
                new ForgotPasswordRequest(context, email.getText().toString()).exec();
        });
    }

    private boolean validateSubmit() {
        String emailText = email.getText().toString();
        String emailConfirmText = emailConfirm.getText().toString();

        if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            Alertas.error(context, context.getString(R.string.alert_invalid_email));
            return false;
        }

        if (!emailText.equals(emailConfirmText)) {
            Alertas.error(context, context.getString(R.string.alert_confirm_email_diff));
            return false;
        }

        return true;
    }

}



