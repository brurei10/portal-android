package orsegups.com.br.portaldocliente.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.HashMap;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.AuthenticatingWebView;
import orsegups.com.br.portaldocliente.util.AuthenticatingWebViewCallbackMethods;


/**
 * Created by Orsegups on 03/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class WebViewctivity extends AppCompatActivity implements AuthenticatingWebViewCallbackMethods {

    private WebView webView;
    private ProgressDialog myProgressDialog;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_webview);

        context = this;


        webView = findViewById(R.id.webview);

        webView.getSettings().setUseWideViewPort(true);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);


        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        AuthenticatingWebView authenticatingWebView = new AuthenticatingWebView(webView, this);
        authenticatingWebView.makeRequest(getURL());

    }

    private String getURL() {

        return getIntent().getStringExtra("urlEvento");
    }

    /**
     * Display a progress indicator while authenticating. Implements a callback function called by
     * AuthenticatingWebViewCallbackMethods
     */
    @Override
    public final void startProgressDialog() {

        /*
         * Create a progressDialog if it does not exist.
         */
        if (myProgressDialog == null) {
            myProgressDialog = new ProgressDialog(context);
            myProgressDialog.setTitle("Teste Titulo");
            myProgressDialog.setMessage("Teste descricao");
            myProgressDialog.setCancelable(false);
            myProgressDialog.setIndeterminate(true);
        }

        /*
         * Show the progress dialog.
         */
        myProgressDialog.show();
    }

    /**
     * Stop and destroy a progress indicator (if it exists). Implements a callback function called by
     * AuthenticatingWebViewCallbackMethods.
     */
    @Override
    public final void stopProgressDialog() {
        myProgressDialog.hide();
    }

    @Override
    public void displayResults(HashMap<String, String> authorizationReturnParameters) {

    }
}



