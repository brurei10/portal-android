package orsegups.com.br.portaldocliente.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.activity.ViewPDFActivity;
import orsegups.com.br.portaldocliente.model.Arquivo;

public class DocumentoPDFAdapter extends RecyclerView.Adapter {


    private List<Arquivo> mLista;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.linha_arquivo, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        Arquivo arquivo = mLista.get(i);
        ViewHolder viewHolder1 = (ViewHolder) viewHolder;
        String texto = arquivo.getNomeArquivo();
        viewHolder1.mTextViewTitulo.setText(texto);

        viewHolder1.mTextViewSubTitulo.setText(arquivo.getTipoArquivoSite());

        viewHolder1.mLayout.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), ViewPDFActivity.class);
            intent.putExtra(ViewPDFActivity.PDF, arquivo.getUrlDownload());
            intent.putExtra(ViewPDFActivity.NOME_ARQUIVO, arquivo.getNomeArquivo());
            v.getContext().startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }

    public void setLista(List<Arquivo> lista) {
        mLista = lista;
    }


    private static class ViewHolder extends RecyclerView.ViewHolder {
        final View mLayout;
        TextView mTextViewTitulo;
        TextView mTextViewSubTitulo;

        ViewHolder(View view) {
            super(view);
            mTextViewTitulo = view.findViewById(R.id.textoTituloValor);
            mTextViewSubTitulo = view.findViewById(R.id.textoTipoValor);
            mLayout = view.findViewById(R.id.lyt_clid_arquivos);
        }
    }
}
