package orsegups.com.br.portaldocliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.Iconics;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.activity.AddContaActivity;
import orsegups.com.br.portaldocliente.fragment.AlterarSenhaFragment;
import orsegups.com.br.portaldocliente.fragment.ChatFragment;
import orsegups.com.br.portaldocliente.fragment.DocumentosFragment;
import orsegups.com.br.portaldocliente.fragment.EmpresaNotasFiscaisFragment;
import orsegups.com.br.portaldocliente.fragment.HomeFragment2;
import orsegups.com.br.portaldocliente.fragment.IntroChatFragment;
import orsegups.com.br.portaldocliente.fragment.InviteUserFragment;
import orsegups.com.br.portaldocliente.fragment.SobreFragment;
import orsegups.com.br.portaldocliente.fragment.TimeLineFragment;
import orsegups.com.br.portaldocliente.services.ServiceSaveMobileId;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.PermissionUtils;
import orsegups.com.br.portaldocliente.util.UtilImage;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.UserDTO;

public class MainActivity extends AppCompatActivity {

    Drawable drFotoPerfil;
    private Context context;
    private Activity activity;
    private Drawer drawer;
    private Fragment currentFragment;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private android.view.Menu menu;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Iconics.init(getApplicationContext());
        Iconics.registerFont(new GoogleMaterial());
        context = this;
        activity = this;
        SincronizaBaseDados.badgeContador(context, Constantes.BADGE_LOCAL_DEFAULT);

        initAvatar();
        initToolbar();
        sendMobileId();

        String title;
        try {
            int local = Integer.parseInt(getIntent().getExtras().get(Constantes.SP_LOCAL_PUSH_NUMBER).toString());
            Log.i("Local--->", String.valueOf(local));
            switch (local) {
                case 1:
                    currentFragment = new EmpresaNotasFiscaisFragment();
                    title = getString(R.string.doc_nota);
                    break;
                case 3:
                    currentFragment = new DocumentosFragment();
                    title = getString(R.string.doc);
                    break;
                case 0:
                    currentFragment = new HomeFragment2();
                    title = getString(R.string.inicio);
                    break;
                case 5:
                    currentFragment = new ChatFragment();
                    title = getString(R.string.chat_online);
                    break;
                default:
                    currentFragment = new TimeLineFragment(false);
                    title = getString(R.string.time_line);
            }
        } catch (Exception e) {
            Log.i("Local Erro", ";(");
            currentFragment = new HomeFragment2();
            title = getString(R.string.inicio);
        }

        switchFragment(title);
    }

    private void switchFragment(String title) {
        position = drawer.getCurrentSelectedPosition();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout, currentFragment);
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(title);

        if (menu != null) {
            menu.clear();
            getMenuInflater().inflate(R.menu.menu_share, menu);
        }
    }

    private void initAvatar() {
        SharedPreferences sp = this.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String photoBase64 = sp.getString(Constantes.SP_FOTO_BASE64, "");
        PorterDuff.Mode mMode = PorterDuff.Mode.SRC_ATOP;

        if (photoBase64.isEmpty()) {
            drFotoPerfil = getApplicationContext().getResources().getDrawable(R.drawable.ic_person_black_24_px_1);
            drFotoPerfil.setColorFilter(getResources().getColor(R.color.white), mMode);
        } else {
            Bitmap bitmap = UtilImage.retornaImageBitmap(photoBase64);
            drFotoPerfil = new BitmapDrawable(getResources(), bitmap);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AccountHeader header = new AccountHeaderBuilder().withActivity(this)
                .withCompactStyle(true)
                .withProfileImagesVisible(false)
                .withProfiles(getAccounts())
                .withHeaderBackground(R.color.primaryColor)
                .withTextColor(ContextCompat.getColor(this, R.color.white))
                .withOnAccountHeaderListener((view, profile, current) -> {
                    if (!current) {
                        trocaPerfil((ProfileDrawerItem) profile);
                    }
                    return false;
                })
                .build();

        drawer = new DrawerBuilder().withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggleAnimated(true)
                .withAccountHeader(header)
                .withSliderBackgroundColorRes(R.color.white)
                .withDrawerItems(getMenuItems())

                .withOnDrawerItemClickListener((view, position, drawerItem) -> {

                    if (drawerItem.getIdentifier() == Constantes.MENU_ADD_NOVO) {
                        Intent i = new Intent(MainActivity.this, AddContaActivity.class);
                        startActivity(i);
                        return true;
                    } else if (drawerItem.getIdentifier() == Constantes.MENU_USER_INVITE) {
                        currentFragment = new InviteUserFragment();
                        switchFragment("Convidar Usuário");
                    } else if (drawerItem.getIdentifier() == Constantes.MENU_CONFIGURACAO) {
                        currentFragment = new AlterarSenhaFragment();
                        switchFragment("Alterar Senha");
                    }

                    return false;
                })
                .build();

        SharedPreferences sp = this.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        long idSelected = Long.parseLong(sp.getString(Constantes.SP_CNPJ_CLI, ""));
        drawer.setSelection(idSelected);

        header.setActiveProfile(idSelected);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_share, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_home:
                currentFragment = new HomeFragment2();
                switchFragment(getString(R.string.inicio));
                break;
            case R.id.menu_sobre:
                if (!(currentFragment instanceof SobreFragment)) {
                    currentFragment = new SobreFragment();
                    switchFragment("Sobre");
                }
                break;
            case R.id.menu_alterar_senha:
                if (!(currentFragment instanceof AlterarSenhaFragment)) {
                    currentFragment = new AlterarSenhaFragment();
                    switchFragment("Alterar Senha");
                }
                break;
            case R.id.menu_chat:
                if (!(currentFragment instanceof IntroChatFragment)) {
                    currentFragment = new IntroChatFragment();
                    switchFragment("Chat Online");
                }
                break;
            case R.id.menu_sair:
                SincronizaBaseDados.removeToken(context, activity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawer.setSelectionAtPosition(position, false);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            //TODO checar o porque estava voltando direto para a HomeFragment
            super.onBackPressed();
//            currentFragment = new HomeFragment2();
//            initToolbar();
//            switchFragment(getString(R.string.inicio));
        }
    }

    private void sendMobileId() {
        SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        if (!Utils.getAskedSendMobileId(context) || sp.getString(Constantes.SP_TOKEN_CLI, "").isEmpty()) {

            Utils.setAskedSendMobileId(context, true);

            if (!Utils.getOrsegupsIdCnpj(context).equals("")) {

                final SweetAlertDialog dialog = Alertas.genericConfirmDialog(context, "Recebimento de Notificações", "Deseja receber notificações no seu dispositivo?");

                dialog.setConfirmClickListener(sweetAlertDialog -> {

                    new ServiceSaveMobileId(context).execute();
                    sweetAlertDialog.dismissWithAnimation();

                });

            }

        }
    }

    private List<IProfile> getAccounts() {
        List<IProfile> profiles = new ArrayList<>();
        SharedPreferences sp = this.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        try {
            UserDTO user = new ObjectMapper().readValue(sp.getString(Constantes.SP_USER_ORSEGUPS_ID, ""), UserDTO.class);
            List<AccountDTO> accounts = user.getProduct().getAccounts();

            for (AccountDTO account : accounts) {
                //RUA;NUMERO;BAIRRO;CIDADE
                String[] separated = account.getIdentifier().split(";");
                String endereco = separated[0];
                if (separated.length >= 4) {
                    String rua = separated[0];
                    if (!rua.contains(",")) {
                        rua = separated[0] + ", " + separated[1];
                    }
                    endereco = rua + " - " + separated[2] + " / " + separated[3];
                }
                profiles.add(new ProfileDrawerItem()
                        .withName(account.getUserOwnerName())
                        .withEmail(endereco)
                        .withNameShown(true)
                        .withTag(account.getCpfCnpjOwner())
                        .withSelectedColorRes(R.color.Color_AliceBlue)
                        .withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_account_box).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text))
                        .withIdentifier(Long.parseLong(account.getContractCode())));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return profiles;
    }

    private void trocaPerfil(ProfileDrawerItem profile) {
        SharedPreferences sp = this.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString(Constantes.SP_CNPJ_CLI, "" + profile.getIdentifier());
        editor.putString(Constantes.SP_LOGIN_ORSEGUPS_ID, "" + profile.getTag());
        editor.putString(Constantes.SP_CNPJ_CLI_MASK, profile.getName().getText().toString());

        editor.putString(Constantes.SP_TIMELINE, "");
        editor.putString(Constantes.SP_ESTATISTICA, "");
        editor.putString(Constantes.SP_NOTAS_FISCAIS, "");
        editor.putString(Constantes.SP_SEGURANCA_ELETRONICA, "");
        editor.putString(Constantes.SP_SOLICITACOES, "");
        editor.putString(Constantes.SP_RECLAMACOES, "");
        editor.commit();

        try {

            UserDTO user = new ObjectMapper().readValue(sp.getString(Constantes.SP_USER_ORSEGUPS_ID, ""), UserDTO.class);
            PermissionUtils.storePermissions(context, Utils.getAccountByContractCode(user.getProduct().getAccounts(), profile.getIdentifier()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        currentFragment = new HomeFragment2();
        switchFragment(getString(R.string.inicio));
    }

    private List<IDrawerItem> getMenuItems() {
        List<IDrawerItem> items = new ArrayList<>();

        items.add(
                new ProfileSettingDrawerItem()
                        .withName("Convidar usuário")
                        .withDescription("Convidar um novo usuário ")
                        .withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_person_add).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text))
                        .withIdentifier(Constantes.MENU_USER_INVITE)
        );

        items.add(
                new ProfileSettingDrawerItem()
                        .withName("Alterar senha")
                        .withDescription("Alterar senha do usuário")
                        .withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_settings).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text))
                        .withIdentifier(Constantes.MENU_CONFIGURACAO)
        );

        items.add(
                new DividerDrawerItem()
        );

        String nome = !Utils.getOrsegupsIdName(context).equals("") ? Utils.getOrsegupsIdName(context) : Utils.getOrsegupsIdEmail(context);
        String email = !Utils.getOrsegupsIdName(context).equals("") ? Utils.getOrsegupsIdEmail(context) : "";

        if ((!Utils.getOrsegupsIdName(context).equals("") || !Utils.getOrsegupsIdEmail(context).equals("")) && !Utils.getOrsegupsIdCnpj(context).equals("")) {
            email = Utils.applyCpfCnpjMask(Utils.getOrsegupsIdCnpj(context));
        } else if (!Utils.getOrsegupsIdCnpj(context).equals("")) {
            nome = Utils.applyCpfCnpjMask(Utils.getOrsegupsIdCnpj(context));
        }

        ProfileDrawerItem profileDrawerItem = new ProfileDrawerItem()
                .withNameShown(true)
                .withName(nome)
                .withEmail(email)
                .withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_person).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text));

        items.add(profileDrawerItem);
        return items;
    }

}