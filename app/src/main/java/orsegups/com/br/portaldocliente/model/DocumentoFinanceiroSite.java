package orsegups.com.br.portaldocliente.model;

import androidx.annotation.Nullable;

import java.math.BigDecimal;
import java.util.List;

public class DocumentoFinanceiroSite {
    private Long nfse;
    private Long rps;
    private String emissao;
    private String serie;
    private BigDecimal valor;
    @Nullable
    private String linkBoleto;
    private String linkNfse;
    private String linkNfseDownload;
    private String linkDarfPcc;
    private String linkDarfIRRF;
    private String linkGPS;
    private String linkXML;
    private String codSel;
    private String dirNes;
    private String numCgc;
    private Long numDfs;
    private Long codFpg;
    private String sitTit;
    private String codver;
    private String codBar;
    private Long mostraToolTip;
    private List<Boleto> boletos;
    private String cgcEmissor;

    public Long getNfse() {
        return nfse;
    }

    public void setNfse(Long nfse) {
        this.nfse = nfse;
    }

    public Long getRps() {
        return rps;
    }

    public void setRps(Long rps) {
        this.rps = rps;
    }

    public String getEmissao() {
        return emissao;
    }

    public void setEmissao(String emissao) {
        this.emissao = emissao;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Nullable
    public String getLinkBoleto() {
        return linkBoleto;
    }

    public void setLinkBoleto(@Nullable String linkBoleto) {
        this.linkBoleto = linkBoleto;
    }

    public String getLinkNfse() {
        return linkNfse;
    }

    public void setLinkNfse(String linkNfse) {
        this.linkNfse = linkNfse;
    }

    public String getLinkNfseDownload() {
        return linkNfseDownload;
    }

    public void setLinkNfseDownload(String linkNfseDownload) {
        this.linkNfseDownload = linkNfseDownload;
    }

    public String getLinkDarfPcc() {
        return linkDarfPcc;
    }

    public void setLinkDarfPcc(String linkDarfPcc) {
        this.linkDarfPcc = linkDarfPcc;
    }

    public String getLinkDarfIRRF() {
        return linkDarfIRRF;
    }

    public void setLinkDarfIRRF(String linkDarfIRRF) {
        this.linkDarfIRRF = linkDarfIRRF;
    }

    public String getLinkGPS() {
        return linkGPS;
    }

    public void setLinkGPS(String linkGPS) {
        this.linkGPS = linkGPS;
    }

    public String getLinkXML() {
        return linkXML;
    }

    public void setLinkXML(String linkXML) {
        this.linkXML = linkXML;
    }

    public String getCodSel() {
        return codSel;
    }

    public void setCodSel(String codSel) {
        this.codSel = codSel;
    }

    public String getDirNes() {
        return dirNes;
    }

    public void setDirNes(String dirNes) {
        this.dirNes = dirNes;
    }

    public String getNumCgc() {
        return numCgc;
    }

    public void setNumCgc(String numCgc) {
        this.numCgc = numCgc;
    }

    public Long getNumDfs() {
        return numDfs;
    }

    public void setNumDfs(Long numDfs) {
        this.numDfs = numDfs;
    }

    public Long getCodFpg() {
        return codFpg;
    }

    public void setCodFpg(Long codFpg) {
        this.codFpg = codFpg;
    }

    public String getSitTit() {
        return sitTit;
    }

    public void setSitTit(String sitTit) {
        this.sitTit = sitTit;
    }

    public String getCodver() {
        return codver;
    }

    public void setCodver(String codver) {
        this.codver = codver;
    }

    public Long getMostraToolTip() {
        return mostraToolTip;
    }

    public void setMostraToolTip(Long mostraToolTip) {
        this.mostraToolTip = mostraToolTip;
    }

    public String getCodBar() {
        return codBar;
    }

    public void setCodBar(String codBar) {
        this.codBar = codBar;
    }

    public List<Boleto> getBoletos() {
        return boletos;
    }

    public void setBoletos(List<Boleto> boletos) {
        this.boletos = boletos;
    }

    public String getCgcEmissor() {
        return cgcEmissor;
    }

    public void setCgcEmissor(String cgcEmissor) {
        this.cgcEmissor = cgcEmissor;
    }
}
