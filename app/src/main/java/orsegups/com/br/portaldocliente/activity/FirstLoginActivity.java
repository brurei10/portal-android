package orsegups.com.br.portaldocliente.activity;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

public class FirstLoginActivity extends AppCompatActivity {

    // UI references.
    private EditText password;
    private EditText passwordConfirm;
    private Button btnSend;
    private Context context;

    private String currentPassword;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_first_login);
        context = this;

        password = findViewById(R.id.txt_password);
        passwordConfirm = findViewById(R.id.txt_password_confirm);
        btnSend = findViewById(R.id.send_button);

        currentPassword = getIntent().getStringExtra("currentPassword");
        email = Utils.getOrsegupsIdEmail(context);

        initBtnSend();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    private void initBtnSend() {
        btnSend.setOnClickListener(v -> {
            if (validateSubmit()) {
                SincronizaBaseDados.changePassword(context, currentPassword, password.getText().toString(), email, true);
            }
        });
    }

    private boolean validateSubmit() {
        String passwordText = password.getText().toString();
        String passwordConfirmText = passwordConfirm.getText().toString();

        if (passwordText.length() < 6) {
            Alertas.error(context, context.getString(R.string.alert_min_password_6));
            return false;
        }

        if (!passwordText.equals(passwordConfirmText)) {
            Alertas.error(context, context.getString(R.string.alert_confirm_password_diff));
            return false;
        }

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

