package orsegups.com.br.portaldocliente.fragment;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.adapter.TimelineListAdapter;
import orsegups.com.br.portaldocliente.firebase.MyFirebaseMessagingService;
import orsegups.com.br.portaldocliente.model.Boleto;
import orsegups.com.br.portaldocliente.model.NotaTimeline;
import orsegups.com.br.portaldocliente.model.Rat;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.OnLoadMoreListener;
import orsegups.com.br.portaldocliente.volley.Net.OnTaskCompleted;
import orsegups.com.br.portaldocliente.volley.Net.ServiceHandler;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class TimeLineFragment extends Fragment implements OnTaskCompleted {

    public RecyclerView mRecyclerView;
    View view;
    private TimelineListAdapter mAdapter;
    private TextView emptyView;
    private PtrClassicFrameLayout mPtrFrame;
    private ImageLoader mImageLoader;
    private LinearLayoutManager mLayoutManager;
    private SharedPreferences sp;
    private List<Object> objetos;
    private ProgressBar progressBar;

    private boolean isFirst = true;
    private Thread thread;
    private Handler handler;
    private int NUMPAG = 0;
    private Boolean isFinalLista = false;
    private SweetAlertDialog alerta;
    private LinearLayout linearPai;
    private boolean isRat = false;

    public TimeLineFragment() {
        super();
    }

    @SuppressLint("ValidFragment")
    public TimeLineFragment(boolean isRat) {
        this.isRat = isRat;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_timeline, container, false);
        emptyView = view.findViewById(R.id.empty_view);
        progressBar = view.findViewById(R.id.item_progress_bar);

        setHasOptionsMenu(true);
        objetos = new ArrayList<>();
        handler = new Handler();

        sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        Log.i("SESSION ID >>> ", sp.getString(Constantes.SP_SESSION_ID, ""));
        objetos = new ArrayList<>();
        MixpanelUtil.track(Utils.getCnpjLogado(view.getContext()), "TimeLineFragment", "TIMELINE", view.getContext());

        initImageLoader();
        configuraRefresh();

        return view;
    }


    private void configuraRefresh() {

        boolean isConexao = Utils.verificaConexaoInternet();

        if (isConexao) {
            alerta = Alertas.wait(view.getContext(), "");
            NUMPAG = 0;
            objetos = new ArrayList<>();
            callRat();

            if (sp.getInt(Constantes.SP_BADGE_RAT, 0) > 0) {
                MyFirebaseMessagingService.setBadge(view.getContext(), 0);
            }

        } else {
            configuraSemConexao();
        }
    }

    private void configuraSemConexao() {

    }


    private void initItensTimeLine(JsonElement element, boolean isRefresh) {

        int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

        if (total == 0) {

            isFinalLista = true;
        }

        if (NUMPAG > 0) {

            addItemList(element, false);
            mAdapter.setLoaded();
        }

        if (NUMPAG == 0) {


            SincronizaBaseDados.updateBadge(view.getContext(), Constantes.BADGE_LOCAL_DEFAULT);

            if (total <= 9) {

                isFinalLista = true;
            }

            addItemList(element, true);


            configureList();
        }

    }

    private void addItemList(JsonElement element, boolean isPrimeiraPagina) {

        Gson gson = new Gson();

        int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();

        for (int i = 0; i < total; i++) {

            String tipo = element.getAsJsonObject().get("ret").getAsJsonArray().get(i).getAsJsonObject().get("tipReg").toString();
            String json = element.getAsJsonObject().get("ret").getAsJsonArray().get(i).getAsJsonObject().get("obj").toString();

            switch (tipo) {
                case "1":
                    NotaTimeline nota = gson.fromJson(json, NotaTimeline.class);
                    objetos.add(nota);

                    break;
                case "2":
                    Boleto boleto = gson.fromJson(json, Boleto.class);
                    boolean situacao = element.getAsJsonObject().get("ret").getAsJsonArray().get(i).getAsJsonObject().get("obj").getAsJsonObject().get("sitTit").getAsBoolean();
                    boleto.setSituacao(situacao);
                    objetos.add(boleto);

                    break;
                case "4":
                    Rat rat = gson.fromJson(json, Rat.class);
                    objetos.add(rat);

                default:
                    break;
            }

            if (!isPrimeiraPagina) {
                mAdapter.notifyItemInserted(objetos.size());
            }

        }
    }

    private void configureList() {


        mRecyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);


        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new TimelineListAdapter(view.getContext(), objetos, mImageLoader, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);

        //todo declarar variavel texto vazio
        if (objetos.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                if (!isFinalLista) {
                    progressBar.setVisibility(View.VISIBLE);

                    final Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);

                            ++NUMPAG;
                            callRat();
                        }
                    };

                    handler.post(r);


                }

            }
        });

        if (alerta != null) {
            this.alerta.dismiss();
        }


    }

    public void callRat() {
        String cnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");

        String url = Constantes.URL_TIMELINE_MOBILE
                + "contractCode=" + Utils.getContractCode(view.getContext())
                + "&cgcCpf=" + cnpj
                + "&numPag=" + NUMPAG;


        if (isRat) {
            url = Constantes.URL_RAT
                    + "contractCode=" + Utils.getContractCode(view.getContext())
                    + "&cgcCpf=" + cnpj
                    + "&numPag=" + NUMPAG;
        }

        Log.i("URL: ", url);

        ServiceHandler.makeServiceCall(url, ServiceHandler.ServiceHandlerEnum.GET, getContext(), this);
    }


    private void initImageLoader() {

        DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.erro_img)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration conf = new ImageLoaderConfiguration.Builder(view.getContext())
                .defaultDisplayImageOptions(mDisplayImageOptions)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(conf);

    }

    @Override
    public void onTaskCompleted(@NonNull String response) {

        try {
            JsonParser jsonParser = new JsonParser();
            JsonElement element;
            try {
                element = jsonParser.parse(response);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
            String msg = element.getAsJsonObject().get("msg").getAsString();

            if (status == 100) {


                SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(Constantes.SP_TIMELINE, response);
                editor.commit();

                initItensTimeLine(element, false);


            } else {

                if (msg.replace("\"\"", "").equalsIgnoreCase("Nenhum registro encontrado")) {
                    emptyView.setVisibility(View.VISIBLE);
                    final Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            alerta.dismiss();

                        }
                    };

                    handler.post(r);
                } else {
                    Alertas.erroInesperado(view.getContext(), msg.replace("\"\"", ""));
                }


            }

        } catch (Exception e) {
            Alertas.erroComunicacaoServidor(view.getContext());
            e.printStackTrace();
            configuraSemConexao();
        }

    }
}