package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.ClienteGrupoEmpresa;

public class ClienteGrupoEmpresaDao extends BaseDaoImpl<ClienteGrupoEmpresa, Integer> {
    public ClienteGrupoEmpresaDao(ConnectionSource cs) throws SQLException {
        super(ClienteGrupoEmpresa.class);
        setConnectionSource(cs);
        initialize();
    }
}
