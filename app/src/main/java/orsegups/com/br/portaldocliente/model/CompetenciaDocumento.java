package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

public class CompetenciaDocumento implements Serializable {
    private String competencia;
    private List<ArquivoSite> arquivos;


    public CompetenciaDocumento() {
        super();
    }

    public CompetenciaDocumento(String competencia, List<ArquivoSite> arquivos) {
        super();
        this.competencia = competencia;
        this.arquivos = arquivos;
    }

    public String getCompetencia() {
        return competencia;
    }

    public List<ArquivoSite> getArquivos() {
        return arquivos;
    }

    @NonNull
    @Override
    public String toString() {
        return "CompetenciaDocumento [competencia=" + competencia + ", arquivos=" + arquivos + "]";
    }


}
