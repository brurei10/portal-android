package orsegups.com.br.portaldocliente.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.adapter.PanicoListAdapter;
import orsegups.com.br.portaldocliente.model.ContasSigmaSite;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class PanicoFragment extends Fragment {

    public RecyclerView recyclerView;
    private TextView emptyView;


    View view;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_panico, container, false);
        emptyView = (TextView) view.findViewById(R.id.empty_view);
        setHasOptionsMenu(true);
        configuraSemConexao();
        // MixpanelUtil.track(Utils.getCnpjLogado(view.getContext()),"SEGURANCA ELETRONICA","SEGURANCA ELETRONICA",view.getContext());
        return view;
    }

    private void configuraSemConexao() {

        SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
        String response = sp.getString(Constantes.SP_SEGURANCA_ELETRONICA, "");

        List<ContasSigmaSite> reclamacoes = new ArrayList<>();

        if (!response.isEmpty()) {
            try {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement element = jsonParser.parse(response);
                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();
                for (int i = 0; i < total; i++) {

                    ContasSigmaSite reclamacao = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(i), ContasSigmaSite.class);

                    if(reclamacao.getTipoConta().equals("ALARME")) {
                        reclamacoes.add(reclamacao);
                    }


                }
            } catch (Exception e) {
            }
        }
        configureList(reclamacoes);
    }


    private void configureList(List<ContasSigmaSite> reclamacoes) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        PanicoListAdapter mAdapter = new PanicoListAdapter(view.getContext(), reclamacoes);
        recyclerView.setAdapter(mAdapter);

        if (reclamacoes.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

    }
}


