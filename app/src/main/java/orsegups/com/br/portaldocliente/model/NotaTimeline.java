package orsegups.com.br.portaldocliente.model;

import java.math.BigDecimal;

public class NotaTimeline {
    private Long nfse;
    private Long rps;
    private String emissao;
    private String serie;
    private BigDecimal valor;
    private String linkNfse;
    private Long numDfs;
    private String datHorEnv;

    public NotaTimeline() {
    }

    public Long getNfse() {
        return nfse;
    }

    public Long getRps() {
        return rps;
    }

    public String getEmissao() {
        return emissao;
    }

    public String getSerie() {
        return serie;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public String getLinkNfse() {
        return linkNfse;
    }

    public Long getNumDfs() {
        return numDfs;
    }

    public String getDatHorEnv() {
        return datHorEnv;
    }
}
