package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by filipe.neis on 19/04/2017.
 */

public class BadgeContador {

    @NonNull
    private List<BadgeRat> mListaRats = new ArrayList<>();
    @NonNull
    private List<BadgeNota> mListxaNotas = new ArrayList<>();

    /**
     * @param cnpj quando passar o parametro null, ele soma todas as qtds de todos cnpj/cpf
     */
    public int getNotasfiscaisCount(@Nullable String cnpj) {
        int total = 0;
        for (BadgeNota nota :
                mListxaNotas) {
            if (cnpj != null) {
                if (nota.getCpfCnpj().equalsIgnoreCase(cnpj)) {
                    total += nota.getQtd();
                }
            } else {
                total += nota.getQtd();
            }
        }
        return total;
    }

    private int getNotasTotalCount() {
        return getNotasfiscaisCount(null);
    }

    private int getRatTotalCount() {
        return getRatCount(null);
    }

    /**
     * @param contractCode quando passar o parametro null, ele soma todas as qtds de todos contractCode
     */
    public int getRatCount(@Nullable String contractCode) {
        int total = 0;
        for (BadgeRat rat :
                mListaRats) {
            if (contractCode != null) {
                if (rat.getContractCode().equalsIgnoreCase(contractCode)) {
                    total += rat.getQtd();
                }
            } else {
                total += rat.getQtd();
            }
        }
        return total;

    }

    public int getTotal() {
        return getNotasTotalCount() + getRatTotalCount();
    }

    @NonNull
    public List<BadgeRat> getListaRats() {
        return mListaRats;
    }

    public void setListaRats(@NonNull List<BadgeRat> listaRats) {
        mListaRats = listaRats;
    }

    @NonNull
    public List<BadgeNota> getListxaNotas() {
        return mListxaNotas;
    }

    public void setListxaNotas(@NonNull List<BadgeNota> listxaNotas) {
        mListxaNotas = listxaNotas;
    }

}
