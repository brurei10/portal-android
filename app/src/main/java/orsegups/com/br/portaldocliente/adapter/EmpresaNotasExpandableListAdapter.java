package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.activity.NotaFiscalActivity;
import orsegups.com.br.portaldocliente.model.DocumentoFinanceiroSite;
import orsegups.com.br.portaldocliente.model.EmpresaDocFinanceiroSite;

/**
 * Created by Orsegups on 23/02/2017.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author filipe.neis
 */
public class EmpresaNotasExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> mHeader; // header titles
    // Child nfeData in format of header title, child title
    private HashMap<String, List<String>> mChild;
    @Nullable
    private List<EmpresaDocFinanceiroSite> mEmpresaDocFinanceiroSites;

    public EmpresaNotasExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData, @Nullable List<EmpresaDocFinanceiroSite> empresaDocFinanceiroSites) {
        this.mContext = context;
        this.mHeader = listDataHeader;
        this.mChild = listChildData;
        this.mEmpresaDocFinanceiroSites = empresaDocFinanceiroSites;
    }

    @Override
    public int getGroupCount() {
        // Get header size
        return this.mHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<String> lista = this.mChild.get(this.mHeader.get(groupPosition));
        if (lista != null) {
            return lista.size();
        }

        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        // Get header position
        return this.mHeader.get(groupPosition);
    }

    @Nullable
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<String> lista = this.mChild.get(this.mHeader.get(groupPosition));
        if (lista != null && lista.size() > childPosition) {
            return lista.get(childPosition);
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {

        return false;
    }

    private String cpfCnpj(String s) {

        if (s.length() <= 11) {
            s = orsegups.com.br.portaldocliente.util.Utils.completeToLeft(s, 11);
            return s.substring(0, 3) + "." + s.substring(3, 6) + "." + s.substring(6, 9) + "-" + s.substring(9, 11);
        } else {
            s = orsegups.com.br.portaldocliente.util.Utils.completeToLeft(s, 14);
            return (s.substring(0, 2) + "." + s.substring(2, 5) + "." +
                    s.substring(5, 8) + "/" + s.substring(8, 12) + "-" +
                    s.substring(12, 14));
        }
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.header_empresas_notas, parent, false);
        }


        String cnpj;
        if (mEmpresaDocFinanceiroSites != null) {

            TextView header_text = convertView.findViewById(R.id.header);
            cnpj = orsegups.com.br.portaldocliente.util.Utils.completeToLeft(mEmpresaDocFinanceiroSites.get(groupPosition).getCnpj(), 14);
            String nome = mEmpresaDocFinanceiroSites.get(groupPosition).getNomeEmpresa();
            String headerTitle = "CNPJ: " + cpfCnpj(cnpj) + "\nEmpresa: " + nome;//getGroup(groupPosition);
            //    header_text.setText(headerTitle);
            Spannable sText = new SpannableString(headerTitle);
            sText.setSpan(new RelativeSizeSpan(0.8f), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sText.setSpan(new RelativeSizeSpan(0.8f), 24, 33, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            //sText.setSpan(new ForegroundColorSpan(Color.BLUE), 5, 9, 0);
            header_text.setText(sText);

            // If group is expanded then change the text into bold and change the
            // icon
            if (isExpanded) {
                header_text.setTypeface(null, Typeface.BOLD);
                // header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, '\ue5c7', 0);//up
            } else {
                // If group is not expanded then change the text back into normal
                // and change the icon

                header_text.setTypeface(null, Typeface.NORMAL);
                //header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, '\ue5c7', 0); //dow
            }
        }


        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Getting child text
        String childText = "";// getChild(groupPosition, childPosition);
        if (mEmpresaDocFinanceiroSites != null) {
            childText = mEmpresaDocFinanceiroSites.get(groupPosition).getCompetenciaDocFinanceiroSite().get(childPosition).getCompetencia();
        }
        // Inflating child layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_empresas_notas, parent, false);
        }

        TextView child_text = convertView.findViewById(R.id.child);
        LinearLayout lyt = convertView.findViewById(R.id.lyt_clid_empresas_notas);
        RelativeLayout child_relative = convertView.findViewById(R.id.child_relative);

        child_relative.setVisibility(View.GONE);

        if (mEmpresaDocFinanceiroSites.get(groupPosition).getCompetenciaDocFinanceiroSite().get(childPosition).getSituacaoTitulo().equalsIgnoreCase("AB")) {
            child_relative.setVisibility(View.VISIBLE);
        }

        lyt.setOnClickListener(v -> {

            if (mEmpresaDocFinanceiroSites.size() > groupPosition && mEmpresaDocFinanceiroSites.get(groupPosition).getCompetenciaDocFinanceiroSite().size() > childPosition) {
                EmpresaDocFinanceiroSite empresaDocFinanceiroSite = mEmpresaDocFinanceiroSites.get(groupPosition);
                List<DocumentoFinanceiroSite> lista = new ArrayList<>();
                for (DocumentoFinanceiroSite doc : mEmpresaDocFinanceiroSites.get(groupPosition).getCompetenciaDocFinanceiroSite().get(childPosition).getDocumentoFinanceiroSiteDTO()
                ) {
                    if (doc.getCgcEmissor().equalsIgnoreCase(empresaDocFinanceiroSite.getCnpj())) {
                        lista.add(doc);
                    }
                }
                orsegups.com.br.portaldocliente.util.Utils.setDocumentoFinanceiroSites(lista);
                Intent intent = new Intent(mContext, NotaFiscalActivity.class);
                intent.putExtra(orsegups.com.br.portaldocliente.util.Constantes.NOTA_FILIAL, mEmpresaDocFinanceiroSites.get(groupPosition).getCodigoFilial().toString());
                intent.putExtra(orsegups.com.br.portaldocliente.util.Constantes.NOTA_EMPRESA, mEmpresaDocFinanceiroSites.get(groupPosition).getCodigoEmpresa().toString());
                intent.putExtra(orsegups.com.br.portaldocliente.util.Constantes.NOTA_COMPETENCIA, mEmpresaDocFinanceiroSites.get(groupPosition).getCompetenciaDocFinanceiroSite().get(childPosition).getCompetencia());

                mContext.startActivity(intent);
            }


        });

        child_text.setText(childText);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

}
