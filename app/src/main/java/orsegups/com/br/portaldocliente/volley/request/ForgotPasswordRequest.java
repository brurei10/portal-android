package orsegups.com.br.portaldocliente.volley.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.volley.common.AbstractRequest;
import orsegups.com.br.portaldocliente.volley.oauth.OAuth2Request;

public class ForgotPasswordRequest extends AbstractRequest {

    private final String email;

    public ForgotPasswordRequest(Context context, String email) {
        super(context);
        this.email = email;
    }

    public void exec() {
        awaitDialog.show();

        OAuth2Request oAuth2Request = new OAuth2Request() {
            @Override
            public void response(final String token) {
                try {
                    String url = Constantes.URL_FORGOT_PASSWORD;

                    final JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", email);

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, jsonObject, successResponse(), errorResponse()) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("Authorization", "Bearer " + token);
                            params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                            return params;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }
                    };

                    makeRequest(jsonObjectRequest);
                } catch (JSONException e) {
                    awaitDialog.dismiss();
                    Alertas.error(context, context.getString(R.string.alert_error_contact_support));
                }
            }
        };

        oAuth2Request.request(context);
    }

    private Response.Listener<JSONObject> successResponse() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                awaitDialog.dismiss();
                Alertas.resetPassword(context);
            }
        };
    }

    private Response.ErrorListener errorResponse() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("d", "Erro");
                awaitDialog.dismiss();
                switch (error.networkResponse.statusCode) {
                    case 400:
                    case 404:
                        Alertas.error(context, context.getString(R.string.alert_email_not_found));
                        break;
                    case 401:
                        Alertas.error(context, context.getString(R.string.alert_error_contact_support));
                        break;
                    default:
                        Alertas.erroComunicacaoServidor(context);
                }
            }
        };
    }

}
