package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

public class BadgeRat {

    private int qtd;
    private int cdCliente;
    @NonNull
    private String contractCode = "";

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public int getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(int cdCliente) {
        this.cdCliente = cdCliente;
    }

    @NonNull
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(@NonNull String contractCode) {
        this.contractCode = contractCode;
    }
}
