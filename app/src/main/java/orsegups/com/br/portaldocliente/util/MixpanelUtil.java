package orsegups.com.br.portaldocliente.util;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by filipeamaralneis on 25/08/17.
 */

public class MixpanelUtil {

    public static final String MIXPANELTOKEN = Constantes.TOKEN_MIXPANEL;

    public static void  track(String cnpj, String event ,String group, Context contexto) {

        MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(contexto, MIXPANELTOKEN);
        JSONObject props = new JSONObject();
        String email = Utils.getEmail(contexto);

        try {
            props.put("GROUP", group);
            props.put("CNPJ", cnpj);
            props.put("NOME", Utils.getNome(contexto));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(email != null && !email.isEmpty())
            mixpanelAPI.identify(email);

        mixpanelAPI.track(event,props);
    }

    public static void trackMessage(String cnpj, String event, String group, Context contexto, String mensagem) {

        MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(contexto, MIXPANELTOKEN);
        JSONObject props = new JSONObject();
        String email = Utils.getEmail(contexto);

        try {
            props.put("GROUP", group);
            props.put("CNPJ", cnpj);
            props.put("MSG", mensagem);
            props.put("NOME", Utils.getNome(contexto));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (email != null && !email.isEmpty())
            mixpanelAPI.identify(email);

        mixpanelAPI.track(event, props);
    }
}
