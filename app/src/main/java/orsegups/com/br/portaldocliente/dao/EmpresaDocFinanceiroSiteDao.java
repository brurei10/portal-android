package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.EmpresaDocFinanceiroSite;


public class EmpresaDocFinanceiroSiteDao extends BaseDaoImpl<EmpresaDocFinanceiroSite, Integer> {
    public EmpresaDocFinanceiroSiteDao(ConnectionSource cs) throws SQLException {
        super(EmpresaDocFinanceiroSite.class);
        setConnectionSource(cs);
        initialize();
    }
}
