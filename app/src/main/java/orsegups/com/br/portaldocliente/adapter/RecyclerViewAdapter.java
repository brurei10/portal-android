package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.ImagemCameraRat;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

    private List<ImagemCameraRat> itemList;
    private Context context;
    private ImageLoader mImageLoader;

    public RecyclerViewAdapter(Context context, List<ImagemCameraRat> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_imagem, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView, itemList);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        initImageLoader();
        holder.context = context;
        setImage(holder.countryPhoto, itemList.get(position).getLnkImg());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    private void initImageLoader() {

        DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.erro_img)
                // .showImageOnLoading(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                //.displayer(new FadeInBitmapDisplayer(1000))
                .build();

        ImageLoaderConfiguration conf = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(mDisplayImageOptions)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(10)
                .writeDebugLogs()
                .build();
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(conf);

    }

    private void setImage(ImageView image, String url) {

        mImageLoader.displayImage(url,
                image,
                null,
                new ImageLoadingListener() {

                    @Override
                    public void onLoadingCancelled(String uri, View view) {
                        //  Log.i("Script", "onLoadingCancelled()");
                    }

                    @Override
                    public void onLoadingComplete(String uri, View view, Bitmap bmp) {
                        //  Log.i("Script", "onLoadingComplete()");
                    }

                    @Override
                    public void onLoadingFailed(String uri, View view, FailReason fail) {
                        //Log.i("Script", "onLoadingFailed("+fail+")");
                    }

                    @Override
                    public void onLoadingStarted(String uri, View view) {
                        // Log.i("Script", "onLoadingStarted()");
                    }

                }, (uri, view, current, total) -> {

                    //Log.i("Script", "onProgressUpdate("+uri+" : "+total+" : "+current+")");
                });

    }
}

