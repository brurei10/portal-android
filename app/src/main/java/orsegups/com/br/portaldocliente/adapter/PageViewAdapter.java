package orsegups.com.br.portaldocliente.adapter;

/**
 * Created by filipe.neis on 23/03/2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.ContaSigma;
import orsegups.com.br.portaldocliente.model.Grafico;
import orsegups.com.br.portaldocliente.model.PontosGrafico;

public class PageViewAdapter extends PagerAdapter {

    private List<View> viewList;
    private List<ContaSigma> contaSigmas;
    private List<String> titulos;
    private RadioButton sTatico, sDesvio, sOrdem;
    private RadioGroup radioGroup;
    private Context context;

    public PageViewAdapter(List<ContaSigma> contaSigmas, List<String> titulos, Context context) {
        this.viewList = new ArrayList<>();
        this.contaSigmas = contaSigmas;
        this.titulos = titulos;
        this.context = context;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View v = viewList.get(position);
        configuraGrafico(v, contaSigmas.get(position).getGraficos(), position);
        collection.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return viewList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @NonNull
    List<View> getData() {
        if (viewList == null) {
            viewList = new ArrayList<>();
        }

        return viewList;
    }

    public void setData(@Nullable List<View> list) {
        this.viewList.clear();
        if (list != null && !list.isEmpty()) {
            this.viewList.addAll(list);
        }

        notifyDataSetChanged();
    }

    private void configuraGrafico(final View v, List<Grafico> graficos, final int position) {


        final TextView tituloEstastitica = v.findViewById(R.id.dashboard_titulo_estatistica);
        tituloEstastitica.setText(titulos.get(position));


        sTatico = v.findViewById(R.id.switch_tatico);
        sDesvio = v.findViewById(R.id.switch_desvio);
        sOrdem = v.findViewById(R.id.switch_ordem);


        LineChart lineChart = v.findViewById(R.id.chart1);

        lineChart.getDescription().setEnabled(false);
        lineChart.setDrawGridBackground(false);
        lineChart.setGridBackgroundColor(Color.TRANSPARENT);


        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setGranularity(3f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            private SimpleDateFormat mFormat = new SimpleDateFormat("dd/MM");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mFormat.format(new Date((long) value));

            }
        });
        xAxis.setGridColor(Color.TRANSPARENT);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setTextColor(Color.TRANSPARENT);


        lineChart.setData(generateDataLine(v, graficos, lineChart));

        Legend l = lineChart.getLegend();
        l.setTextColor(Color.TRANSPARENT);
        l.setEnabled(false);
        l.resetCustom();

        lineChart.animateX(750);


        //// TODO: 17/05/17 Detalhe Estatistica
//        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
//            @Override
//            public void onValueSelected(Entry e, Highlight h) {
//                //  detalhesEstatisticas(v);
//            }
//
//            @Override
//            public void onNothingSelected() {
//
//            }
//        });

    }

    private LineData generateDataLine(View view, List<Grafico> graficos, LineChart lineChart) {

        ArrayList<Entry> atTaticos = new ArrayList<>();
        ArrayList<Entry> desHabitos = new ArrayList<>();
        ArrayList<Entry> orServicos = new ArrayList<>();


        for (Grafico grafico : graficos) {

            // Ordem de servico
            if (grafico.getTipGra() == 1L) {
                for (PontosGrafico pg : grafico.getLisPon()) {
                    Entry e = new Entry();
                    e.setX(Float.parseFloat(pg.getData()));
                    e.setY(pg.getTempo());
                    orServicos.add(e);

                }

            }
            // Atendimento tatico
            if (grafico.getTipGra() == 2L) {
                for (PontosGrafico pg : grafico.getLisPon()) {
                    Entry e = new Entry();
                    e.setX(Float.parseFloat(pg.getData()));
                    e.setY(pg.getTempo());
                    atTaticos.add(e);

                }

            }

            // Desvio de habito
            if (grafico.getTipGra() == 3L) {
                for (PontosGrafico pg : grafico.getLisPon()) {
                    Entry e = new Entry();
                    e.setX(Float.parseFloat(pg.getData()));
                    e.setY(pg.getTempo());
                    desHabitos.add(e);
                }

            }

        }


        return configCheks(view, lineChart, atTaticos, desHabitos, orServicos);
    }

    private LineData configCheks(View view, final LineChart lineChart, final ArrayList<Entry> atTaticos, final ArrayList<Entry> desHabitos, final ArrayList<Entry> orServicos) {
        ArrayList<ILineDataSet> sets = new ArrayList<>();
        if (!atTaticos.isEmpty()) {
            sets.add(createSet(atTaticos, 1));
            sTatico.setChecked(true);
            confLeftAxis(lineChart, false);

            if (orServicos.isEmpty()) {
                sOrdem.setEnabled(false);

            } else {
                sOrdem.setEnabled(true);
            }
            if (desHabitos.isEmpty()) {
                sDesvio.setEnabled(false);
            } else {
                sDesvio.setEnabled(true);
            }


        } else if (!desHabitos.isEmpty()) {
            sets.add(createSet(desHabitos, 2));
            confLeftAxis(lineChart, false);
            sDesvio.setChecked(true);


            if (orServicos.isEmpty()) {
                sOrdem.setEnabled(false);
            } else {
                sOrdem.setEnabled(true);
            }
            if (atTaticos.isEmpty()) {
                sTatico.setEnabled(false);
            } else {
                sTatico.setEnabled(true);
            }

        } else if (!orServicos.isEmpty()) {
            sets.add(createSet(orServicos, 3));
            confLeftAxis(lineChart, true);
            sOrdem.setChecked(true);

            if (desHabitos.isEmpty()) {
                sDesvio.setEnabled(false);
            } else {
                sDesvio.setEnabled(true);
            }
            if (atTaticos.isEmpty()) {
                sTatico.setEnabled(false);
            } else {
                sTatico.setEnabled(true);
            }

        }

        setColorTextRadioButton(sTatico);
        setColorTextRadioButton(sDesvio);
        setColorTextRadioButton(sOrdem);

        LineData cd = new LineData(sets);
        cd.setValueTextColor(Color.WHITE);


        radioGroup = view.findViewById(R.id.radio_grupo);
        //  sTatico.setChecked(true);


        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            Log.d("chk", "id" + checkedId);

            if (checkedId == R.id.switch_tatico) {
                switchClick(lineChart, atTaticos, 1);
                confLeftAxis(lineChart, false);

            } else if (checkedId == R.id.switch_desvio) {

                switchClick(lineChart, desHabitos, 2);
                confLeftAxis(lineChart, false);

            } else if (checkedId == R.id.switch_ordem) {
                switchClick(lineChart, orServicos, 3);
                confLeftAxis(lineChart, true);


            }
        });

        return cd;


    }

    private void switchClick(LineChart lineChart, ArrayList<Entry> lista, int local) {
//        removeDataSet(lineChart);

        removeDataSet(lineChart);
        lineChart.getLineData().addDataSet(createSet(lista, local));

        updateGrafico(lineChart);

    }


    private void updateGrafico(LineChart lineChart) {

        //  lineChart.getLineData().notifyDataChanged();
        lineChart.notifyDataSetChanged();
        lineChart.invalidate();
    }


    private LineDataSet createSet(ArrayList<Entry> lista, int local) {

        int cor = R.drawable.bg_gradient;
        int cor2 = Color.parseColor("#2196F3");

        if (local == 2) {
            cor = R.drawable.bg_gradient2;
            cor2 = Color.parseColor("#678acc");
        } else if (local == 3) {
            cor = R.drawable.bg_gradient3;
            cor2 = Color.parseColor("#689F38");
        }

        Drawable drawable = ContextCompat.getDrawable(context, cor);
        LineDataSet set = new LineDataSet(lista, "DataSet 1");
        set.setLineWidth(2.5f);
        set.setCircleRadius(4.5f);
        set.setColor(cor2);
        set.setCircleColor(set.getColor());
        set.setDrawValues(false);
        set.setDrawFilled(true);
        set.setFillDrawable(drawable);

        return set;
    }

    private void removeDataSet(LineChart mChart) {

        LineData data = mChart.getData();

        if (data != null) {

            data.clearValues();

            mChart.notifyDataSetChanged();
            mChart.invalidate();
        }
    }


    private void setColorTextRadioButton(RadioButton aSwitch) {

        if (aSwitch.isEnabled()) {
            aSwitch.setTextColor(Color.WHITE);
        } else {
            aSwitch.setTextColor(Color.parseColor("#9D9FA2"));
        }
    }

    private void confLeftAxis(LineChart lineChart, final Boolean isDia) {

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setLabelCount(5, false);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setGridColor(Color.TRANSPARENT);
        leftAxis.setValueFormatter((value, axis) -> {

            String valor = String.valueOf(value);
            String tipo = isDia ? " dia" : " min";
            return valor.replaceAll("\\.?0*$", "") + tipo;
        });
    }

}