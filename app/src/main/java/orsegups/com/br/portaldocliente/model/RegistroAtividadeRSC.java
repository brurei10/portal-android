package orsegups.com.br.portaldocliente.model;

import java.util.GregorianCalendar;

public class RegistroAtividadeRSC {
    private String nomeAtividade;
    private String descricao;
    private GregorianCalendar dataInicialRSC;
    private GregorianCalendar dataFinalRSC;
    private GregorianCalendar prazo;
    // Datas para Json I
    private String dataInicialString;
    private String dataFinalString;
    private String prazoString;
    // Datas para Json F
    private Long neoId;
    private String responsavel;

    public String getNomeAtividade() {
        return nomeAtividade;
    }

    public void setNomeAtividade(String nomeAtividade) {
        this.nomeAtividade = nomeAtividade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public GregorianCalendar getDataInicialRSC() {
        return dataInicialRSC;
    }

    public void setDataInicialRSC(GregorianCalendar dataInicialRSC) {
        this.dataInicialRSC = dataInicialRSC;
    }

    public GregorianCalendar getDataFinalRSC() {
        return dataFinalRSC;
    }

    public void setDataFinalRSC(GregorianCalendar dataFinalRSC) {
        this.dataFinalRSC = dataFinalRSC;
    }

    public GregorianCalendar getPrazo() {
        return prazo;
    }

    public void setPrazo(GregorianCalendar prazo) {
        this.prazo = prazo;
    }

    public String getDataInicialString() {
        return dataInicialString;
    }

    public void setDataInicialString(String dataInicialString) {
        this.dataInicialString = dataInicialString;
    }

    public String getDataFinalString() {
        return dataFinalString;
    }

    public void setDataFinalString(String dataFinalString) {
        this.dataFinalString = dataFinalString;
    }

    public String getPrazoString() {
        return prazoString;
    }

    public void setPrazoString(String prazoString) {
        this.prazoString = prazoString;
    }

    public Long getNeoId() {
        return neoId;
    }

    public void setNeoId(Long neoId) {
        this.neoId = neoId;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

}
