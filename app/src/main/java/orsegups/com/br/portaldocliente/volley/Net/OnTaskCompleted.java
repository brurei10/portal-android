package orsegups.com.br.portaldocliente.volley.Net;

import androidx.annotation.NonNull;

/**
 * Created by filipeamaralneis on 02/08/17.
 */

public interface OnTaskCompleted {
    void onTaskCompleted(@NonNull String response);
}

