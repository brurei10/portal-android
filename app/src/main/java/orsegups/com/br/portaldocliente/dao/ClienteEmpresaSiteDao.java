package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.ClienteEmpresaSite;

public class ClienteEmpresaSiteDao extends BaseDaoImpl<ClienteEmpresaSite, Integer> {
    public ClienteEmpresaSiteDao(ConnectionSource cs) throws SQLException {
        super(ClienteEmpresaSite.class);
        setConnectionSource(cs);
        initialize();
    }
}
