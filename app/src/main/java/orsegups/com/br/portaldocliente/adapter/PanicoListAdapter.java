package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.model.ContasSigmaSite;

/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */
public class PanicoListAdapter extends RecyclerView.Adapter<PanicoListAdapter.ViewHolder> {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<ContasSigmaSite> filtered_items = new ArrayList<>();
    private Context ctx;
    // private OnItemClickListener mOnItemClickListener;


//    public interface OnItemClickListener {
//        void onItemClick(View view, int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mOnItemClickListener = mItemClickListener;
//    }


    public PanicoListAdapter(Context context, List<ContasSigmaSite> items) {
        filtered_items = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
    }

    @NonNull
    @Override
    public PanicoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_panico, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final ContasSigmaSite conta = filtered_items.get(position);

        String particao = Utils.firstCharacterUpper(conta.getParticao()) + " " + Utils.firstCharacterUpper(conta.getTipoConta());
        String endereco = Utils.firstCharacterUpper(conta.getEndereco()) + " - " + Utils.firstCharacterUpper(conta.getBairro()) + " - " + Utils.firstCharacterUpper(conta.getCidade()) + " - " + conta.getUf().toUpperCase();
        String fantasia = Utils.firstCharacterUpper(conta.getFantasia());


        final String msg = particao + " - " + fantasia + "\n" + endereco;

        holder.conta.setText(WordUtils.capitalize(msg));
        holder.acionarConta.setOnClickListener(v -> {

            SweetAlertDialog dialog = Alertas.msgAcionarConta(ctx, msg);
            dialog.setConfirmClickListener(sweetAlertDialog -> Log.d("Filipe", "Panico com sucesso"));

        });


    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each nfeData item is just a string in this case
        TextView conta;
        Button acionarConta;

        public ViewHolder(View v) {
            super(v);
            conta = (TextView) v.findViewById(R.id.row_panico_conta);
            acionarConta = (Button) v.findViewById(R.id.row_panico_acionar);


        }
    }


}