package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.SiteTipoDocumentoCliente;


public class SiteTipoDocumentoClienteDao extends BaseDaoImpl<SiteTipoDocumentoCliente, Integer> {
    public SiteTipoDocumentoClienteDao(ConnectionSource cs) throws SQLException {
        super(SiteTipoDocumentoCliente.class);
        setConnectionSource(cs);
        initialize();
    }
}
