package orsegups.com.br.portaldocliente.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Documento {
    @SerializedName("chkDeacordoRecnf")
    private boolean mChkDeacordoRecnf;
    @SerializedName("intnet")
    private String mIntnet;
    @SerializedName("cpfCli")
    private String mCpfCli;
    @Nullable
    @SerializedName("empresas")
    private List<Empresa> mEmpresas;

    public boolean isChkDeacordoRecnf() {
        return mChkDeacordoRecnf;
    }

    public String getIntnet() {
        return mIntnet;
    }

    public String getCpfCli() {
        return mCpfCli;
    }

    @Nullable
    public List<Empresa> getEmpresas() {
        return mEmpresas;
    }
}
