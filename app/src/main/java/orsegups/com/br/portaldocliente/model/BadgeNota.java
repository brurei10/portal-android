package orsegups.com.br.portaldocliente.model;

import androidx.annotation.NonNull;

public class BadgeNota {

    private int qtd;
    @NonNull
    private String cpfCnpj = "";

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    @NonNull
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(@NonNull String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }
}
