package orsegups.com.br.portaldocliente.volley.conf;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by FABIO on 14/02/2016.
 */
public class IsConnect {

    public static boolean isConnected(Context context) {
        String TAG = "isConnected";
        try {
            ConnectivityManager cm = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
                return true;
            } else  {
                try {
                    ConnectivityManager cm2 = (ConnectivityManager)
                            context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (cm2.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
                        return true;
                    }

                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    return false;
                }

                return false;
            }
        } catch (Exception e) {
            Log.e(TAG,e.getMessage());
            return false;
        }
    }
}
