package orsegups.com.br.portaldocliente.model;

import java.io.Serializable;

/**
 * Created by filipeamaralneis on 17/04/2018.
 */

public class MenuType implements Serializable {

    private int id;
    private String nome;


    public MenuType(int id, String nome) {

        this.id = id;
        this.nome = nome;

    }

    public MenuType() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}