package orsegups.com.br.portaldocliente.activity;


import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.adapter.HistoricoReclamacaoListAdapter;
import orsegups.com.br.portaldocliente.model.HistoricoReclamacao;


/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */

public class HistoricoReclamacoescActivity extends AppCompatActivity {

    public RecyclerView recyclerView;
    private Toolbar toolbar;
    private HistoricoReclamacaoListAdapter mAdapter;
    private List<HistoricoReclamacao> registroAtividadeRSCList;
    private ActionBar actionBar;
    private TextView emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_historico_sol_rec);

        MixpanelUtil.track(Utils.getCnpjLogado(this), Constantes.MIXPANEL_DETALHE_HISTORICO, "HISTORICO RECLAMACAO", this);


        initToolbar();
        initLista();


    }

    public void initLista() {
        emptyView = findViewById(R.id.empty_view);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        //  mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        registroAtividadeRSCList = new ArrayList<>();
        registroAtividadeRSCList = Utils.getHistoricoReclamacaos();

        Utils.setRegistroAtividadeRSCList(null);

        if (registroAtividadeRSCList == null) {
            registroAtividadeRSCList = new ArrayList<>();
        }
        mAdapter = new HistoricoReclamacaoListAdapter(this, registroAtividadeRSCList);
        recyclerView.setAdapter(mAdapter);


        if (registroAtividadeRSCList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);


        }
    }

    public void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Historico RRC");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);

    }


}
