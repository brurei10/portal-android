package orsegups.com.br.portaldocliente.volley.Net;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.PermissionUtils;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by filipeamaralneis on 02/08/17.
 */

public class ServiceHandler {


    public ServiceHandler() {

    }

    public static void chamadaServicoObject(@NonNull String url, ServiceHandlerEnum put, @NonNull Context context, @NonNull JSONObject jsonObject, @NonNull OnTaskCompletedObject listener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(put.getMethod(), url, jsonObject,
                response -> {
                    listener.OnTaskCompletedJSON(response);
                }, error -> {
            Log.d("d", "Erro");


            switch (error.networkResponse.statusCode) {
                case 400:
                    Alertas.invalidPassword(context);
                    break;
                case 401:
                    PermissionUtils.invalidateSession(context);
                    break;
                default:
                    Alertas.erroComunicacaoServidor(context);
                    break;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                params.put("Authorization", "Bearer " + token);
                params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        executarRequest(jsonObjectRequest, context);
    }

    private static void executarRequest(Request request, Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        request.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    public static void makeServiceCall(String url, ServiceHandlerEnum method, Context context, OnTaskCompleted listener) {

        StringRequest stringRequest = new StringRequest(method.getMethod(), url, response -> {
            Log.d("Debug", response);
            listener.onTaskCompleted(response);
        }, error -> {
            Log.e("Debug", error.toString());
            Alertas.erroComunicacaoServidor(context);
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sp = context.getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                params.put("Content-Type", "application/json");
//                params.put("Authorization", "Bearer " + token);
                params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                if (token != null) {
                    params.put("orsid-auth-token", token);
                }
                return params;
            }
        };
        executarRequest(stringRequest, context);

    }

    public enum ServiceHandlerEnum {
        GET, POST, PUT;


        public int getMethod() {
            switch (this) {
                case GET:
                    return Request.Method.GET;
                case POST:
                    return Request.Method.POST;
                case PUT:
                    return Request.Method.PUT;
            }
            return Request.Method.GET;
        }
    }


}