package orsegups.com.br.portaldocliente.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.adapter.DocumentoExpandableListAdapter;
import orsegups.com.br.portaldocliente.model.Documento;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Orsegups on 08/03/17.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 0.1
 */
public class DocumentosFragment extends Fragment {

    public RecyclerView recyclerView;
    View view;
    private ExpandableListView expandableListView;
    private DocumentoExpandableListAdapter mAdapter;
    private TextView emptyView;
    private PtrClassicFrameLayout mPtrFrame;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_empresas, container, false);
        emptyView = view.findViewById(R.id.empty_view);
        configuraRefresh();
        return view;
    }

    private void configuraRefresh() {
        mPtrFrame = view.findViewById(R.id.store_house_ptr_frame);
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                boolean isConecao = Utils.verificaConexaoInternet();

                if (isConecao) {
                    loadDocumentoFinanceiro();
                } else {
                    configuraSemConexao();
                }
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }
        });

        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setPullToRefresh(false);
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh(true);
            }
        }, 100);
    }

    private void loadDocumentoFinanceiro() {


        try {

            // final SweetAlertDialog dialog = Alertas.comunicacaoServidor(view.getContext(),view.getContext().getString(R.string.alerta_carregando));
            final SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
//            String codcli = sp.getString(Constantes.SP_CNPJ_CLI_MASK, "");
            String cpfCnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");
            final List<Documento> empresaDocFinanceiroSites = new ArrayList<>();

            String url = Constantes.URL_DOCUMENTO + cpfCnpj;

            RequestQueue queue = Volley.newRequestQueue(view.getContext());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Gson gson = new Gson();
                            JsonParser jsonParser = new JsonParser();
                            JsonElement element = jsonParser.parse(response);
                            int status = Integer.parseInt(element.getAsJsonObject().get("status").toString());
                            String msg = element.getAsJsonObject().get("msg").getAsString();

                            if (status == 100) {
                                SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(Constantes.SP_DOCUMENTO_CLI, response);
                                editor.commit();
                                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();


                                for (int i = 0; i < total; i++) {

                                    Documento reclamacao = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(0).getAsJsonObject(), Documento.class);
                                    empresaDocFinanceiroSites.add(reclamacao);

                                    Log.d("", "" + reclamacao);
                                }
                                //  dialog.dismiss();

                                configureList(empresaDocFinanceiroSites);


                            } else {
                                mPtrFrame.refreshComplete();
//                                if (status == 96) {
//                                    Alertas.sessaoExpirada(view.getContext(), sp.getString(Constantes.SP_CNPJ_CLI_MASK, ""));
//                                } else {
//                                    Alertas.erroInesperado(view.getContext(), msg.replace("\"\"", ""));
//                                }
                                emptyView.setVisibility(View.VISIBLE);
                            }

                            Log.d("d", response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("d", "Erro");
                    //  dialog.dismiss();
                    mPtrFrame.refreshComplete();
                    //Alertas.erroComunicacaoServidor(view.getContext());
                    configuraSemConexao();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
                    String token = sp.getString(Constantes.SP_TOKEN_ORSEGUPS_ID, "");
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer " + token);
                    params.put("X-AUTH-TOKEN", "499c7a9a40f3bcda2c54471d03f494dc");
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    120000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);


        } catch (Exception e) {
            mPtrFrame.refreshComplete();
            Alertas.erroComunicacaoServidor(view.getContext());
            configuraSemConexao();
        }
    }

    private void configuraSemConexao() {

//        SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);
//        String response = sp.getString(Constantes.SP_DOCUMENTO_CLI, "");
//        List<ClienteEmpresaSite> empresaDocFinanceiroSites = new ArrayList<>();
//        if (!response.isEmpty()) {
//
//            try {
//                Gson gson = new Gson();
//                JsonParser jsonParser = new JsonParser();
//                JsonElement element = jsonParser.parse(response);
//                int total = element.getAsJsonObject().get("ret").getAsJsonArray().size();
//
//                for (int i = 0; i < total; i++) {
//
//                    ClienteEmpresaSite reclamacao = gson.fromJson(element.getAsJsonObject().get("ret").getAsJsonArray().get(0).getAsJsonObject().get("empresas").getAsJsonArray().get(i), ClienteEmpresaSite.class);
//                    empresaDocFinanceiroSites.add(reclamacao);
//
//                }
//
//            } catch (Exception ignored) {
//            }
//        }
//        configureList(empresaDocFinanceiroSites);
    }


    private void configureList(List<Documento> empresaDocFinanceiroSites) {
        mPtrFrame.refreshComplete();

        expandableListView = view.findViewById(R.id.simple_expandable_listview);

        expandableListView.setGroupIndicator(null);

        final int[] lastExpandedPosition = {-1};

        // fecha a pergunta aberta quando abre uma nova pergunta
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                // closeKeyboard();
                if (lastExpandedPosition[0] != -1
                        && groupPosition != lastExpandedPosition[0]) {
                    expandableListView.collapseGroup(lastExpandedPosition[0]);

                }
                lastExpandedPosition[0] = groupPosition;
            }
        });

        mAdapter = new DocumentoExpandableListAdapter(view.getContext(), empresaDocFinanceiroSites.get(0), this);
        // Setting adpater over expandablelistview

        expandableListView.setAdapter(mAdapter);


        if (empresaDocFinanceiroSites.isEmpty()) {
            expandableListView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            expandableListView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }


    }


}


