package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.ArquivoSite;


public class ArquivoSiteDao extends BaseDaoImpl<ArquivoSite, Integer> {
    public ArquivoSiteDao(ConnectionSource cs) throws SQLException {
        super(ArquivoSite.class);
        setConnectionSource(cs);
        initialize();
    }
}
