package orsegups.com.br.portaldocliente.model;

import java.util.List;

public class ContasSigmaSite {
    private Long cdCliente;
    private String razao;
    private String fantasia;
    private String uf;
    private String cidade;
    private String bairro;
    private String endereco;
    private String centralParticao;
    private String central;
    private String particao;
    private String empresa;
    private String linkEventos;
    private String tipoConta;
    private List<OrdemServicoSite> ordensDeServico;

    public List<OrdemServicoSite> getOrdensDeServico() {
        return ordensDeServico;
    }

    public void setOrdensDeServico(List<OrdemServicoSite> ordensDeServico) {
        this.ordensDeServico = ordensDeServico;
    }

    public Long getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(Long cdCliente) {
        this.cdCliente = cdCliente;
    }

    public String getRazao() {
        return razao;
    }

    public void setRazao(String razao) {
        this.razao = razao;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCentralParticao() {
        return centralParticao;
    }

    public void setCentralParticao(String centralParticao) {
        this.centralParticao = centralParticao;
    }

    public String getCentral() {
        return central;
    }

    public void setCentral(String central) {
        this.central = central;
    }

    public String getParticao() {
        return particao;
    }

    public void setParticao(String particao) {
        this.particao = particao;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getLinkEventos() {
        return linkEventos;
    }

    public void setLinkEventos(String linkEventos) {
        this.linkEventos = linkEventos;
    }

    public String getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }
}
