package orsegups.com.br.portaldocliente.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.BoletoActivity;
import orsegups.com.br.portaldocliente.model.DocumentoFinanceiroSite;
import orsegups.com.br.portaldocliente.volley.Net.OnLoadMoreListener;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class NotaListAdapter extends RecyclerView.Adapter {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    //PDFView pdfView;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private String TAG = "PDF-FILIPE";
    private List<DocumentoFinanceiroSite> notasList;
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public NotaListAdapter(Context context, List<DocumentoFinanceiroSite> items, RecyclerView recyclerView) {
        //original_items = items;
        notasList = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;

//
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

//                            totalItemCount = linearLayoutManager.getItemCount();
//                            lastVisibleItem = linearLayoutManager
//                                    .findLastVisibleItemPosition();
//                            if (!loading
//                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                                // End has been reached
//                                // Do something
//                                if (onLoadMoreListener != null) {
//                                    onLoadMoreListener.onLoadMore();
//                                }
//                                loading = true;
//                            }
                        }
                    });
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return notasList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_empresa_notas_fiscais, parent, false);

            vh = new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof ViewHolder) {
            final DocumentoFinanceiroSite doc = notasList.get(position);
            ((ViewHolder) holder).barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.cor_barra_vertical_card_view));
            ((ViewHolder) holder).nfe.setText(doc.getNumDfs().toString());
            ((ViewHolder) holder).rps.setText(doc.getRps().toString());
            ((ViewHolder) holder).serie.setText(doc.getSerie());
            ((ViewHolder) holder).emisao.setText(doc.getEmissao());
            NumberFormat format = NumberFormat.getCurrencyInstance();
            ((ViewHolder) holder).valor.setText(format.format(doc.getValor()));

            ((ViewHolder) holder).btnVerNota.setOnClickListener(v -> openWebPage(doc.getLinkNfse()));
            ((ViewHolder) holder).btnCompartilhar.setOnClickListener(v -> compartilharNFE(doc));

            if (doc.getSitTit().equalsIgnoreCase("AB")) {
                ((ViewHolder) holder).barraLateral.setBackgroundResource(R.color.bootstrap_brand_warning);

            }

            if (doc.getBoletos().isEmpty()) {
                ((ViewHolder) holder).btnBoletos.setEnabled(false);
            } else {

                ((ViewHolder) holder).btnBoletos.setEnabled(true);

                ((ViewHolder) holder).btnBoletos.setOnClickListener(v -> {
                    //http://portal-mobile-prod.sa-east-1.elasticbeanstalk.com/portalcliente/v1/competenciaNotas?cgcCpf=76484013000145
                    doc.getBoletos().get(0).setLinkboleto(doc.getLinkBoleto());
                    Utils.setBoletos(doc.getBoletos());
                    Intent browserIntent = new Intent(ctx, BoletoActivity.class);
                    ctx.startActivity(browserIntent);
                });
            }


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    private void openWebPage(String url) {
//        Uri webpage = Uri.parse(url);
//
//        Intent intent = new Intent();
//        intent.setData(webpage);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        ctx.startActivity(intent);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        ctx.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return notasList.size();
    }

    private void compartilharNFE(DocumentoFinanceiroSite doc) {

        NumberFormat format = NumberFormat.getCurrencyInstance();

        StringBuilder cabecalho = new StringBuilder();

        cabecalho.append("<br>");
        cabecalho.append("<i>NFs-e:</i> " + doc.getNumDfs().toString());
        cabecalho.append("<br>");
        cabecalho.append("RPS: " + doc.getRps());
        cabecalho.append("<br>");
        cabecalho.append("Série: " + doc.getSerie() + "<br>");
        cabecalho.append("Emissão: " + doc.getEmissao() + "<br>");
        cabecalho.append("Valor: " + format.format(doc.getValor()) + "<br>");
        cabecalho.append("PDF NFs-e: " + doc.getLinkNfse() + "<br>");


        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        // emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"filipe.neis@orsegups.com.br"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NFS-e " + doc.getNumDfs());
        emailIntent.putExtra(Intent.EXTRA_TEXT, Utils.fromHtml(cabecalho.toString()).toString());
        ctx.startActivity(Intent.createChooser(emailIntent, "Compartilhar com:"));


    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
//

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar1);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView valor;
        // each nfeData item is just a string in this case
        TextView nfe;
        TextView rps;
        TextView emisao;
        TextView serie;
        Button btnCompartilhar;
        Button btnVerNota;
        Button btnBoletos;
        View barraLateral;


        public ViewHolder(View v) {
            super(v);

            barraLateral = v.findViewById(R.id.barra_lateral);
            nfe = v.findViewById(R.id.row_nota_nfe);
            rps = v.findViewById(R.id.row_nota_rps);
            serie = v.findViewById(R.id.row_nota_serie);
            emisao = v.findViewById(R.id.row_nota_emissao);
            valor = v.findViewById(R.id.row_nota_valor);
            btnCompartilhar = v.findViewById(R.id.row_nota_exportar);
            btnVerNota = v.findViewById(R.id.row_nota_ver_nota);
            btnBoletos = v.findViewById(R.id.row_nota_boleto);


        }
    }
}
