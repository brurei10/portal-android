package orsegups.com.br.portaldocliente.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.MixpanelUtil;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.Net.SincronizaBaseDados;

public class AlterarSenhaFragment extends Fragment {

    View view;
    // UI references.
    private EditText currentPassword;
    private EditText newPassword;
    private EditText confirmNewPassword;
    private Button btnChangePassword;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_alterar_senha, container, false);
        context = view.getContext();

        currentPassword = view.findViewById(R.id.current_password);
        newPassword = view.findViewById(R.id.new_password);
        confirmNewPassword = view.findViewById(R.id.confirm_new_password);

        btnChangePassword = view.findViewById(R.id.btn_change_password);
        btnChangePassword.setOnClickListener(v -> updatePassword());

        MixpanelUtil.track(Utils.getCnpjLogado(context), Constantes.MIXPANEL_DETALHE_PERFIL, "CONFIGURACOES", context);
        return view;
    }

    private void updatePassword() {
        String currentPasswordText = currentPassword.getText().toString();
        String newPasswordText = newPassword.getText().toString();

        if (validateSubmit()) {
            MixpanelUtil.trackMessage(Utils.getCnpjLogado(context), Constantes.MIXPANEL_DETALHE_PERFIL, "CONFIGURACOES", context, "ALTEROU SENHA");
            SincronizaBaseDados.changePassword(context, currentPasswordText, newPasswordText, Utils.getOrsegupsIdEmail(context));
        }
    }

    private boolean validateSubmit() {
        String currentPasswordText = currentPassword.getText().toString();
        String newPasswordText = newPassword.getText().toString();
        String confirmNewPasswordText = confirmNewPassword.getText().toString();

        if (currentPasswordText.length() < 1) {
            Alertas.error(context, context.getString(R.string.alert_current_password_empty));
            return false;
        }

        if (newPasswordText.length() < 6) {
            Alertas.error(context, context.getString(R.string.alert_min_password_6));
            return false;
        }

        if (!newPasswordText.equals(confirmNewPasswordText)) {
            Alertas.error(context, context.getString(R.string.alert_confirm_password_diff));
            return false;
        }

        return true;
    }

}