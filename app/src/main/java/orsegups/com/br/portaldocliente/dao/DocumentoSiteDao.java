package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.DocumentoSite;


public class DocumentoSiteDao extends BaseDaoImpl<DocumentoSite, Integer> {
    public DocumentoSiteDao(ConnectionSource cs) throws SQLException {
        super(DocumentoSite.class);
        setConnectionSource(cs);
        initialize();
    }
}
