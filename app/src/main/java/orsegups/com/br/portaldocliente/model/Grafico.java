package orsegups.com.br.portaldocliente.model;

import java.util.List;

/**
 * Created by filipeamaralneis on 16/05/17.
 */

public class Grafico {

    private Long tipGra;
    private String desTipGra;
    private List<PontosGrafico> lisPon;

    public Long getTipGra() {
        return tipGra;
    }

    public void setTipGra(Long tipGra) {
        this.tipGra = tipGra;
    }

    public String getDesTipGra() {
        return desTipGra;
    }

    public void setDesTipGra(String desTipGra) {
        this.desTipGra = desTipGra;
    }

    public List<PontosGrafico> getLisPon() {
        return lisPon;
    }

    public void setLisPon(List<PontosGrafico> lisPon) {
        this.lisPon = lisPon;
    }
}
