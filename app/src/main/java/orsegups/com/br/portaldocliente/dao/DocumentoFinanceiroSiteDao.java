package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.DocumentoFinanceiroSite;


public class DocumentoFinanceiroSiteDao extends BaseDaoImpl<DocumentoFinanceiroSite, Integer> {
    public DocumentoFinanceiroSiteDao(ConnectionSource cs) throws SQLException {
        super(DocumentoFinanceiroSite.class);
        setConnectionSource(cs);
        initialize();
    }
}