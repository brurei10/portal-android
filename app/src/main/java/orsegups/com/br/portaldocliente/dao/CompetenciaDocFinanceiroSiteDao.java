package orsegups.com.br.portaldocliente.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import orsegups.com.br.portaldocliente.model.CompetenciaDocFinanceiroSite;


public class CompetenciaDocFinanceiroSiteDao extends BaseDaoImpl<CompetenciaDocFinanceiroSite, Integer> {
    public CompetenciaDocFinanceiroSiteDao(ConnectionSource cs) throws SQLException {
        super(CompetenciaDocFinanceiroSite.class);
        setConnectionSource(cs);
        initialize();
    }
}
