package orsegups.com.br.portaldocliente.model;

/**
 * Created by filipe.neis on 18/04/2018.
 */

public class Deslocamento {

    private int historico;

    private int codigoCliente;
    private Double latitude;
    private Double longitude;
    private String codigoEvento;
    private String nomeEvento;
    private int codigoViatura;
    private String nomeViatura;
    private String placaViatura;
    private String descricaoViatura;
    private int codigoStatus;
    private String nomeStatus;
    private boolean viaturaParceira;

    public Deslocamento() {
    }

    public int getHistorico() {
        return historico;
    }

    public void setHistorico(int historico) {
        this.historico = historico;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCodigoEvento() {
        return codigoEvento;
    }

    public void setCodigoEvento(String codigoEvento) {
        this.codigoEvento = codigoEvento;
    }

    public String getNomeEvento() {
        return nomeEvento;
    }

    public void setNomeEvento(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public int getCodigoViatura() {
        return codigoViatura;
    }

    public void setCodigoViatura(int codigoViatura) {
        this.codigoViatura = codigoViatura;
    }

    public String getNomeViatura() {
        return nomeViatura;
    }

    public void setNomeViatura(String nomeViatura) {
        this.nomeViatura = nomeViatura;
    }

    public String getPlacaViatura() {
        return placaViatura;
    }

    public void setPlacaViatura(String placaViatura) {
        this.placaViatura = placaViatura;
    }

    public String getDescricaoViatura() {
        return descricaoViatura;
    }

    public void setDescricaoViatura(String descricaoViatura) {
        this.descricaoViatura = descricaoViatura;
    }

    public int getCodigoStatus() {
        return codigoStatus;
    }

    public void setCodigoStatus(int codigoStatus) {
        this.codigoStatus = codigoStatus;
    }

    public String getNomeStatus() {
        return nomeStatus;
    }

    public void setNomeStatus(String nomeStatus) {
        this.nomeStatus = nomeStatus;
    }

    public boolean isViaturaParceira() {
        return viaturaParceira;
    }

    public void setViaturaParceira(boolean viaturaParceira) {
        this.viaturaParceira = viaturaParceira;
    }
}
