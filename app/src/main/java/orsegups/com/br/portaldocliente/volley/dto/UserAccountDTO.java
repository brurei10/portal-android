package orsegups.com.br.portaldocliente.volley.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountDTO {

    private List<AccountDTO> data;

    public UserAccountDTO() {
        super();
    }

    public UserAccountDTO(List<AccountDTO> data) {
        this();
        this.data = data;
    }

    public List<AccountDTO> getData() {
        return data;
    }

    public void setData(List<AccountDTO> data) {
        this.data = data;
    }

}
