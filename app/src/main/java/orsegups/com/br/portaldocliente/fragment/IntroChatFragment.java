package orsegups.com.br.portaldocliente.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import net.take.blipchat.AuthType;
import net.take.blipchat.BlipClient;
import net.take.blipchat.models.Account;
import net.take.blipchat.models.AuthConfig;
import net.take.blipchat.models.BlipOptions;

import java.util.HashMap;
import java.util.Map;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.model.Chat;
import orsegups.com.br.portaldocliente.util.Constantes;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Orsegups on 05/03/18.<br>
 * Orsegups(link www.orsegups.com.br)<br>
 *
 * @author Filipe Amaral Néis
 * @version Orsegups 1.3
 */
public class IntroChatFragment extends Fragment {

    private View view;
    private Button btnIniciarChat;
    private LinearLayout linearLayout;
    private EditText nomeUsuario;
    private boolean mMostrarBlipChat;

    public IntroChatFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_intro_chat, container, false);
        view.setVisibility(View.INVISIBLE);
        nomeUsuario = view.findViewById(R.id.nome_usuario_whats);
//        if (!Chat.getNome().isEmpty()) {
        nomeUsuario.setText(Chat.getNome(view.getContext()));
//        getChatFragment();
//        }

        ImageView img1 = view.findViewById(R.id.intro_image_1);
        ImageView img2 = view.findViewById(R.id.intro_image_2);
        btnIniciarChat = view.findViewById(R.id.intro_chat_btn);
        linearLayout = view.findViewById(R.id.intro_chat_linear);

        btnIniciarChat.setEnabled(false);

        btnIniciarChat.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.primaryColorDisable));


        IconicsDrawable icon1 = new IconicsDrawable(view.getContext(), GoogleMaterial.Icon.gmd_chat_bubble_outline).color(ContextCompat.getColor(view.getContext(), R.color.primaryColor));
        IconicsDrawable icon2 = new IconicsDrawable(view.getContext(), GoogleMaterial.Icon.gmd_query_builder).color(ContextCompat.getColor(view.getContext(), R.color.primaryColor));

        img1.setImageDrawable(icon1);
        img2.setImageDrawable(icon2);

        btnIniciarChat.setOnClickListener(v -> {

//                getChatFragment();
        });

        configuraEditText();

        showView();

        String url = Constantes.URL_CHAT_USAR;
        StringRequest request = new StringRequest(Request.Method.GET, url, response -> {

            JsonParser jsonParser = new JsonParser();
            JsonElement element = jsonParser.parse(response);
            mMostrarBlipChat = Boolean.parseBoolean(element.getAsJsonObject().get("showBlipChat").toString());
            getChatFragment();
        }, error -> getChatFragment());
        RequestQueue queue = Volley.newRequestQueue(view.getContext());
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
        return null;
    }

    private void getChatFragment() {
        if (mMostrarBlipChat) {
            //Retirando o frament da tela, qaundo fazer o voltar já vai para o menu.
            this.getFragmentManager().popBackStack();
            ((AppCompatActivity) view.getContext()).getSupportActionBar().setTitle("Chat online");

            final SharedPreferences sp = view.getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, MODE_PRIVATE);

            String contractCode = sp.getString(Constantes.SP_CNPJ_CLI, "");
            String cpfCnpj = sp.getString(Constantes.SP_LOGIN_ORSEGUPS_ID, "");
            String email = sp.getString(Constantes.SP_EMAIL_ORSEGUPS_ID, "");
            String nome = sp.getString(Constantes.SP_NOME_CLI, "");
            Account account = new Account();
            account.setFirstName(nome);
            account.setFullName(nome);
            account.setLastName(nome);
            account.setEmail(email);

            Map<String, String> mapDados = new HashMap<>();
            if (cpfCnpj != null) {
                mapDados.put("login", cpfCnpj);
            }
            if (email != null) {
                mapDados.put("email", email);
            }
            if (contractCode != null) {
                mapDados.put("contractCode", contractCode);
            }

            String fcmUserToken = sp.getString(Constantes.SP_TOKEN_CLI, "");
            mapDados.put("#inbox.forwardTo", String.format("%s@firebase.gw.msging.net", fcmUserToken));
            account.setExtras(mapDados);
            BlipOptions blipOptions = new BlipOptions();

            blipOptions.setAccount(account);
            AuthConfig authConfig = new AuthConfig(AuthType.Dev, nome, "teste");
            blipOptions.setAuthConfig(authConfig);


            BlipClient.openBlipThread(view.getContext(), "b3JzZWd1cHNwcm9kOjQyOWQzMTZlLTRmOGQtNDgyZS05NDU5LTE1NTA1NjhjYWE3Mw==", blipOptions);

            if (nome != null) {
                Chat.saveNome(nome, view.getContext());
            }
        } else {
//
            this.getFragmentManager().beginTransaction()
                    .replace(R.id.framelayout, new ChatFragment())
                    .addToBackStack(null)
                    .commit();
        }

    }

    private void configuraEditText() {

        nomeUsuario.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() >= 3) {
                    btnIniciarChat.setEnabled(true);
                    btnIniciarChat.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.primaryColor));
                } else {
                    btnIniciarChat.setEnabled(false);
                    btnIniciarChat.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.primaryColorDisable));
                }
            }
        });
    }

    private void showView() {
        if (linearLayout != null) {
            linearLayout.setVisibility(View.VISIBLE);
        }
    }

}