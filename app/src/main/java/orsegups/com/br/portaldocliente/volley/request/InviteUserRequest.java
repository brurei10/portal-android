package orsegups.com.br.portaldocliente.volley.request;

import android.app.Activity;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.PermissionUtils;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.volley.common.AbstractRequest;
import orsegups.com.br.portaldocliente.volley.common.TokenRequest;
import orsegups.com.br.portaldocliente.volley.dto.AccountDTO;
import orsegups.com.br.portaldocliente.volley.dto.ProductDTO;

public class InviteUserRequest extends AbstractRequest {

    private ProductDTO product;
    private String email;
    private String cpfCnpj;

    public InviteUserRequest(Context context, String cpfCnpj, ProductDTO product, String email) {
        super(context);
        this.product = product;
        this.email = email;
        this.cpfCnpj = cpfCnpj;
    }

    public void exec() {
        awaitDialog.show();

        AccountDTO account = Utils.getActiveAccount(context);
        if (account != null) {

            Map<String, Object> body = new HashMap<>();
            body.put("email", email);
            body.put("login", email);
            body.put("product", product);
            String requestBody = "";

            try {
                requestBody = new ObjectMapper().writeValueAsString(body);
                TokenRequest request = new TokenRequest(context, Request.Method.POST, Constantes.URL_INVITE_USER, requestBody, successResponse(), errorResponse());
                makeRequest(request);
            } catch (JsonProcessingException e) {
                Alertas.unexpectedError(context);
            }
        } else {
            Alertas.unexpectedError(context);
        }
    }

    private Response.Listener<String> successResponse() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("successResponse");
                awaitDialog.dismiss();
                Alertas.success(context, context.getString(R.string.alert_invite_success),
                    new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog dialog) {
                        dialog.dismiss();
                        ((Activity) context).onBackPressed();
                    }
                });
            }
        };
    }

    private Response.ErrorListener errorResponse() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("errorResponse");
                awaitDialog.dismiss();
                Integer status = error.networkResponse != null ? error.networkResponse.statusCode : -1;
                switch (status) {
                    case -1:
                    case 400:
                    case 404:
                        Alertas.error(context, context.getString(R.string.alert_error_contact_support));
                        break;
                    case 401:
                        PermissionUtils.invalidateSession(context);
                        break;
                    case 409:
                        Alertas.error(context, context.getString(R.string.alert_already_invited));
                        break;
                    default:
                        Alertas.erroComunicacaoServidor(context);
                        break;
                }
            }
        };
    }

}
