package orsegups.com.br.portaldocliente.adapter;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.view.IconicsImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import orsegups.com.br.portaldocliente.R;
import orsegups.com.br.portaldocliente.util.Alertas;
import orsegups.com.br.portaldocliente.util.Avaliacao;
import orsegups.com.br.portaldocliente.util.Constantes;
import orsegups.com.br.portaldocliente.util.Utils;
import orsegups.com.br.portaldocliente.activity.RatDetalhesActivity;
import orsegups.com.br.portaldocliente.model.Boleto;
import orsegups.com.br.portaldocliente.model.NotaTimeline;
import orsegups.com.br.portaldocliente.model.Rat;
import orsegups.com.br.portaldocliente.volley.Net.OnLoadMoreListener;


/**
 * Created by filipeamaralneis on 14/07/16.
 */
public class TimelineListAdapter extends RecyclerView.Adapter {

    private final int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<Object> objetos = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private ImageLoader mImageLoader;


    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 11;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public TimelineListAdapter(Context context, List<Object> items, ImageLoader mImageLoader, RecyclerView recyclerView) {
        setHasStableIds(true);
        objetos = items;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        ctx = context;
        this.mImageLoader = mImageLoader;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_timeline, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {

            ((ViewHolder) holder).barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.cor_barra_vertical_card_view));

            if (objetos.get(position) instanceof NotaTimeline) {

                ((ViewHolder) holder).viewRat.setVisibility(View.GONE);
                ((ViewHolder) holder).viewBoleto.setVisibility(View.GONE);
                ((ViewHolder) holder).viewNf.setVisibility(View.VISIBLE);

                final NotaTimeline doc = (NotaTimeline) objetos.get(position);

                ((ViewHolder) holder).nfeNumero.setText(doc.getNumDfs().toString());
                ((ViewHolder) holder).nfeRps.setText(doc.getRps().toString());
                ((ViewHolder) holder).nfeSerie.setText(doc.getSerie());
                ((ViewHolder) holder).nfeEmisao.setText(doc.getEmissao());
                ((ViewHolder) holder).nfeData.setText(doc.getDatHorEnv());
                NumberFormat format = NumberFormat.getCurrencyInstance();
                ((ViewHolder) holder).nfeValor.setText(format.format(doc.getValor()));
                ((ViewHolder) holder).nfeBtnCompartilhar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        compartilharNFE(doc);
                    }
                });

            } else if (objetos.get(position) instanceof Boleto) {
                final Boleto boleto = (Boleto) objetos.get(position);
                configuraBoleto(((ViewHolder) holder), boleto);


            } else if (objetos.get(position) instanceof Rat) {

                final Rat rat = (Rat) objetos.get(position);

                configuraRat(((ViewHolder) holder), rat);

            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void configuraRat(final ViewHolder holder, final Rat rat) {

        holder.viewNf.setVisibility(View.GONE);
        holder.viewBoleto.setVisibility(View.GONE);
        holder.ratFotoCabecalho.setVisibility(View.VISIBLE);


        if (rat.getDataAtendimento() == null || rat.getDataAtendimento().trim().isEmpty() || rat.getDataAtendimento().equalsIgnoreCase("null")) {
            holder.ratViewData.setVisibility(View.GONE);
        } else {
            holder.ratData.setText(Utils.getDataTimeLine(rat.getDataAtendimento(), rat.getHoraAtendimento()));
        }

        if (rat.getEvento() != null && !rat.getEvento().trim().isEmpty()) {
            String titulo = rat.getEvento();
            titulo = titulo.toLowerCase();
            try {
                titulo = titulo.substring(0, 1).toUpperCase() + titulo.substring(1);
                holder.ratTitulo.setText(titulo);
            } catch (Exception e) {
                holder.ratTitulo.setText(rat.getEvento());
            }

            holder.ratTitulo.setText(titulo);
            holder.ratTitulo.setVisibility(View.VISIBLE);
        } else {
            holder.ratTitulo.setVisibility(View.GONE);
        }


        if (rat.getResultadoDoAtendimento() == null || rat.getResultadoDoAtendimento().trim().isEmpty() || rat.getResultadoDoAtendimento().equalsIgnoreCase("null")) {
            holder.ratViewResultado.setVisibility(View.GONE);
        } else {
            holder.ratViewResultado.setVisibility(View.VISIBLE);
            holder.ratResultado.setText(rat.getResultadoDoAtendimento());
        }

        holder.viewRat.setOnClickListener(v -> {
            Intent i = new Intent(ctx, RatDetalhesActivity.class);
            i.putExtra("nfeValor", false);

            ctx.startActivity(i);
        });


        holder.ratResultado.setTextColor(ctx.getResources().getColor(R.color.material_green_600));

        if (rat.getFotoAitLnk() == null || rat.getFotoAitLnk().trim().isEmpty() || rat.getFotoAitLnk().equalsIgnoreCase("null")) {
            holder.ratFotoAtendente.setVisibility(View.GONE);
        } else {
            try {
                setImage(holder.ratFotoAtendente, rat.getFotoAitLnk());

                holder.ratFotoAtendente.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                holder.ratFotoAtendente.setVisibility(View.GONE);
                // holder.ratProgressBar.setVisibility(View.GONE);
            }
        }

        if (rat.getFotoLocalLnk() == null || rat.getFotoLocalLnk().trim().isEmpty() || rat.getFotoLocalLnk().equalsIgnoreCase("null")) {
            holder.ratFotoLocalAtendimento.setVisibility(View.GONE);
        } else {
            try {
                setImage(holder.ratFotoLocalAtendimento, rat.getFotoLocalLnk());

                holder.ratFotoLocalAtendimento.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                holder.ratFotoLocalAtendimento.setVisibility(View.GONE);
            }
        }

        if (rat.getNeoId() == null) {

            rat.setNeoId((long) Constantes.NEOID_ORSEGUPS);
        }


        // todo validar imagens de cabecario
        switch ((int) rat.getTipRat()) {


            case 1:
            case 3:
            case 4:

                holder.ratFotoCabecalho.setImageResource(R.drawable.desvio_habito2);
                holder.ratNome.setText("Desvio de hábito");

            case 2://Desvio de hábito - XXX2

                holder.ratResultado.setTextColor(ctx.getResources().getColor(R.color.material_red_600));
                holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_red_600));
                holder.ratFotoCabecalho.setImageResource(R.drawable.desvio_habito2);
                holder.ratNome.setText("Desvio de hábito");

                break;

            case 5: // OS Fechamento
                holder.ratFotoCabecalho.setImageResource(R.drawable.atendimento_tecnico);
                holder.ratNome.setText("Atendimento Técnico");
                break;

            case 8://XVID com Imagem
                holder.ratFotoCabecalho.setImageResource(R.drawable.conexao_remota);
                holder.ratNome.setText("Falha conexão remota");
                break;

            case 6:
            case 9:
                holder.ratFotoCabecalho.setImageResource(R.drawable.verificacao_imagens);
                holder.ratNome.setText("Monitoramento de Imagem");
                break;

            case 7://XVID com Imagem
                holder.ratFotoCabecalho.setImageResource(R.drawable.verificacao_imagens);
                holder.ratNome.setText("Verificação de imagens");
                break;
            default:
                holder.ratFotoCabecalho.setImageResource(R.drawable.aatendimento_tatico);
                holder.ratNome.setText("Atendimento Tático");

                break;

        }

        holder.ratLocal.setText(rat.getLocal());


        holder.viewRat.setOnClickListener(v -> {
            Intent i = new Intent(ctx, RatDetalhesActivity.class);
            i.putExtra(Constantes.OBJ_RAT, rat);
            i.putExtra("titulo", holder.ratNome.getText().toString());
            ctx.startActivity(i);
        });

        holder.ratDetalhesBtn.setOnClickListener(v -> {
            Intent i = new Intent(ctx, RatDetalhesActivity.class);
            i.putExtra(Constantes.OBJ_RAT, rat);
            i.putExtra("titulo", holder.ratNome.getText().toString());
            ctx.startActivity(i);
        });

        Avaliacao avaliacao = new Avaliacao();
        avaliacao.init(ctx, holder.ratAvaliacao, rat);
        avaliacao.setRatAvaliada(rat.getAvaliacaoNota());

    }

    private void configuraBoleto(final ViewHolder holder, final Boleto boleto) {

        holder.viewRat.setVisibility(View.GONE);
        holder.viewNf.setVisibility(View.GONE);
        holder.viewBoleto.setVisibility(View.VISIBLE);

        holder.boletoNumTitulo.setText(boleto.getNumTit());
        holder.boletoVencimento.setText(boleto.getDatVen());
        holder.boleotData.setText(boleto.getDatHorEnv());
        NumberFormat format = NumberFormat.getCurrencyInstance();
        holder.boletoValor.setText(format.format(boleto.getValTit()));
        holder.boletoBtnCopiarCodigo.setOnClickListener(v -> {

            ClipboardManager _clipboard = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
            _clipboard.setText(boleto.getLinDig());
            Alertas.codigoCopiado(ctx);
        });


        if (!boleto.isSituacao()) {
            holder.boletoSituacao.getIcon().icon(GoogleMaterial.Icon.gmd_warning);
            int cor = holder.itemView.getResources().getColor(R.color.bootstrap_brand_warning);
            holder.boletoSituacao.getIcon().color(cor);
            holder.boletoSituacaoDescricao.setText("Pendente");
            holder.boletoSituacaoDescricao.setTextColor(cor);

            holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.bootstrap_brand_warning));

        } else {
            holder.boletoSituacao.getIcon().icon(GoogleMaterial.Icon.gmd_check_circle);
            int cor = holder.itemView.getResources().getColor(R.color.material_green_600);
            holder.barraLateral.setBackgroundColor(cor);
            holder.boletoSituacao.getIcon().color(cor);
            holder.boletoSituacaoDescricao.setText("Pago");

            holder.barraLateral.setBackgroundColor(ctx.getResources().getColor(R.color.material_green_600));
            holder.boletoLinearBotoes.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return objetos.size();
    }

    public List<Object> getItens() {
        return objetos;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setLoaded() {
        loading = false;
    }

    private void compartilharNFE(NotaTimeline doc) {

        NumberFormat format = NumberFormat.getCurrencyInstance();

        StringBuilder cabecalho = new StringBuilder();

        cabecalho.append("<br>");
        cabecalho.append("<i>NFs-e:</i> " + doc.getNumDfs().toString());
        cabecalho.append("<br>");
        cabecalho.append("RPS: " + doc.getRps());
        cabecalho.append("<br>");
        cabecalho.append("Série: " + doc.getSerie() + "<br>");
        cabecalho.append("Emissão: " + doc.getEmissao() + "<br>");
        cabecalho.append("Valor: " + format.format(doc.getValor()) + "<br>");
        cabecalho.append("PDF NFs-e: " + doc.getLinkNfse() + "<br>");


        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        // emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"filipe.neis@orsegups.com.br"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NFS-e " + doc.getNumDfs());
        emailIntent.putExtra(Intent.EXTRA_TEXT, Utils.fromHtml(cabecalho.toString()).toString());
        ctx.startActivity(Intent.createChooser(emailIntent, "Compartilhar com:"));


    }

    private void setImage(final ImageView image, String url) {
        //Algumas urls são antigas, então coloca o necessario para mostrar elas.
        if (!url.contains("key")) {
            url += "&zoom=19&scale=1&size=300x300&maptype=hybrid&key=AIzaSyBYHmAPLeCdp3u89y5U1yIEohU-yjykI0s&format=png";
        }
        mImageLoader.displayImage(url,
                image,
                null,
                new ImageLoadingListener() {

                    @Override
                    public void onLoadingCancelled(String uri, View view) {
                        //  Log.i("Script", "onLoadingCancelled()");
                        image.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String uri, View view, Bitmap bmp) {
                        //  Log.i("Script", "onLoadingComplete()");
                    }

                    @Override
                    public void onLoadingFailed(String uri, View view, FailReason fail) {
                        image.setVisibility(View.GONE);
                        //Log.i("Script", "onLoadingFailed("+fail+")");
                    }

                    @Override
                    public void onLoadingStarted(String uri, View view) {
                        // Log.i("Script", "onLoadingStarted()");
                    }

                }, (uri, view, current, total) -> {

                    //Log.i("Script", "onProgressUpdate("+uri+" : "+total+" : "+current+")");
                });

    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar1);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nfeData;
        View barraLateral;
        // each nfeData item is just a string in this case
        //RAT
        Button ratDetalhesBtn;
        CardView viewRat;
        CardView viewNf;
        CardView viewBoleto;
        TextView ratNome;
        TextView ratLocal;
        TextView ratData;
        TextView ratTitulo;
        TextView ratResultado;
        ImageView ratFotoCabecalho;
        ImageView ratFotoAtendente;
        ImageView ratFotoLocalAtendimento;
        LinearLayout viewBtn;
        LinearLayout ratViewLocal;
        LinearLayout ratViewLocalFoto;
        LinearLayout ratViewData;
        LinearLayout ratViewResultado;
        ProgressBar ratProgressBar;
        LinearLayout ratAvaliacao;
        //NF
        TextView nfeNumero;
        TextView nfeRps;
        TextView nfeEmisao;
        TextView nfeValor;
        TextView nfeSerie;
        ImageView nfeBtnCompartilhar;

        //BOLETO
        TextView boletoTitulo;
        TextView boletoSituacaoDescricao;
        TextView boleotData;
        TextView boletoNumTitulo;
        TextView boletoValor;
        TextView boletoVencimento;
        ImageView boletoBtnCompartilhar;
        BootstrapButton boletoBtnCopiarCodigo;
        IconicsImageView boletoSituacao;
        LinearLayout boletoLinearBotoes;

        public ViewHolder(View v) {
            super(v);

            barraLateral = v.findViewById(R.id.barra_lateral);

            ratTitulo = v.findViewById(R.id.row_timeline_rat_titulo);
            ratNome = v.findViewById(R.id.row_time_line_rat_nome);
            ratData = v.findViewById(R.id.row_timeline_rat_data);
            ratLocal = v.findViewById(R.id.row_timeline_rat_local);
            ratResultado = v.findViewById(R.id.row_timeline_rat_resultado);
            ratFotoCabecalho = v.findViewById(R.id.row_timeline_rat_foto);
            nfeBtnCompartilhar = v.findViewById(R.id.bt_share);
            ratFotoAtendente = v.findViewById(R.id.row_timeline_rat_foto_atendente);
            ratViewData = v.findViewById(R.id.rat_linear_data);
            ratViewResultado = v.findViewById(R.id.row_timeline_rat_view_resultado);
            ratViewLocal = v.findViewById(R.id.row_timeline_rat_view_local);
            ratViewLocalFoto = v.findViewById(R.id.row_timeline_rat_view_local_foto);
            ratFotoLocalAtendimento = v.findViewById(R.id.rat_detalhe_foto);
            ratProgressBar = v.findViewById(R.id.row_timeline_progress_bar);
            ratDetalhesBtn = v.findViewById(R.id.row_timeline_detalhes_btn);
            ratAvaliacao = v.findViewById(R.id.row_timeline_rat_avaliacao);

            //Nota fiscal
            nfeNumero = v.findViewById(R.id.row_nota_nfe);
            nfeData = v.findViewById(R.id.row_timeline_nf_data);
            nfeRps = v.findViewById(R.id.row_nota_rps);
            nfeSerie = v.findViewById(R.id.row_nota_serie);
            nfeEmisao = v.findViewById(R.id.row_nota_emissao);
            nfeValor = v.findViewById(R.id.row_nota_valor);


            //Boleto
            boletoTitulo = v.findViewById(R.id.row_timeline_boleto_titulo);
            boleotData = v.findViewById(R.id.row_timeline_boleto_data);
            boletoNumTitulo = v.findViewById(R.id.row_timeline_boleto_num_titulo);
            boletoValor = v.findViewById(R.id.row_timeline_boleto_valor);
            boletoVencimento = v.findViewById(R.id.row_timeline_boleto_vencimento);
            boletoBtnCompartilhar = v.findViewById(R.id.row_timeline_boleto_compartilhar);
            boletoBtnCopiarCodigo = v.findViewById(R.id.row_timeline_boleto_copiar_codigo);
            boletoSituacaoDescricao = v.findViewById(R.id.row_timeline_boleto_situacao_descricao);
            boletoSituacao = v.findViewById(R.id.row_timeline_boleto_situacao);
            boletoLinearBotoes = v.findViewById(R.id.row_timeline_boleto_botoes);


            viewBtn = v.findViewById(R.id.row_timeline_view_btn);
            viewNf = v.findViewById(R.id.row_timeline_nf);
            viewRat = v.findViewById(R.id.row_timeline_rat);
            viewBoleto = v.findViewById(R.id.row_timeline_boleto);

        }
    }

}